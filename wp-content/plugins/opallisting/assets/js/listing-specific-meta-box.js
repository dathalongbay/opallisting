/**
 * Created by DELL on 9/1/2016.
 */


jQuery(document).ready(function($){
    var opallisting_ppt_specific_fields_key = [];

    var opallisting_ppt_type_select_val =  $("#opallisting_ppt_type").val();

    $("#opallisting_ppt_type option").each(function()
    {
        var optionVal = $(this).val();

        if(optionVal != opallisting_ppt_type_select_val){
            $("#opallisting_ppt_specific_fields_"+optionVal).hide();
        }

        opallisting_ppt_specific_fields_key.push(optionVal);

        $("#opallisting_ppt_specific_fields_"+optionVal).addClass("opallisting-specific-field");

    });

    $("#opallisting_ppt_type").on("change", function (e) {
        e.preventDefault();

        var selectVal = $(this).val();

        $(".opallisting-specific-field").hide();

        $("#opallisting_ppt_specific_fields_"+selectVal).show();

    });

});


