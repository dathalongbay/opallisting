/**
 * Created by DELL on 8/30/2016.
 */



jQuery(document).ready(function($){

    $('.fa-icon-picker').fontIconPicker({
        //source:    ['fa fa-adjust', 'icon-search', 'icon-user', 'icon-tag', 'icon-help'],
    }); // Load with default options


    $(".listing-creator-custom-fields .content-fields").on("click", ".opallisting-remove-option", function(e){
        e.preventDefault();
        var remove_element_container = $(this).closest(".options-container");
        $(this).closest(".option-row").remove();

        remove_element_container.find("input.opallisting-options-default").each(function(index){
            console.log(index);
            $(this).val(index);
        });

    });


    $(".listing-creator-custom-fields .content-fields").on('click', '.panel-title', function(e){
        e.preventDefault();
        $(this).closest(".panel-group").find(".panel-body").slideToggle();
    });

    $(".listing-creator-custom-fields .content-fields").on("click", ".remove-custom-field-item", function(e){
        e.preventDefault();
        $(this).closest(".panel-group").remove();

        // rename select options

        $('.select-container').each(function (index, value) {


            $(this).find('input.opallisting-options-label').attr("name", "opal_custom_select_options_label["+index+"][]");
            $(this).find('input.opallisting-options-value').attr("name", "opal_custom_select_options_value["+index+"][]");
            $(this).find('input.opallisting-options-default').attr("name", "opal_custom_select_options_default["+index+"][]");
            $(this).find('input.opallisting-select-index').val(index);



        });
    });

    $("#create-text").click(function(e){
        e.preventDefault();

        var nonce = $(this).attr("data-nonce");

        jQuery.ajax({
            type : "post",
            dataType : "json",
            url : myAjax.ajaxurl,
            data : {action: "creator_custom_type", type : "text", nonce: nonce},
            success: function(response) {
                $(".listing-creator-custom-fields .content-fields").append(response);
                if(response.type == "success") {
                    $(".listing-creator-custom-fields .content-fields").append(response.html);

                    $('.fa-icon-picker').fontIconPicker({});

                }
                else {
                    alert("Error please try again");
                }
            }
        })
    });

    $("#create-textarea").click(function(e){
        e.preventDefault();
        var nonce = $(this).attr("data-nonce");

        jQuery.ajax({
            type : "post",
            dataType : "json",
            url : myAjax.ajaxurl,
            data : {action: "creator_custom_type", type : "textarea", nonce: nonce},
            success: function(response) {
                if(response.type == "success") {
                    $(".listing-creator-custom-fields .content-fields").append(response.html);

                    $('.fa-icon-picker').fontIconPicker({});
                }
                else {
                    alert("Error please try again");
                }
            }
        })
    });

    $("#create-select").click(function(e){
        e.preventDefault();

        var nonce = $(this).attr("data-nonce");

        jQuery.ajax({
            type : "post",
            dataType : "json",
            url : myAjax.ajaxurl,
            data : {action: "creator_custom_type", type : "select", nonce: nonce},
            success: function(response) {
                if(response.type == "success") {

                    var index_select = $("input.opallisting-select-index").length;

                    $(".listing-creator-custom-fields .content-fields").append(response.html);
                    $(".select-container:last").find("input.opallisting-select-index").val(index_select);

                    $('.fa-icon-picker').fontIconPicker({});

                }
                else {
                    alert("Error please try again");
                }
            }
        })
    });

    $("#create-checkbox").click(function(e){
        e.preventDefault();

        var nonce = $(this).attr("data-nonce");

        jQuery.ajax({
            type : "post",
            dataType : "json",
            url : myAjax.ajaxurl,
            data : {action: "creator_custom_type", type : "checkbox", nonce: nonce},
            success: function(response) {
                if(response.type == "success") {
                    $(".listing-creator-custom-fields .content-fields").append(response.html);

                    $('.fa-icon-picker').fontIconPicker({});
                }
                else {
                    alert("Error please try again");
                }
            }
        })
    });


    $(".listing-creator-custom-fields .content-fields").on("click", ".add-new-options", function(e){
        e.preventDefault();

        var option_container = $(this).closest('.select-container').find(".options-container");

        var index = $(this).closest('.select-container').find("input.opallisting-select-index").val();

        var option_index = $(this).closest('.select-container').find("input.opallisting-options-default").length;

        var checked_default = '';
        console.log(option_index);
        if(option_index == 0){
            checked_default = 'checked';
        }

        var append_html = '<div class="row option-row" style="margin-top: 10px; margin-bottom: 10px">';
            append_html += '<div class="col-lg-4 col-md-4">' +
                                '<div>' +
                                    '<strong>Label</strong>' +
                                '</div>' +
                                '<div style="margin-top: 10px"><input type="text" name="opal_custom_select_options_label['+index+'][]" class="opallisting-options-label form-control" value="" /></div>' +
                            '</div>' +

                            '<div class="col-lg-4 col-md-4">' +
                                '<div>' +
                                    '<strong>Value</strong>' +
                                '</div>' +
                                '<div style="margin-top: 10px"><input type="text" name="opal_custom_select_options_value['+index+'][]" class="opallisting-options-value form-control" value="" /></div>' +
                            '</div>' +
                            '<div class="col-lg-3 col-md-3">' +
                                '<div>' +
                                    '<strong>Default value</strong>' +
                                '</div>' +
                                '<div style="margin-top: 10px"><input type="radio" class="opallisting-options-default" '+checked_default+' name="opal_custom_select_options_default['+index+']" class="opallisting-options-default" value="'+option_index+'"></div>' +
                            '</div>' +
                            '<div class="col-lg-1 col-md-1">' +
                                '<a href="#" class="opallisting-remove-option">x</a>' +
                            '</div>';
            append_html += '</div>';
        option_container.append(append_html);
    });
});
