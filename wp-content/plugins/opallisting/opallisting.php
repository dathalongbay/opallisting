<?php
/**
 * Plugin Name: Opal Listing
 * Plugin URI: http://www.wpopal.com/listing/
 * Description: An e-commerce toolkit that helps you sell anything. Beautifully.
 * Version: 1.0
 * Author: WPOPAL
 * Author URI: http://www.wpopal.com
 * Requires at least: 3.8
 * Tested up to: 4.1
 *
 * Text Domain: opallisting
 * Domain Path: /i18n/languages/
 *
 * @package Opal-Listing
 * @category Plugins
 * @author WPOPAL
 */
if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists("OpalListing") ){

	final class OpalListing{

		/**
		 * @var Opallisting The one true Opallisting
		 * @since 1.0
		 */
		private static $instance;

		/**
		 * Opallisting Roles Object
		 *
		 * @var object
		 * @since 1.0
		 */
		public $roles;

		/**
		 * Opallisting Settings Object
		 *
		 * @var object
		 * @since 1.0
		 */
		public $opallisting_settings;

		/**
		 * Opallisting Session Object
		 *
		 * This holds donation data for user's session
		 *
		 * @var object
		 * @since 1.0
		 */
		public $session;

		/**
		 * Opallisting HTML Element Helper Object
		 *
		 * @var object
		 * @since 1.0
		 */
		public $html;


		/**
		 * Opallisting Emails Object
		 *
		 * @var object
		 * @since 1.0
		 */
		public $emails;

		/**
		 * Opallisting Email Template Tags Object
		 *
		 * @var object
		 * @since 1.0
		 */
		public $email_tags;

		/**
		 * Opallisting Customers DB Object
		 *
		 * @var object
		 * @since 1.0
		 */
		public $customers;

		/**
		 * Opallisting API Object
		 *
		 * @var object
		 * @since 1.1
		 */
		public $api;

		/**
		 *
		 */
		public function __construct() {

		}

		/**
		 * Main Opallisting Instance
		 *
		 * Insures that only one instance of Opallisting exists in memory at any one
		 * time. Also prevents needing to define globals all over the place.
		 *
		 * @since     1.0
		 * @static
		 * @staticvar array $instance
		 * @uses      Opallisting::setup_constants() Setup the constants needed
		 * @uses      Opallisting::includes() Include the required files
		 * @uses      Opallisting::load_textdomain() load the language files
		 * @see       Opallisting()
		 * @return    Opallisting
		 */
		public static function getInstance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Opallisting ) ) {
				self::$instance = new OpalListing;
				self::$instance->setup_constants();

				add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );

				self::$instance->includes();
			 	self::$instance->roles              = new Opallisting_Roles();

	 			/**
				 *
				 */
			 
				add_filter( 'opallisting_google_map_api', array( __CLASS__, 'load_google_map_api') );
			}

			return self::$instance;
		}

		public static function load_google_map_api( $key ){ 
			if( get_option('googleapikey') ){
				$key = '//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key='.get_option('googleapikey') ;
			}
			return $key; 
		}
		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is a single
		 * object, therefore we don't want the object to be cloned.
		 *
		 * @since  1.0
		 * @access protected
		 * @return void
		 */
		public function __clone() {
			// Cloning instances of the class is forbidden
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'opallisting' ), '1.0' );
		}

		/**
		 *
		 */
		public function setup_constants(){
			// Plugin version
			if ( ! defined( 'OPALLISTING_VERSION' ) ) {
				define( 'OPALLISTING_VERSION', '1.3.1.1' );
			}

			// Plugin Folder Path
			if ( ! defined( 'OPALLISTING_PLUGIN_DIR' ) ) {
				define( 'OPALLISTING_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin Folder URL
			if ( ! defined( 'OPALLISTING_PLUGIN_URL' ) ) {
				define( 'OPALLISTING_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin Root File
			if ( ! defined( 'OPALLISTING_PLUGIN_FILE' ) ) {
				define( 'OPALLISTING_PLUGIN_FILE', __FILE__ );
			}

			// Plugin Root File
			if ( ! defined( 'OPALLISTING_THEMER_WIDGET_TEMPLATES' ) ) {
				define( 'OPALLISTING_THEMER_WIDGET_TEMPLATES', get_stylesheet_directory().'/' );
			}

			if( !defined('OPALMEMBERSHIP_PACKAGES_PREFIX') ){
				define( 'OPALMEMBERSHIP_PACKAGES_PREFIX', 'opallisting_package_' );
			}

			if( !defined("OPALLISTING_CLUSTER_ICON_URL") ){
				define( 'OPALLISTING_CLUSTER_ICON_URL',  apply_filters( 'opallisting_cluster_icon_url', OPALLISTING_PLUGIN_URL.'assets/cluster-icon.png') );
			} 
		}

		public function setup_cmb2_url() {
			return OPALLISTING_PLUGIN_URL . 'inc/vendors/cmb2/libraries';
		}

		public function includes(){
			global $opallisting_options;

			/**
			 * Get the CMB2 bootstrap!
			 *
			 * @description: Checks to see if CMB2 plugin is installed first the uses included CMB2; we can still use it even it it's not active. This prevents fatal error conflicts with other themes and users of the CMB2 WP.org plugin
			 *
			 */

			if ( file_exists( WP_PLUGIN_DIR . '/cmb2/init.php' ) ) {
				require_once WP_PLUGIN_DIR . '/cmb2/init.php';
			} elseif ( file_exists( OPALLISTING_PLUGIN_DIR . 'inc/vendors/cmb2/libraries/init.php' ) ) {
				require_once OPALLISTING_PLUGIN_DIR . 'inc/vendors/cmb2/libraries/init.php';
				//Customize CMB2 URL
				add_filter( 'cmb2_meta_box_url', array($this, 'setup_cmb2_url') );
			}
			// cmb2 custom field
			if( file_exists(OPALLISTING_PLUGIN_DIR . 'inc/vendors/cmb2/custom-fields/map/map.php') ){
				require_once OPALLISTING_PLUGIN_DIR . 'inc/vendors/cmb2/custom-fields/map/map.php';
				require_once OPALLISTING_PLUGIN_DIR . 'inc/vendors/cmb2/custom-fields/upload/upload.php';
				require_once OPALLISTING_PLUGIN_DIR . 'inc/vendors/cmb2/custom-fields/listing-type/listing-type.php';
			}
			require_once OPALLISTING_PLUGIN_DIR . 'inc/admin/register-settings.php';
			$opallisting_options = opallisting_get_settings();

			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-template-loader.php';

			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-query.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/mixes-functions.php';
		

			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-roles.php';

			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-font-awesome.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-comment.php';

			opallisting_includes( OPALLISTING_PLUGIN_DIR . 'inc/post-types/*.php' );
			opallisting_includes( OPALLISTING_PLUGIN_DIR . 'inc/taxonomies/*.php' );

			require_once OPALLISTING_PLUGIN_DIR . 'inc/template-functions.php';


			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-property.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-agent.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-user.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-favorite.php';

			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-scripts.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-shortcodes.php';

			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-metabox.php';
			 
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-submission.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-search.php';

			require_once OPALLISTING_PLUGIN_DIR . 'install.php';
			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-email.php';


			require_once OPALLISTING_PLUGIN_DIR . 'inc/class-opallisting-vc.php';

			require_once OPALLISTING_PLUGIN_DIR . 'inc/function-search-fields.php';

			add_action( 'widgets_init', array($this, 'widgets_init') );

			add_action( 'init', array($this, 'set_location_actived') );
			require_once OPALLISTING_PLUGIN_DIR . 'inc/ajax-functions.php';
			require_once OPALLISTING_PLUGIN_DIR.'inc/vendors/opalmembership/membership.php';
		}

		
		public static function set_location_actived(){ 
			if( isset($_GET['set_location']) && !empty($_GET['set_location']) ){
				$_SESSION['set_location'] = trim( $_GET['set_location'] );
				wp_redirect( home_url('/') );exit;
			}
			if( isset($_GET['clear_location']) && !empty($_GET['clear_location']) ){ 
				$_SESSION['set_location'] = null;
				wp_redirect( home_url('/') );exit;
			}
			if( isset($_SESSION['set_location']) && !empty($_SESSION['set_location']) ){
				Opallisting_Query::$LOCATION = $_SESSION['set_location'];
			}
			
		}

		/**
		 *
		 */
		public function load_textdomain() {
			// Set filter for Opallisting's languages directory
			$lang_dir = dirname( plugin_basename( OPALLISTING_PLUGIN_FILE ) ) . '/languages/';
			$lang_dir = apply_filters( 'opallisting_languages_directory', $lang_dir );

			// Traditional WordPress plugin locale filter
			$locale = apply_filters( 'plugin_locale', get_locale(), 'opallisting' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'opallisting', $locale );

			// Setup paths to current locale file
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/opallisting/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/opallisting folder
				load_textdomain( 'opallisting', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/opallisting/languages/ folder
				load_textdomain( 'opallisting', $mofile_local );
			} else {
				// Load the default language files
				load_plugin_textdomain( 'opallisting', false, $lang_dir );
			}
		}

		public function widgets_init() {
			opallisting_includes( OPALLISTING_PLUGIN_DIR . 'inc/widgets/*.php' );
		}
	}
}

function OpalListing(){
	return OpalListing::getInstance();
}

OpalListing();

