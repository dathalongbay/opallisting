
<div class="property-listing my-favorite">
 	<div class="panel panel-default">
 		<div class="panel-body">
				<?php if( $loop->have_posts() ): ?>
					<div class="opallisting-rows">
						<div class="row">
						<?php $cnt=0; while ( $loop->have_posts() ) : $loop->the_post(); global $post;  ?>
							
				 					
			 				<div class="col-lg-12">
		                    	<?php echo Opallisting_Template_Loader::get_template_part( 'content-property-list' ); ?>
		                	</div>
						
						<?php endwhile; ?>
						</div>
					</div>	
						<?php opallisting_pagination( $loop->max_num_pages ); ?>

				<?php else : ?>	
				<?php echo Opallisting_Template_Loader::get_template_part( 'content-none' ); ?>
				<?php endif; ?>
		</div>	
 	</div>
</div>
<?php wp_reset_postdata(); ?>