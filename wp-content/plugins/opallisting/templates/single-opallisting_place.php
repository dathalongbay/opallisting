<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); ?>

	<section id="main-container" class="site-main container" role="main">
		<main id="primary" class="content content-area">
			<div class="single-opallisting-container">
				<?php if ( have_posts() ) : ?>
				
						<?php while ( have_posts() ) : the_post();  ?>
		                    <?php echo Opallisting_Template_Loader::get_template_part( 'content-single-place', array(), opallisting_single_the_property_layout() ); ?>
						<?php endwhile; ?>
					
					<?php the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'opallisting' ),
						'next_text'          => __( 'Next page', 'opallisting' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'opallisting' ) . ' </span>',
					) ); ?>
					
					<?php 
						
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
											
					?>

				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
