<div class="panel-group" >
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="toggle-panel">
                    TextField</a>
                <a href="#" class="remove-custom-field-item" style="display: block; float: right;">x</a>
            </h4>
        </div>
        <div class="panel-body" style="display: none">
            <div class="form-group">
                <label class="control-label col-sm-2">Metakey</label>
                <div class="col-sm-10">
                    <input type="text" name="cf_meta_key[]" class="form-control" placeholder="Enter metakey">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Title</label>
                <div class="col-sm-10">
                    <input type="text" name="cf_title[]" class="form-control" placeholder="Enter title">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Default value</label>
                <div class="col-sm-10">
                    <input type="text" name="cf_default_value[]" class="form-control" placeholder="Enter default value">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Required</label>
                <div class="col-sm-10">
                    <select name="cf_required[]">

                        <option value="no">No</option>
                        <option value="yes">Yes</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cf_type[]" value="text" />
</div>
