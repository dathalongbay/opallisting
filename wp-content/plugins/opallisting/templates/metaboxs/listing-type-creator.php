

<div class="listing-creator-custom-fields">
    <div class="control-button">
        <a href="#" id="create-text" class="btn btn-info">Text</a>
        <a href="#" id="create-textarea" class="btn btn-info">Textarea</a>
        <a href="#" id="create-select" class="btn btn-info">Select</a>
        <a href="#" id="create-checkbox" class="btn btn-info">Checkbox</a>
    </div>
    <div class="content-fields form-horizontal">   
        
        <?php

        $icons = new Opallisting_font_awesome(OPALLISTING_PLUGIN_URL.'assets/font-awesome-4.6.3/css/font-awesome.css');

        $icons_data = $icons->getIcons();

        $no = 1;


        if($custom_fields){
            $index_select = 0; // $index - 2D array - repeater fields
            while (isset($custom_fields[$no - 1])) {
                $custom_field = $custom_fields[$no - 1];
                switch($custom_field['type']){
                    case 'text':
                        $required = isset($custom_field['required']) ? $custom_field['required'] : '';
                        $title = isset($custom_field['title']) ? $custom_field['title'] : '';
                        $meta_key = isset($custom_field['meta_key']) ? $custom_field['meta_key'] : '';
                        $default_value = isset($custom_field['default_value']) ? $custom_field['default_value'] : '';
                        $icon_pack = isset($custom_field['icon_pack']) ? $custom_field['icon_pack'] : '';
                        $icon = isset($custom_field['icon']) ? $custom_field['icon'] : '';
                        $field = new Opalling_Custom_Field_Text($required,$title,$meta_key,$default_value, '', false, $icons_data, $icon);
                        $field->render();
                        break;
                    case 'textarea':
                        $required = isset($custom_field['required']) ? $custom_field['required'] : '';
                        $title = isset($custom_field['title']) ? $custom_field['title'] : '';
                        $meta_key = isset($custom_field['meta_key']) ? $custom_field['meta_key'] : '';
                        $default_value = isset($custom_field['default_value']) ? $custom_field['default_value'] : '';
                        $icon_pack = isset($custom_field['icon_pack']) ? $custom_field['icon_pack'] : '';
                        $icon = isset($custom_field['icon']) ? $custom_field['icon'] : '';
                        $field = new Opallisting_Custom_Field_Textarea($required,$title,$meta_key,$default_value, '', false, $icons_data, $icon);
                        $field->render();
                        break;
                    case 'select':
                        $required = isset($custom_field['required']) ? $custom_field['required'] : '';
                        $title = isset($custom_field['title']) ? $custom_field['title'] : '';
                        $options = isset($custom_field['options']) ? $custom_field['options'] : array();
                        $meta_key = isset($custom_field['meta_key']) ? $custom_field['meta_key'] : '';
                        $default_value = isset($custom_field['default_value']) ? $custom_field['default_value'] : '';
                        $icon_pack = isset($custom_field['icon_pack']) ? $custom_field['icon_pack'] : '';
                        $icon = isset($custom_field['icon']) ? $custom_field['icon'] : '';
                        $field = new Opallisting_Custom_Field_Select($title,$options,$meta_key,$default_value,false, $icons_data, $icon);
                        $field->render($index_select);
                        $index_select++;
                        break;
                    case 'checkbox':
                        $required = isset($custom_field['required']) ? $custom_field['required'] : '';
                        $title = isset($custom_field['title']) ? $custom_field['title'] : '';
                        $meta_key = isset($custom_field['meta_key']) ? $custom_field['meta_key'] : '';
                        $default_value = isset($custom_field['default_value']) ? $custom_field['default_value'] : '';
                        $icon_pack = isset($custom_field['icon_pack']) ? $custom_field['icon_pack'] : '';
                        $icon = isset($custom_field['icon']) ? $custom_field['icon'] : '';
                        $field = new Opallisting_Custom_Field_Checkbox($required,$title,$meta_key,$default_value, '', false, $icons_data, $icon);
                        $field->render();
                        break;
                    default:
                }
                $no++;

            }
        }
        ?>
       

    </div>
</div>

