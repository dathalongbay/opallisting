<div class="panel-group select-container" >
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="toggle-panel">
                    Select</a>
                <a href="#" class="remove-custom-field-item" style="display: block; float: right;">x</a>
            </h4>
        </div>
        <div class="panel-body" style="display: none">
            <div class="form-group">
                <label class="control-label col-sm-2">Metakey</label>
                <div class="col-sm-10">
                    <input type="text" name="cf_meta_key[]" class="form-control" placeholder="Enter metakey" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Title</label>
                <div class="col-sm-10">
                    <input type="text" name="cf_title[]" class="form-control" placeholder="Enter title" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Options</label>
                <div class="col-sm-10 options-container">
                    <a href="#" class="btn btn-info add-new-options">Add New Item</a>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cf_type[]" value="select" />
    <input type="hidden" name="cf_select_id[]" class="cf_select_id opallisting-select-index" value="" />
</div>
