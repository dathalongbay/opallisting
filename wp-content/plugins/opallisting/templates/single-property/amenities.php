<?php 
	global $property, $post; 
 	
	$amenities =  $property->get_amenities();	

 	$facilities = $property->get_facilities();

 	// echo '<pre>'.print_r( $facilities , 1);
?>
<?php if( $amenities  ): ?>
<div class="list-group property-amenities opallisting-box">
	<h3 class="list-group-item-heading"><?php _e("Amenities", "opallisting"); ?></h3>
	<div class="list-group-item-text">
		<div class="row">
			<?php foreach( $amenities as $amenity ):  ?>
				<div class="col-lg-4 col-md-4 <?php if(  has_term( $amenity->term_id, 'opallisting_amenities', $post )  ) : ?>active<?php endif; ?>">
					<i class="fa fa-check"></i> <?php echo $amenity->name; ?>
				</div>
			<?php endforeach; ?>
		</div>	
	</div>
</div>
<?php endif; ?>

<?php if( $facilities ): ?>
	
	<div class="list-group property-amenities opallisting-box">
		<h3 class="list-group-item-heading"><?php _e("Facilities", "opallisting"); ?></h3>
		<div class="list-group-item-text">
			<div class="row">
				<?php foreach( $facilities as $facility ):  ?>
					<div class="col-lg-6 col-md-6 active">
					
						<span><i class="fa fa-check"></i>  <?php echo $facility[OPALLISTING_PROPERTY_PREFIX.'public_facilities_key']; ?> : </span>
						
						<strong><?php echo $facility[OPALLISTING_PROPERTY_PREFIX.'public_facilities_value']; ?></strong>
					</div>
				<?php endforeach; ?>
			</div>	
		</div>
	</div>

<?php endif; ?>