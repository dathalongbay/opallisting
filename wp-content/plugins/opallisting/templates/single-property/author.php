<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $post;

$contact_id = (int)Opallisting_Query::get_agent_by_property($post->ID);

?>
<?php if( $contact_id ): ?> 
 <?php 
 	$email = get_post_meta( $contact_id, OPALLISTING_AGENT_PREFIX . 'email', true );
 	$args = array( 'post_id' => 0, 'email' => $email );
?>
<div class="opallisting-box property-agent-section">
	<h3><?php _e( 'Contact Agent', 'opallisting' ); ?></h3>
	<div class="row">
		<div class="col-lg-7 property-agent-info">
			<?php OpalListing_Agent::render_box_info( $contact_id  ); ?>
		</div>
		<div class="col-lg-5 property-agent-contact">
			<?php echo Opallisting_Template_Loader::get_template_part( 'single-agent/form', $args ); ?>
		</div>	
	</div>	
</div>
<?php else : ?>
<?php $email = get_user_meta( $post->post_author, OPALLISTING_USER_PROFILE_PREFIX . 'email', true ); ?>
<?php 
 
	$args = array( 'post_id' => 0, 'email' => $email );
	$data =  get_userdata( $post->post_author ); 
?>

<div class="opallisting-box property-agent-section">
	<h3><?php _e( 'Contact Agent', 'opallisting' ); ?></h3>
	<div class="row">
		<div class="col-lg-7 property-agent-info">
			<?php echo Opallisting_Template_Loader::get_template_part( 'parts/author-box', array('author'=>$data , 'hide_description' => true ) ); ?>
		</div>
		<div class="col-lg-5 property-agent-contact">
			<?php echo Opallisting_Template_Loader::get_template_part( 'single-agent/form', $args ); ?>
		</div>	
	</div>	
</div>

<?php endif; ?>

