<div class="property-preview">
	<?php
	global $property;
	$galleries = $property->getGallery();
	if ( !empty($galleries[0]) && isset( $galleries[0]) ):  
	?>

	<div class="owl-carousel-play" data-ride="carousel">
		<div class="owl-carousel-wrapper">
			<div id="sync1" class="owl-carousel" data-slide="1"  data-singleItem="true" data-navigation="true" data-pagination="false">

				<?php if ( has_post_thumbnail() ): ?>
					<?php the_post_thumbnail( 'full' ); ?>
				<?php endif; ?>

				<?php if( isset($galleries[0]) && is_array($galleries[0]) ): ?>
					<?php foreach ($galleries[0] as $src): ?>
						<img src="<?php echo $src; ?>" alt="gallery">
					<?php endforeach; ?>
				<?php endif; ?>
				
			</div>

			<a class="opallisting-left carousel-control carousel-md radius-x" data-slide="prev" href="#">
				<span class="fa fa-angle-left"></span>
			</a>
			<a class="opallisting-right carousel-control carousel-md radius-x" data-slide="next" href="#">
				<span class="fa fa-angle-right"></span>
			</a>

		</div>

		<div class="owl-thumb-wrapper">
			<div id="sync2" class="owl-carousel" data-items="5">
			  	<?php if ( has_post_thumbnail() ): ?>
					<?php the_post_thumbnail( 'full' ); ?>
				<?php endif; ?>

				<?php if( isset($galleries[0]) && is_array($galleries[0]) ): ?>
					<?php foreach ($galleries[0] as $src): ?>
						<img src="<?php echo $src; ?>" alt="gallery">
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>		
	 	
	</div>
	<?php else : ?>
	
	<?php if ( has_post_thumbnail() ): ?>
		<div class="property-thumbnail">
			<?php the_post_thumbnail( 'full' ); ?>
		</div>	
	<?php endif; ?>

	<?php endif; ?>

</div>