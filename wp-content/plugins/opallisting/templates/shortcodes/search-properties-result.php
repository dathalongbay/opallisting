	<?php
		if( class_exists("OpalListing_Search") ):
			$query = OpalListing_Search::get_search_results_query();
		?>
		<div class="opallisting-archive-container">
				<div class="opallisting-archive-top"><div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						 <?php opallisting_show_display_modes(); ?>
					</div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="opallisting-sortable pull-right">
							<?php echo opallisting_render_sortable_dropdown(); ?>
						</div>	
					</div>
				</div></div>	
				
				<div class="opallisting-archive-bottom opallisting-rows">
					<?php if( $query->have_posts() ): ?> 
						<div class="row">
							<?php if ( (isset($_COOKIE['opallisting_displaymode']) && $_COOKIE['opallisting_displaymode'] == 'list') || (!isset($_COOKIE['opallisting_displaymode']) && opallisting_options('displaymode', 'grid') == 'list') ):?>
								<?php while ( $query->have_posts() ) : $query->the_post(); ?>
									<div class="col-lg-12 col-md-12 col-sm-12">
				                    	<?php echo Opallisting_Template_Loader::get_template_part( 'content-property-list' ); ?>
				                	</div>
				                <?php endwhile; ?>
							<?php else : ?>
								<?php $cnt = 0; while ( $query->have_posts() ) : $query->the_post(); 
								$cls = '';$column = 4; 
								if( $cnt++%$column==0 ){
									$cls .= ' first-child';
								}

								?>
									<div class="<?php echo $cls; ?> col-lg-4 col-md-4 col-sm-6">
				                    	<?php echo Opallisting_Template_Loader::get_template_part( 'content-property-grid' ); ?>
				                	</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>

					<?php else: ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>	
				</div>	
			</div>
			<?php if( $query->max_num_pages > 1 ): ?>
			<div class="w-pagination"><?php opallisting_pagination(  $query->max_num_pages ); ?></div>
			<?php endif; ?>
		<?php 
		wp_reset_postdata();
		endif; 	
	?>	
