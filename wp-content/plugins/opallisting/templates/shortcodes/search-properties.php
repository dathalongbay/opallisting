<div class="opallisting-search-properties">
	<div class="inner">
		<div id="opallisting-map-preview" style="height:500px;">
			 <div id="mapView">
		        <div class="mapPlaceholder"><!-- <span class="fa fa-spin fa-spinner"></span> <?php //esc_html_e( 'Loading map...', 'opallisting' ); ?> -->
		        	<div class="sk-folding-cube">
						<div class="sk-cube1 sk-cube"></div>
					  	<div class="sk-cube2 sk-cube"></div>
					  	<div class="sk-cube4 sk-cube"></div>
					  	<div class="sk-cube3 sk-cube"></div>
					</div>
		        </div>
		    </div>
		</div>
		<div class="container">
			<div class="search-properies-form">
				<?php OpalListing_Search::render_horizontal_form(); ?>
			</div>
		</div>	
	</div>
</div>	