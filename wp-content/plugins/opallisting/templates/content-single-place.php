<?php global $property, $post; 
	$property = opallisting_place( get_the_ID() );
	$meta   = $property->get_meta_shortinfo();

echo __FILE__;
?> 
<article id="post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Property" <?php post_class(); ?>>
	<?php do_action( 'opallisting_single_property_before' );  ?>
	<header>
		<div class="row">
			<div class="col-lg-9">
					<?php the_title( '<h1 class="entry-title pull-left">', '</h1>' ); ?>

					<?php if( $property->is_featured() ): ?>
					<span class="property-label" data-toggle="tooltip" data-placement="top" title="<?php esc_html_e('Featured Property', 'opallisting'); ?>">
						<i class="fa fa-star"></i>
					</span>
					<?php endif; ?>
			</div>
			<div class="col-lg-3">
				<div class="property-price">
					<span><?php echo  opallisting_price_format( $property->get_price() ); ?></span>

					<?php if( $property->get_sale_price() ): ?>
					<span class="property-saleprice">
						<?php echo  opallisting_price_format( $property->get_sale_price() ); ?>
					</span>
					<?php endif; ?>

					<?php if( $property->get_price_label() ): ?>
					<span class="property-price-label">
						/ <?php echo $property->get_price_label(); ?>
					</span>	
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="property-meta">
			<div class="property-address clearfix">

				<div class="pull-left"><?php _e('Address:','opallisting'); ?>
					<?php echo $property->get_address(); ?> 
					<?php if( $property->latitude && $property->longitude ) : ?>
					<span class="property-view-map"><a href="<?php echo $property->get_google_map_link(); ?>" rel="nofollow" target="_blank"> 
						<i class="fa fa-map-marker"></i></a>
					</span>	
					<?php endif ; ?>
				</div> 
				<div class="pull-left"><?php _e('Location:','opallisting'); ?>  <?php echo $property->render_locations(); ?></div>

			</div>	

			<div class="favorite-button  pull-left"><?php do_shortcode('[opallisting_favorite_button property_id='.get_the_ID() .']'); ?></div>
			<ul class="property-meta-list list-inline">
				<?php if( $meta ) : ?>
					<?php foreach( $meta as $key => $info ) : ?>
						<li class="property-label-<?php echo $key; ?>"><i class="icon-property-<?php echo $key; ?>"></i><span class="label-content"><?php echo apply_filters( 'opallisting'.$key.'_unit_format',  trim($info['value']) ); ?></span> <span class="label-property"><?php echo $info['label']; ?></span></li>
					<?php endforeach; ?>
				<?php endif; ?>
			</ul>

		</div>	
	
	</header>		
	
	<?php
		/**
		 * opallisting_before_single_property_summary hook
		 *
		 * @hooked opallisting_show_product_sale_flash - 10
		 * @hooked opallisting_show_product_images - 20
		 */
		do_action( 'opallisting_single_property_preview' );
	?>

	<div class="summary entry-summary">
		<div class="row">
			<div class="col-lg-4 col-md-5">
				<?php
					/**
					 * opallisting_single_property_summary hook
					 *
					 * @hooked opallisting_template_single_title - 5
					 * @hooked opallisting_template_single_rating - 10
					 * @hooked opallisting_template_single_price - 10
					 * @hooked opallisting_template_single_excerpt - 20
					 * @hooked opallisting_template_single_add_to_cart - 30
					 * @hooked opallisting_template_single_meta - 40
					 * @hooked opallisting_template_single_sharing - 50
					 */
					//do_action( 'opallisting_single_property_summary' );
				?>		
			</div>
			<div class="col-lg-8 col-md-7">	
				<?php 
					echo opallisting_property_content();
				?>
				
			</div>
		</div>
		<div class="content-bottom">
			<div class="row">
				<div class="col-lg-4 col-md-5">
					<h6 class="pull-right"><?php _e("Share this property:", "opallisting"); ?></h6>
				</div>
				<div class="col-lg-8 col-md-7">	
					<?php do_action( 'opallisting_single_content_bottom' ); ?>
				</div>
			</div>
		</div>
	</div><!-- .summary -->
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

	<?php 
		/**
		 * opallisting_after_single_property_summary hook
		 *
		 * @hooked opallisting_output_product_data_tabs - 10
		 * @hooked opallisting_upsell_display - 15
		 * @hooked opallisting_output_related_products - 20
		 */
		do_action( 'opallisting_after_single_property_summary' );

	?>
	<div class="clear clearfix"></div>

</article><!-- #post-## -->

