<?php
global $post;

$limit = apply_filters( 'opallisting_agent_properties_limit', 5 );
$query = Opallisting_Query::get_agent_property( null, get_the_ID(), $limit );

if( $query->have_posts() ) :
?>
<div class="clearfix clear"></div>
<div class="opallisting-box property-same-agent-section">
	<h3><?php echo sprintf( __('My Properties'),  $query->found_posts  );?></h3>
	<div class="box-content opallisting-rows">
		<div class="row">
			<?php while( $query->have_posts() ) : $query->the_post(); ?>
			  	<div class="col-lg-12 col-md-12 col-sm-12">
			  	 <?php echo Opallisting_Template_Loader::get_template_part( 'content-property-list' ); ?>
			  	</div> 
			<?php endwhile; ?>	
		</div>
		<?php if( $query->max_num_pages > 1 ): ?>
		<div class="w-pagination"><?php opallisting_pagination(  $query->max_num_pages ); ?></div>
		<?php endif; ?>

	</div>	
</div>	
<?php endif; 
	wp_reset_postdata();
?>
