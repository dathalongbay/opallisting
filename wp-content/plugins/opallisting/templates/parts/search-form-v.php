<?php
	$fields = OpalListing_Search::get_setting_search_fields( '_v' );
	$slocation  = isset($_GET['location'])?$_GET['location']:opallisting_get_session_location_val();
	$stypes 	= isset($_GET['types'])?$_GET['types']:0;
	$sstatus 	= isset($_GET['status'])?$_GET['status']:0;

	$search_min_price = isset($_GET['min_price']) ? $_GET['min_price'] : opallisting_options( 'search_min_price',0 );
	$search_max_price = isset($_GET['max_price']) ? $_GET['max_price'] : opallisting_options( 'search_max_price',10000000 );
?>

<form id="opallisting-search-form-v" class="opallisting-search-form" action="<?php echo opallisting_get_search_link(); ?>" method="get">
			<div class="row">
				<div class="col-md-12"> 

				


					<ul class="list-inline pull-left">
						<li><i class="fa fa-search"></i></li>
					</ul>
					<?php 
						$statuses = Opallisting_Taxonomy_Status::getList();
		 				if( $statuses ): 
					?>
					<ul class="list-inline clearfix list-property-status pull-left">
						<li class="status-item active" data-id="-1">	
							<span><?php _e( 'Search Properties', 'opallisting' ); ?></span>
						</li>	
					</ul>	
					<?php endif; ?>
				</div>
			</div>	
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">   
						<label><?php _e("Keyword", 'opallisting'); ?></label>
						<input class="form-control" name="search_text">			 
					</div>

					<div class="form-group">
						<label><?php _e("Status", 'opallisting'); ?></label>
						<?php Opallisting_Taxonomy_Status::dropdownList( $sstatus );?>
					</div>


					<div class="form-group">
						<label><?php _e("Location", 'opallisting'); ?></label>
						<?php Opallisting_Taxonomy_Location::dropdownList( $slocation );?>
					</div>

					<div class="form-group">
						<label><?php _e("Type", 'opallisting'); ?></label>
						<?php  Opallisting_Taxonomy_Type::dropdownList( $stypes ); ?>
					</div>

					<?php if( $fields ): ?>
						<?php foreach( $fields as $key => $label ):  ?>
						<div class="form-group">
							<label><?php echo $label; ?></label>
							<?php opallisting_property_render_field_template( $key, __("No . ", 'opallisting' ) . $label ); ?>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>

					<div class="form-group">
						 <div class="cost-price-content">
							<?php 

						 	 	$data = array(
									'id' 	 => 'price',
									'unit'   => '$ ',
									'ranger_min' => opallisting_options( 'search_min_price',0 ),
									'ranger_max' =>  opallisting_options( 'search_max_price',10000000 ),
									'input_min'  =>  $search_min_price,
									'input_max'	 => $search_max_price
								);
								opallisting_property_slide_ranger_template( __("Price:",'opallisting'), $data );

							?>
						</div>
					</div>

					<div class="form-group">
						 <div class="area-range-content">
							<?php echo opallisting_property_areasize_field_template(); ?>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-danger btn-lg btn-search btn-block">
							<?php _e('Search', 'opallisting'); ?>
						</button>
					</div>

				</div>
				
			</div>	
		</form>

