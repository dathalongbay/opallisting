<?php 
	$fields = OpalListing_Search::get_setting_search_fields();
	$slocation  = isset($_GET['location'])?$_GET['location']: opallisting_get_session_location_val();
	$stypes 	= isset($_GET['types'])?$_GET['types']:-1;
	$sstatus 	= isset($_GET['status'])?$_GET['status']:-1;

	$search_min_price = isset($_GET['min_price']) ? $_GET['min_price'] :  opallisting_options( 'search_min_price',0 );
	$search_max_price = isset($_GET['max_price']) ? $_GET['max_price'] : opallisting_options( 'search_max_price',10000000 );

?>
<form id="opallisting-search-form" class="opallisting-search-form" action="<?php echo opallisting_get_search_link(); ?>" method="get">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3">
			<h3><?php echo isset($title) ? $title : _e( 'Quick Search', 'opallisting' ); ?></h3>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-9">
			<?php
				$statuses = Opallisting_Taxonomy_Status::getList();
 				if( $statuses ):
			?>
			<ul class="list-inline clearfix list-property-status">
				<li class="status-item  <?php if( $sstatus == -1 ): ?> active<?php endif; ?>" data-id="-1">
					<span><?php _e( 'All', 'opallisting' ); ?></span>
				</li>
				<?php foreach( $statuses as $status ): ?>

				<li class="status-item <?php if( $sstatus==$status->slug): ?> active<?php endif; ?>" data-id="<?php echo $status->slug; ?>">
					<span><?php echo $status->name; ?> </span>
				</li>
				<?php endforeach; ?>
			</ul>
			<input type="hidden" value="<?php echo $sstatus; ?>" name="status" />
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10 col-md-10 col-sm-10">
			<div class="row">
				<!-- <div class="col-lg-3 col-md-3 col-sm-3">
					<label><?php //_e("Keyword", 'opallisting'); ?></label>
					<input class="form-control" name="search_text">
				</div> -->

				<div class="col-lg-4 col-md-4 col-sm-4">
					<label><?php _e("Location", 'opallisting'); ?></label>
					<?php Opallisting_Taxonomy_Location::dropdownList( $slocation );?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8">
					<div class="row">
						<?php if( $fields ): ?>
							<?php foreach( $fields as $key => $label ): ?>
							<?php if( $key == "areasize" ) :  continue; ; endif; ?>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<label><?php echo $label ?></label>
								<?php opallisting_property_render_field_template( $key, __("No . ", 'opallisting' ) . $label ); ?>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4">
					<label><?php _e("Type", 'opallisting'); ?></label>
					<?php  Opallisting_Taxonomy_Type::dropdownList( $stypes ); ?>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4">
					    <?php

					 	 	$data = array(
								'id' 	 => 'price',
								'unit'   => '$ ',
								'ranger_min' => opallisting_options( 'search_min_price',0 ),
								'ranger_max' => opallisting_options( 'search_max_price',10000000 ),
								'input_min'  =>  $search_min_price,
								'input_max'	 => $search_max_price
							);
							opallisting_property_slide_ranger_template( __("Price:",'opallisting'), $data );

						?>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4">
					<?php echo opallisting_property_areasize_field_template(); ?>
				</div>

			</div>
		</div>
		<div class="col-lg-2 col-md-2  col-sm-2">
			<button type="submit" class="btn btn-danger btn-lg btn-search">
				<?php _e('Search'); ?>
			</button>
		</div>
	</div>
</form>