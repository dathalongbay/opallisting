<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$property = opallisting_place( get_the_ID() );

global $property, $post;

$meta   = $property->get_meta_shortinfo();

?>
<article itemscope itemtype="http://schema.org/Property" <?php post_class(); ?>><div class="property-list-style">
	<?php do_action( 'opallisting_before_property_loop_item' ); ?>
	<div class="property-list container-cols-2">
			<header>
				<?php echo $property->render_statuses(); ?>

				<?php opallisting_get_loop_thumbnail(); ?>
				<?php do_shortcode('[opallisting_favorite_button property_id='.get_the_ID() .']'); ?>
				 
				<div class="property-meta">
			     	 <ul class="property-meta-list list-inline">
						<?php if( $meta ) : ?>
							<?php foreach( $meta as $key => $info ) : ?>
								<li class="property-label-<?php echo $key; ?>"><i class="icon-property-<?php echo $key; ?>"></i><span class="label-property"><?php echo $info['label']; ?></span> <span class="label-content"><?php echo apply_filters( 'opallisting'.$key.'_unit_format',  trim($info['value']) ); ?></span></li>
							<?php endforeach; ?>
						<?php endif; ?>
					</ul>	
			     </div>	

			</header>
				<div class="abs-col-item">
					<div class="entry-content">
						
						<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
							
					  	<div class="property-address">
							<?php echo $property->get_address(); ?>
						</div>

							<div class="property-abs-bottom">
							 	<div class="property-meta-bottom">
									<div class="property-meta">
										<?php echo apply_filters( 'opallisting_areasize_unit_format', $property->get_metabox_value( 'areasize' ) );?>
									</div>	
									<div class="property-price">
										<span><?php echo  opallisting_price_format( $property->get_price() ); ?></span>

										<?php if( $property->get_sale_price() ): ?>
										<span class="property-saleprice">
											<?php echo  opallisting_price_format( $property->get_sale_price() ); ?>
										</span>
										<?php endif; ?>

										<?php if( $property->get_price_label() ): ?>
										<span class="property-price-label">
											/ <?php echo $property->get_price_label(); ?>
										</span>	
										<?php endif; ?>
									</div>
								</div>

								<div class="entry-content-bottom clearfix">
									<div class="pull-left">
									<?php echo $property->render_author_link(); ?>
									</div>
									<div class="pull-right">
										<a class="btn-view-more text-primary" href="<?php the_permalink(); ?>"><i class="fa fa-angle-right"></i></a>
									</div>
								</div>
							</div>	

					</div><!-- .entry-content -->
	 			</div>
	</div>	

	<?php do_action( 'opallisting_after_property_loop_item' ); ?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div></article><!-- #post-## -->
