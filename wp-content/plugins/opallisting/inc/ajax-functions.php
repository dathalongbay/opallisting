<?php 
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if (!function_exists('opallisting_process_send_email')) {
	function opallisting_process_send_email() {

		$name = esc_html( $_POST['name'] );
		$email = esc_html( $_POST['email'] );
		$message = esc_html( $_POST['message'] );
		$post_id = absint( $_POST['post_id'] );
		$agent_id = esc_html( $_POST['agent_id'] );

		$subject = opallisting_get_option('sent_contact_form_email_subject');
		$headers = sprintf( "From: %s <%s>\r\n Content-type: text/html", $name, $email );

		$agent = get_post( $agent_id );
		$agent_email = get_post_meta( $agent_id, OPALLISTING_AGENT_PREFIX . 'email', true );
		$receive_name = $agent->name;

		if ( !empty($post_id) ) {
			$property_link = get_permalink( $post_id );
		} else {
			$property_link = '';
		}
		$tags = array("{receive_name}", "{name}", "{email}", "{property_link}", "{message}");
		$values = array($receive_name, $name, $email, $property_link, $message);

		$body = opallisting_get_option('sent_contact_form_email_body');
		$body = html_entity_decode($body);
		$message = str_replace($tags, $values, $body);

		$status = wp_mail( $agent_email, $subject, $message, $headers );

		if ( ! empty( $status ) && 1 == $status ) {
			$return = array( 'status' => 'success', 'msg' => __( 'Message has been successfully sent.', 'opallisting' ) );
		} else {
			$return = array( 'status' => 'danger', 'msg' => __( 'Unable to send a message.', 'opallisting' ) );
		}

		echo json_encode($return); die();
	}
}

add_action( 'wp_ajax_send_email_contact', 'opallisting_process_send_email' );
add_action( 'wp_ajax_nopriv_send_email_contact', 'opallisting_process_send_email' );

/* set feature property */
add_action( 'wp_ajax_opallisting_set_feature_property', 'opallisting_set_feature_property' );
add_action( 'wp_ajax_nopriv_opallisting_set_feature_property', 'opallisting_set_feature_property' );
if ( ! function_exists( 'opallisting_set_feature_property' ) ) {
	function opallisting_set_feature_property() {

		if ( ! isset( $_REQUEST['nonce'] ) && ! wp_verify_nonce( $_REQUEST['nonce'], 'nonce' ) ) return;
		if ( ! isset( $_REQUEST['property_id'] ) ) return;
		update_post_meta( absint( $_REQUEST['property_id'] ), OPALLISTING_PROPERTY_PREFIX . 'featured', 1 );

		wp_redirect( admin_url( 'edit.php?post_type=opallisting_place' ) ); exit();
	}
}
/* remove feature property */
add_action( 'wp_ajax_opallisting_remove_feature_property', 'opallisting_remove_feature_property' );
add_action( 'wp_ajax_nopriv_opallisting_remove_feature_property', 'opallisting_remove_feature_property' );
if ( ! function_exists( 'opallisting_remove_feature_property' ) ) {
	function opallisting_remove_feature_property() {
		if ( ! isset( $_REQUEST['nonce'] ) && ! wp_verify_nonce( $_REQUEST['nonce'], 'nonce' ) ) return;

		if ( ! isset( $_REQUEST['property_id'] ) ) return;

		update_post_meta( absint( $_REQUEST['property_id'] ), OPALLISTING_PROPERTY_PREFIX . 'featured', '' );
		wp_redirect( admin_url( 'edit.php?post_type=opallisting_place' ) ); exit();
	}
}

/**
 * Set Featured Item Following user
 */
add_action( 'wp_ajax_opallisting_toggle_featured_property', 'opallisting_toggle_featured_property' );
add_action( 'wp_ajax_nopriv_opallisting_toggle_featured_property', 'opallisting_toggle_featured_property' );

function opallisting_toggle_featured_property(){
	 	
 	global $current_user;
    wp_get_current_user();
    $user_id =   $current_user->ID;

    $property_id = intval( $_POST['property_id'] );
    $post = get_post( $property_id );

    if( $post->post_author == $user_id ) {
   
      	$check = apply_filters( 'opallisting_set_feature_property_checked', false );
        if( $check ) {
            do_action( 'opallisting_toggle_featured_property_before', $user_id, $property_id );
            update_post_meta( $property_id, OPALLISTING_PROPERTY_PREFIX . 'featured', 1 );
            echo json_encode( array( 'status' => true, 'msg' => __('Could not set this as featured','opallisting') ) );
            wp_die();
        } 
    }  

    echo json_encode( array( 'status' => false, 'msg' => __('Could not set this as featured','opallisting') ) );
    wp_reset_query();
    wp_die();
 
}
