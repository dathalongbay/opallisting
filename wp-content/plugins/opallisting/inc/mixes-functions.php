<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

function opallisting_get_current_uri(){
    global $wp;
    $current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
    return $current_url;
}

function opallisting_search_agent_uri(){
     global $opallisting_options;

    $search_agents = isset( $opallisting_options['search_agents'] ) ? get_permalink( absint( $opallisting_options['search_agents'] ) ) : opallisting_get_current_uri();

    return apply_filters( 'opallisting_get_search_agents_page_uri', $search_agents );
}
function opallisting_get_session_location_val(){
    return isset($_SESSION['set_location']) ?$_SESSION['set_location']:0;  
}


function opallisting_get_location_active(){
    $location = opallisting_get_session_location_val();
    if( !is_numeric($location) ){
        $term = get_term_by('slug', $location, 'opallisting_location');
        $name = is_object($term)?$term->name:__( 'Your Location', 'opallisting' );

        return $name;
    }else { 
        return __( 'Your Location', 'opallisting' );
    }
}

 
function opallisting_get_upload_image_url( $attach_data ) {
    $upload_dir         =   wp_upload_dir();
    $image_path_array   =   explode( '/', $attach_data['file'] );
    $image_path_array   =   array_slice( $image_path_array, 0, count( $image_path_array ) - 1 );
    $image_path         =   implode( '/', $image_path_array );
    $thumbnail_name     =   null;
    if ( isset( $attach_data['sizes']['user-image'] ) ) {
        $thumbnail_name     =   $attach_data['sizes']['user-image']['file'];
    } elseif( isset($attach_data['sizes']['thumbnail']['file']) ) {
        $thumbnail_name     =   $attach_data['sizes']['thumbnail']['file'];
    }else {
        return $upload_dir['baseurl'] . '/' .$attach_data['file'];
    }
    return $upload_dir['baseurl'] . '/' . $image_path . '/' . $thumbnail_name ;
}

/**
 *
 */
function opallisting_clean_attachments( $user_id ){
    
  
    $query = new WP_Query( 
        array( 
            'post_type'   => 'attachment', 
            'post_status' => 'inherit', 
            'author'      => $user_id , 
            'meta_query' => array(
                array(
                    'key' => '_pending_to_use',
                     'value' => 1,
                     'compare' => '>=',
                )
            )    
        ) 
    );

    if( $query->have_posts() ){   
        while( $query->have_posts() ){ $query->the_post();
            wp_delete_attachment( get_the_ID() );
        }
    }
    wp_reset_postdata(); 
}

function opallisting_update_attachement_used( $attachment_id ){

}

/**
 * batch including all files in a path.
 *
 * @param String $path : PATH_DIR/*.php or PATH_DIR with $ifiles not empty
 */
function opallisting_includes( $path, $ifiles=array() ){

    if( !empty($ifiles) ){
         foreach( $ifiles as $key => $file ){
            $file  = $path.'/'.$file;
            if(is_file($file)){
                require($file);
            }
         }
    }else {
        $files = glob($path);
        foreach ($files as $key => $file) {
            if(is_file($file)){
                require($file);
            }
        }
    }
}

/**
 *
 */
function opallisting_place( $id ){
    global $property;

    $property = new Opallisting_Property( $id );

    return $property;
}

function opallisting_options( $key, $default = '' ){

    global $opallisting_options;

    $value =  isset($opallisting_options[ $key ]) ? $opallisting_options[ $key ] : $default;
    $value = apply_filters( 'opallisting_option_', $value, $key, $default );

    return apply_filters( 'opallisting_option_' . $key, $value, $key, $default );
}

function opallisting_get_image_placeholder( $size, $url=false ){

    global $_wp_additional_sizes;

    $width  = 0;
    $height = 0;
    $img_text   = get_bloginfo('name');

    if ( in_array( $size , array( 'thumbnail', 'medium', 'large' ) ) ) {
        $width = get_option( $size . '_size_w' );
        $height = get_option( $size . '_size_h' );

    } elseif ( isset( $_wp_additional_sizes[$size] ) ) {

        $width = $_wp_additional_sizes[ $size ]['width'];
        $height = $_wp_additional_sizes[ $size ]['height'];
    }

    if( intval( $width ) > 0 && intval( $height ) > 0 ) {
        if( $url ){
           return 'http://placehold.it/' . $width . 'x' . $height . '&text=' . urlencode( $img_text ) ;
        }
        return '<img src="http://placehold.it/' . $width . 'x' . $height . '&text=' . urlencode( $img_text ) . '" />';
    }
}


/**
 *
 */
function opallisting_get_currencies() {
    $currencies = array(
        'USD'  => __( 'US Dollars (&#36;)', 'give' ),
        'EUR'  => __( 'Euros (&euro;)', 'give' ),
        'GBP'  => __( 'Pounds Sterling (&pound;)', 'give' ),
        'AUD'  => __( 'Australian Dollars (&#36;)', 'give' ),
        'BRL'  => __( 'Brazilian Real (R&#36;)', 'give' ),
        'CAD'  => __( 'Canadian Dollars (&#36;)', 'give' ),
        'CZK'  => __( 'Czech Koruna', 'give' ),
        'DKK'  => __( 'Danish Krone', 'give' ),
        'HKD'  => __( 'Hong Kong Dollar (&#36;)', 'give' ),
        'HUF'  => __( 'Hungarian Forint', 'give' ),
        'ILS'  => __( 'Israeli Shekel (&#8362;)', 'give' ),
        'JPY'  => __( 'Japanese Yen (&yen;)', 'give' ),
        'MYR'  => __( 'Malaysian Ringgits', 'give' ),
        'MXN'  => __( 'Mexican Peso (&#36;)', 'give' ),
        'NZD'  => __( 'New Zealand Dollar (&#36;)', 'give' ),
        'NOK'  => __( 'Norwegian Krone (Kr.)', 'give' ),
        'PHP'  => __( 'Philippine Pesos', 'give' ),
        'PLN'  => __( 'Polish Zloty', 'give' ),
        'SGD'  => __( 'Singapore Dollar (&#36;)', 'give' ),
        'SEK'  => __( 'Swedish Krona', 'give' ),
        'CHF'  => __( 'Swiss Franc', 'give' ),
        'TWD'  => __( 'Taiwan New Dollars', 'give' ),
        'THB'  => __( 'Thai Baht (&#3647;)', 'give' ),
        'INR'  => __( 'Indian Rupee (&#8377;)', 'give' ),
        'TRY'  => __( 'Turkish Lira (&#8378;)', 'give' ),
        'RIAL' => __( 'Iranian Rial (&#65020;)', 'give' ),
        'RUB'  => __( 'Russian Rubles', 'give' )
    );

    return apply_filters( 'opallisting_currencies', $currencies );
}

/**
 * Get the price format depending on the currency position
 *
 * @return string
 */
function opallisting_price_format_position() {
    global $opallisting_options;
    $currency_pos = opallisting_options('currency_position','before');

    $format = '%1$s%2$s';
    switch ( $currency_pos ) {
        case 'before' :
            $format = '%1$s%2$s';
        break;
        case 'after' :
            $format = '%2$s%1$s';
        break;
        case 'left_space' :
            $format = '%1$s&nbsp;%2$s';
        break;
        case 'right_space' :
            $format = '%2$s&nbsp;%1$s';
        break;
    }

    return apply_filters( 'opallisting_price_format_position', $format, $currency_pos );
}

/**
 *
 */
function opallisting_price_format( $price, $args=array() ){

    $price = opallisting_price( $price , $args );
    $price = sprintf( opallisting_price_format_position(), opallisting_currency_symbol(), $price );

    return apply_filters( 'opallisting_price_format', $price );
}

function opallisting_get_currency( ){
    return opallisting_options( 'currency', 'USD' );
}

/**
 *
 */
function opallisting_currency_symbol( $currency = '' ) {
    if ( ! $currency ) {
        $currency = opallisting_get_currency();
    }

    switch ( $currency ) {
        case 'AED' :
            $currency_symbol = 'د.إ';
            break;
        case 'BDT':
            $currency_symbol = '&#2547;&nbsp;';
            break;
        case 'BRL' :
            $currency_symbol = '&#82;&#36;';
            break;
        case 'BGN' :
            $currency_symbol = '&#1083;&#1074;.';
            break;
        case 'AUD' :
        case 'CAD' :
        case 'CLP' :
        case 'COP' :
        case 'MXN' :
        case 'NZD' :
        case 'HKD' :
        case 'SGD' :
        case 'USD' :
            $currency_symbol = '&#36;';
            break;
        case 'EUR' :
            $currency_symbol = '&euro;';
            break;
        case 'CNY' :
        case 'RMB' :
        case 'JPY' :
            $currency_symbol = '&yen;';
            break;
        case 'RUB' :
            $currency_symbol = '&#1088;&#1091;&#1073;.';
            break;
        case 'KRW' : $currency_symbol = '&#8361;'; break;
            case 'PYG' : $currency_symbol = '&#8370;'; break;
        case 'TRY' : $currency_symbol = '&#8378;'; break;
        case 'NOK' : $currency_symbol = '&#107;&#114;'; break;
        case 'ZAR' : $currency_symbol = '&#82;'; break;
        case 'CZK' : $currency_symbol = '&#75;&#269;'; break;
        case 'MYR' : $currency_symbol = '&#82;&#77;'; break;
        case 'DKK' : $currency_symbol = 'kr.'; break;
        case 'HUF' : $currency_symbol = '&#70;&#116;'; break;
        case 'IDR' : $currency_symbol = 'Rp'; break;
        case 'INR' : $currency_symbol = 'Rs.'; break;
        case 'NPR' : $currency_symbol = 'Rs.'; break;
        case 'ISK' : $currency_symbol = 'Kr.'; break;
        case 'ILS' : $currency_symbol = '&#8362;'; break;
        case 'PHP' : $currency_symbol = '&#8369;'; break;
        case 'PLN' : $currency_symbol = '&#122;&#322;'; break;
        case 'SEK' : $currency_symbol = '&#107;&#114;'; break;
        case 'CHF' : $currency_symbol = '&#67;&#72;&#70;'; break;
        case 'TWD' : $currency_symbol = '&#78;&#84;&#36;'; break;
        case 'THB' : $currency_symbol = '&#3647;'; break;
        case 'GBP' : $currency_symbol = '&pound;'; break;
        case 'RON' : $currency_symbol = 'lei'; break;
        case 'VND' : $currency_symbol = '&#8363;'; break;
        case 'NGN' : $currency_symbol = '&#8358;'; break;
        case 'HRK' : $currency_symbol = 'Kn'; break;
        case 'EGP' : $currency_symbol = 'EGP'; break;
        case 'DOP' : $currency_symbol = 'RD&#36;'; break;
        case 'KIP' : $currency_symbol = '&#8365;'; break;
        default    : $currency_symbol = ''; break;
    }

    return apply_filters( 'opallisting_currency_symbol', $currency_symbol, $currency );
}

/**
 * Return the thousand separator for prices
 * @since  2.3
 * @return string
 */
function opallisting_get_price_thousand_separator() {
    $separator = stripslashes( opallisting_options( 'thousands_separator' ) );
    return $separator;
}

/**
 * Return the decimal separator for prices
 * @since  2.3
 * @return string
 */
function opallisting_get_price_decimal_separator() {
    $separator = stripslashes( opallisting_options( 'decimal_separator' ,'.') );
    return $separator ? $separator : '.';
}

/**
 * Return the number of decimals after the decimal point.
 * @since  2.3
 * @return int
 */
function opallisting_get_price_decimals() {
    return absint( opallisting_options( 'price_num_decimals', 2 ) );
}


/**
 *
 */
function opallisting_price( $price, $args=array() ){

    $negative = $price < 0;

    if( $negative ) {
        $price = substr( $price, 1 );
    }


    extract( apply_filters( 'opallisting_price_args', wp_parse_args( $args, array(
        'ex_tax_label'       => false,
        'decimal_separator'  => opallisting_get_price_decimal_separator(),
        'thousand_separator' => opallisting_get_price_thousand_separator(),
        'decimals'           => opallisting_get_price_decimals(),

    ) ) ) );

    $negative        = $price < 0;
    $price           = apply_filters( 'opallisting_raw_price', floatval( $negative ? $price * -1 : $price ) );
    $price           = apply_filters( 'opallisting_formatted_price', number_format( $price, $decimals, $decimal_separator, $thousand_separator ), $price, $decimals, $decimal_separator, $thousand_separator );

    return $price;
}

/**
 *
 *  Applyer function to show unit for property
 */

function opallisting_areasize_unit_format( $value='' ){
    return  $value . ' ' . '<span>'.'m2'.'</span>';
}

add_filter( 'opallisting_areasize_unit_format', 'opallisting_areasize_unit_format' );

/**
 *
 *  Applyer function to show unit for property
 */
if(!function_exists('opallisting_fnc_excerpt')){
    //Custom Excerpt Function
    function opallisting_fnc_excerpt($limit,$afterlimit='[...]') {
        $excerpt = get_the_excerpt();
        if( $excerpt != ''){
           $excerpt = explode(' ', strip_tags( $excerpt ), $limit);
        }else{
            $excerpt = explode(' ', strip_tags(get_the_content( )), $limit);
        }
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).' '.$afterlimit;
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return strip_shortcodes( $excerpt );
    }
}

/**
 *
 */
function opallisting_is_own_property( $post_id, $user_id ){
    $post = get_post( $post_id );
    wp_reset_postdata();
    if( ! is_object($post)  || ! $post->ID ){
        return false;
    }
    return $user_id == $post->post_author;
}

function opallisting_pagination($pages = '', $range = 2 ) {
    global $paged;

    if(empty($paged))$paged = 1;

    $prev = $paged - 1;
    $next = $paged + 1;
    $showitems = ( $range * 2 )+1;
    $range = 2; // change it to show more links

    if( $pages == '' ){
        global $wp_query;

        $pages = $wp_query->max_num_pages;
        if( !$pages ){
            $pages = 1;
        }
    }

    if( 1 != $pages ){

        echo '<div class="pbr-pagination pagination-main">';
            echo '<ul class="pagination">';
                echo ( $paged > 2 && $paged > $range+1 && $showitems < $pages ) ? '<li><a aria-label="First" href="'.get_pagenum_link(1).'"><span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span></a></li>' : '';
                echo ( $paged > 1 ) ? '<li><a aria-label="'.__('Previous','opallisting').'" href="'.get_pagenum_link($prev).'"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>' : '<li class="disabled"><a aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>';
                for ( $i = 1; $i <= $pages; $i++ ) {
                    if ( 1 != $pages &&( !( $i >= $paged+$range+1 || $i <= $paged-$range-1 ) || $pages <= $showitems ) )
                    {
                        if ( $paged == $i ){
                            echo '<li class="active"><a href="'.get_pagenum_link($i).'">'.$i.' <span class="sr-only"></span></a></li>';
                        } else {
                            echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
                        }
                    }
                }
                echo ( $paged < $pages ) ? '<li><a aria-label="'.__('Next','opallisting').'" href="'.get_pagenum_link($next).'"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>' : '';
                echo ( $paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages ) ? '<li><a aria-label="Last" href="'.get_pagenum_link( $pages ).'"><span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span></a></li>' : '';
            echo '</ul>';
        echo '</div>';

    }
}

if ( ! function_exists( 'opallisting_insert_user_agent' ) ) {

    function opallisting_insert_user_agent( $args = array() ) {
        $userdata = wp_parse_args( $args, array(
            'first_name'    => '',
            'last_name'     => '',
            'avatar'    => '',
            'job'       => '',
            'email'     => '',
            'phone'     => '',
            'mobile'    => '',
            'fax'       => '',
            'web'       => '',
            'address'   => '',
            'twitter'   => '',
            'facebook'  => '',
            'google'    => '',
            'linkedin'  => '',
            'instagram' => '',
        ) );

        $agent_id = wp_insert_post( array(
                'post_title'    => $args['first_name'] && $args['last_name'] ? $args['first_name'] . ' ' . $args['last_name'] : $userdata['email'],
                'post_content'  => 'empty description',
                'post_excerpt'  => 'empty excerpt',
                'post_type'     => 'opallisting_agent',
                'post_status'   => 'publish',
                'post_author'   => 1
            ), true );

        foreach ( $userdata as $key => $value ) {
            if ( in_array( $key, array( 'first_name', 'last_name' ) ) ) {
                continue;
            }
            update_post_meta( $agent_id, OPALLISTING_AGENT_PREFIX . $key, $value );
        }
        do_action( 'opallisting_insert_user_agent', $agent_id );
        return $agent_id;
    }
}


if(!function_exists('opallisting_get_listing_types')){

    function opallisting_get_listing_types(){

        $listing_types_array = array();
        $args = array(
            'post_type' => 'opallisting_type',
            'posts_per_page' => '-1'
        );

        $listing_types = get_posts($args);

        if (is_array($listing_types) && count($listing_types)) {

            foreach ($listing_types as $listing_type) {

                $listing_types_array[$listing_type->ID] = $listing_type->post_title;

            }

        }
        return $listing_types_array;

    }

}