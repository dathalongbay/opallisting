<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class Opallisting_Field_Listing_Type {

    /**
     * Current version number
     */
    const VERSION = '1.0.0';

    /**
     * Initialize the plugin by hooking into CMB2
     */
    public static function init() {
        add_filter( 'cmb2_render_opal_listing_type', array( __CLASS__, 'render_listing_type' ), 10, 5 );
        add_filter( 'cmb2_sanitize_opal_listing_type', array( __CLASS__, 'sanitize_listing_type' ), 10, 4 );
    }


    public static function setup_admin_scripts() {

        wp_enqueue_script( 'opallisting-listing-data-type-js', plugins_url( 'script.js', __FILE__ ), array( 'jquery') );
        wp_enqueue_style( 'opallisting-listing-data-type-css', plugins_url( 'style.css', __FILE__ ), array( 'jquery') );

    }

    public static function render_listing_type($field, $escaped_value, $object_id, $object_type, $field_type_object){
        self::setup_admin_scripts();

        //global $post;

        //$custom_fields = get_post_meta($post->ID, 'opal_listing_type_custom_fields', true);

        $custom_fields = get_post_meta(111, 'opal_listing_type_custom_fields', true);

        $listing_options = opallisting_get_listing_types();

        $prefix = 'aaa';
        $fields = array(
            array(
                'name'    => 'Test Text',
                'desc'    => 'field description (optional)',
                'default' => 'standard value (optional)',
                'id'      => 'wiki_test_text',
                'type'    => 'text',
            )

        );

        add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );

        return apply_filters( 'opallisting_postype_property_metaboxes_specifics_fields' , $fields );

    }

    public static function metaboxes(){
        $prefix = OPALLISTING_PROPERTY_PREFIX;

        $metaboxes[ $prefix . 'specific_fields' ] = array(
            'id'                        => $prefix . 'specific_fields',
            'title'                     => __( 'Listing type specific fields', 'opallisting' ),
            'object_types'              => array( 'opallisting_place' ),
            'context'                   => 'normal',
            'priority'                  => 'high',
            'show_names'                => true,
            'fields'                    => self::metaboxes_specifics_fields()
        );

        return $metaboxes;
    }

    public static function metaboxes_specifics_fields(){



    }



    public static function sanitize_listing_type($override_value, $value, $object_id, $field_args){

        return $value;

    }

}


Opallisting_Field_Listing_Type::init();