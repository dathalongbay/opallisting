<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if( class_exists("WPBakeryVisualComposerAbstract") ){
    function opallisting_vc_get_term_object( $term ) {
		$vc_taxonomies_types = vc_taxonomies_types();

		return array(
			'label' => $term->name,
			'value' => $term->slug,
			'group_id' => $term->taxonomy,
			'group' => isset( $vc_taxonomies_types[ $term->taxonomy ], $vc_taxonomies_types[ $term->taxonomy ]->labels, $vc_taxonomies_types[ $term->taxonomy ]->labels->name ) ? $vc_taxonomies_types[ $term->taxonomy ]->labels->name : esc_html__( 'Taxonomies', 'mode' ),
		);
	}

	function opallisting_category_field_search( $search_string ) {
		$data = array();
		$vc_taxonomies_types = array('property_category');
		$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
			'hide_empty' => false,
			'search' => $search_string
		) );
		if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
			foreach ( $vc_taxonomies as $t ) {
				if ( is_object( $t ) ) {
					$data[] = opallisting_vc_get_term_object( $t );
				}
			}
		}

		return $data;
	}

	function opallisting_category_render($query) {
		$category = get_term_by('slug', $query['value'], 'property_category');
		if ( ! empty( $query ) && !empty($category)) {
			$data = array();
			$data['value'] = $category->slug;
			$data['label'] = $category->name;
			return ! empty( $data ) ? $data : false;
		}
		return false;
	}

	function opallisting_location_field_search( $search_string ) {

		$data = array();
		$vc_taxonomies_types = array('opallisting_location');
		$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
			'hide_empty' => false,
			'search' => $search_string
		) );

		if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
			foreach ( $vc_taxonomies as $t ) {
				if ( is_object( $t ) ) {
					$data[] = opallisting_vc_get_term_object( $t );
				}
			}
		}

		return $data;
	}

	function opallisting_location_render($query) {
		$category = get_term_by('slug', $query['value'], 'opallisting_location');
		if ( ! empty( $query ) && !empty($category)) {
			$data = array();
			$data['value'] = $category->slug;
			$data['label'] = $category->name;
			return ! empty( $data ) ? $data : false;
		}
		return false;
	}

	function opallisting_types_field_search( $search_string ) {
		$data = array();
		$vc_taxonomies_types = array('opallisting_types');
		$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
			'hide_empty' => false,
			'search' => $search_string
		) );
		if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
			foreach ( $vc_taxonomies as $t ) {
				if ( is_object( $t ) ) {
					$data[] = opallisting_vc_get_term_object( $t );
				}
			}
		}

		return $data;
	}

	function opallisting_types_render($query) {
		$category = get_term_by('slug', $query['value'], 'opallisting_types');
		if ( ! empty( $query ) && !empty($category)) {
			$data = array();
			$data['value'] = $category->slug;
			$data['label'] = $category->name;
			return ! empty( $data ) ? $data : false;
		}
		return false;
	}

	$shortcodes = array( 'pbr_estate_filter_property' );

	foreach( $shortcodes as $shortcode ){

		add_filter( 'vc_autocomplete_'.$shortcode .'_property_category_callback', 'opallisting_category_field_search', 10, 1 );
	 	add_filter( 'vc_autocomplete_'.$shortcode .'_property_category_render', 'opallisting_category_render', 10, 1 );

	 	add_filter( 'vc_autocomplete_'.$shortcode .'_opallisting_location_callback', 'opallisting_location_field_search', 10, 1 );
	 	add_filter( 'vc_autocomplete_'.$shortcode .'_opallisting_location_render', 'opallisting_location_render', 10, 1 );

	 	add_filter( 'vc_autocomplete_'.$shortcode .'_opallisting_types_callback', 'opallisting_types_field_search', 10, 1 );
	 	add_filter( 'vc_autocomplete_'.$shortcode .'_opallisting_types_render', 'opallisting_types_render', 10, 1 );
	}



	function opallisting_property_slugs_field_search( $search_string ){
		$data = array();

		$args = array(
            'post_type' 	 => 'opallisting_place',
            'post_status' 	 => 'publish',
            's'		=> $search_string
        );

		$posts = get_posts( $args );
		if ( is_array( $posts ) && ! empty( $posts ) ) {
			foreach ( $posts as $_post ) {
				$t = array(
					'label' => $_post->post_title,
					'value' => $_post->post_name
				);
				$data[] = $t;
			}
		}
		wp_reset_query();
		return $data;
	}

	function opallisting_property_slugs_render( $query ){

		$_post = get_page_by_path( $query['value'],OBJECT,'opallisting_place' );
 
		if ( ! empty( $query ) && !empty($_post)) {
			$data = array();
			$data['value'] = $_post->post_name;
			$data['label'] = $_post->post_title;
			return ! empty( $data ) ? $data : false;
		}
		return false;
	}

	add_filter( 'vc_autocomplete_pbr_estate_manual_carousel_properties_property_slugs_callback', 'opallisting_property_slugs_field_search', 10, 1 );
	add_filter( 'vc_autocomplete_pbr_estate_manual_carousel_properties_property_slugs_render', 'opallisting_property_slugs_render', 10, 1 );


	// search agents
    vc_map( array(
           
           "name" => __("Search Property Box ", "opallisting"),
           "base" => "pbr_estate_searchbox",
           "class" => "",
           "description" => 'Display form to search properties',
           "category" => __('OpalListing', "opallisting"),
           "params" => array(
        	array(
	            
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	            "admin_label" => true
	       	),
        )
    ));
    // search agents
    vc_map( array(
           
           "name" => __("Search Agents Box ", "opallisting"),
           "base" => "pbr_estate_search_agents",
           "class" => "",
           "description" => 'Display form to search agents',
           "category" => __('OpalListing', "opallisting"),
           "params" => array(
        	array(
	            
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	            "admin_label" => true
	       	),

	       	array(
	            
	            "type" => "textarea_html",
	            "heading" => __("Description", "opallisting"),
	            "param_name" => "description",

	            "admin_label" => false
	       	),
        )
    ));

    vc_map( array(
          "name" 		=> __("Featured Property", "opallisting"),
          "base" 		=> "pbr_featured_property",
          "class" 		=> "",
          "description" => 'Get data from post type Team',
          "category" 	=> __('OpalListing', "opallisting"),
          "params" => array(
        	array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	        ),
	        array(
			    'type' => 'colorpicker',
			    'heading' => esc_html__( 'Title Color', 'opallisting' ),
			    'param_name' => 'title_color',
			    'description' => esc_html__( 'Select font color', 'opallisting' )
			),
	         array(
	            "type" => "textfield",
	            "heading" => __("Description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	        ),


	        array(
	            "type" => "textfield",
	            "heading" => __("Limit", "opallisting"),
	            "param_name" => "limit",
	            "value" => 6,
	            'description' =>  __('Limit featured properties showing', 'opallisting')
	        ),
          )
      ));

	 vc_map( array(
          "name" => __("Carousel Property", "opallisting"),
          "base" => "pbr_estate_carousel_property",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
	            array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	         ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Column", "opallisting"),
	            "param_name" => "column",
	            "value" => '4',
	            'description' =>  ''
	          ),

	         array(
	            "type" => "textfield",
	            "heading" => __("Limit", "opallisting"),
	            "param_name" => "limit",
	            "value" => 6,
	            'description' =>  __('Limit featured properties showing', 'opallisting')
	        ),

	        array(
				"type" => "dropdown",
				"heading" => esc_html__("Enable Thumbnail", 'opallisting'),
				"param_name" => "enable_thumbnail",
				'value' 	=> array(
					esc_html__('Disable', 'opallisting') => 0,
					esc_html__('Enable', 'opallisting') => 1,
				),
				'std' => 0
			),
          )
      ));
	 vc_map( array(
          "name" => __("Manual Carousel Properties", "opallisting"),
          "base" => "pbr_estate_manual_carousel_properties",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
		        array(
		            "type" => "textfield",
		            "heading" => __("Title", "opallisting"),
		            "param_name" => "title",
		            "value" => '',
		              "admin_label" => true
		        ),
	          	
	          	array(
		            'type' => 'autocomplete',
		            "heading" => __("Properties", "opallisting"),
		            'param_name' => 'property_slugs',
		            "value" => '',
		              'settings' => array(
				     	'multiple' => true,
				     	'unique_values' => true,
					     // In UI show results except selected. NB! You should manually check values in backend
					    ),

		        ),
		          array(
		            "type" => "textfield",
		            "heading" => __("Column", "opallisting"),
		            "param_name" => "column",
		            "value" => 1,
		            'description' =>  ''
		          ),
		         array(
		            "type" => "textfield",
		            "heading" => __("Limit", "opallisting"),
		            "param_name" => "limit",
		            "value" => 6,
		            'description' =>  __('Limit featured properties showing', 'opallisting')
		        ),
          )
      ));

      vc_map( array(
          "name" => __("Grid Property", "opallisting"),
          "base" => "pbr_estate_grid_property",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
	            array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	         ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Show Sort By", "opallisting"),
	            "param_name" => "showsortby"
	        ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Column", "opallisting"),
	            "param_name" => "column",
	            "value" => '4',
	            'description' =>  ''
	          ),

	         array(
	            "type" => "textfield",
	            "heading" => __("Limit", "opallisting"),
	            "param_name" => "limit",
	            "value" => 6,
	            'description' =>  __('Limit featured properties showing', 'opallisting')
	        ),
	         array(
	            "type" => "checkbox",
	            "heading" => __("Don't show Description", "opallisting"),
	            "param_name" => "description"
	        ),
	         array(
	            "type" => "checkbox",
	            "heading" => __("Pagination", "opallisting"),
	            "param_name" => "pagination"
	        ),
          )
      ));


      vc_map( array(
          "name" => __("Filter Property", "opallisting"),
          "base" => "pbr_estate_filter_property",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
	            array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	         ),
	        array(
			    'type' => 'autocomplete',
			    'heading' => esc_html__( 'Categories', 'opallisting' ),
			    'value' => '',
			    'param_name' => 'property_category',
			    "admin_label" => true,
			    'description' => esc_html__( 'Select Categories', 'opallisting' ),
			    'settings' => array(
			     	'multiple' => true,
			     	'unique_values' => true,
			     // In UI show results except selected. NB! You should manually check values in backend
			    ),
		   	),
		   	array(
			    'type' => 'autocomplete',
			    'heading' => esc_html__( 'Locations', 'opallisting' ),
			    'value' => '',
			    'param_name' => 'opallisting_location',
			    "admin_label" => true,
			    'description' => esc_html__( 'Select Locations', 'opallisting' ),
			    'settings' => array(
			     	'multiple' => true,
			     	'unique_values' => true,
			     // In UI show results except selected. NB! You should manually check values in backend
			    ),
		   	),
		   	array(
			    'type' => 'autocomplete',
			    'heading' => esc_html__( 'Types', 'opallisting' ),
			    'value' => '',
			    'param_name' => 'opallisting_types',
			    "admin_label" => true,
			    'description' => esc_html__( 'Select Types', 'opallisting' ),
			    'settings' => array(
			     	'multiple' => true,
			     	'unique_values' => true,
			     // In UI show results except selected. NB! You should manually check values in backend
			    ),
		   	),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Show Sort By", "opallisting"),
	            "param_name" => "showsortby"
	        ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Column", "opallisting"),
	            "param_name" => "column",
	            "value" => '4',
	            'description' =>  ''
	          ),

	         array(
	            "type" => "textfield",
	            "heading" => __("Limit", "opallisting"),
	            "param_name" => "limit",
	            "value" => 6,
	            'description' =>  __('Limit featured properties showing', 'opallisting')
	        ),
	         array(
	            "type" => "checkbox",
	            "heading" => __("Don't show Description", "opallisting"),
	            "param_name" => "description"
	        ),
	         array(
	            "type" => "checkbox",
	            "heading" => __("Pagination", "opallisting"),
	            "param_name" => "pagination"
	        ),
          )
      ));

      vc_map( array(
          "name" => __("List Property", "opallisting"),
          "base" => "pbr_estate_list_property",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
	            array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	         ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Show Sort By", "opallisting"),
	            "param_name" => "showsortby"
	        ),
	           array(
	            "type" => "textfield",
	            "heading" => __("Column", "opallisting"),
	            "param_name" => "column",
	            "value" => '4',
	            'description' =>  ''
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Limit per page", "opallisting"),
	            "param_name" => "limit",
	            "value" => 10,
	            'description' =>  __('Limit featured properties showing', 'opallisting')
	        ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Don't show Description", "opallisting"),
	            "param_name" => "description"
	        ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Pagination", "opallisting"),
	            "param_name" => "pagination"
	        ),
          )
      ));


      vc_map( array(
          "name" => __("Grid Agent", "opallisting"),
          "base" => "pbr_estate_grid_agent",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
	            array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	         ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Column", "opallisting"),
	            "param_name" => "column",
	            "value" => '4',
	            'description' =>  ''
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Limit", "opallisting"),
	            "param_name" => "limit",
	            "value" => 6,
	            'description' =>  __('Limit featured agents showing', 'opallisting')
	          ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Show Featured Only", "opallisting"),
	            "param_name" => "onlyfeatured"
	          ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Pagination", "opallisting"),
	            "param_name" => "pagination"
	          ),
          )
      ));

      vc_map( array(
          "name" => __("List Agent", "opallisting"),
          "base" => "pbr_estate_list_agent",
          "class" => "",
          "description" => 'Get data from post type Team',
          "category" => __('OpalListing', "opallisting"),
          "params" => array(
	            array(
	            "type" => "textfield",
	            "heading" => __("Title", "opallisting"),
	            "param_name" => "title",
	            "value" => '',
	              "admin_label" => true
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("description", "opallisting"),
	            "param_name" => "description",
	            "value" => '',
	            'description' =>  ''
	         ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Column", "opallisting"),
	            "param_name" => "column",
	            "value" => '4',
	            'description' =>  ''
	          ),
	          array(
	            "type" => "textfield",
	            "heading" => __("Limit", "opallisting"),
	            "param_name" => "limit",
	            "value" => 6,
	            'description' =>  __('Limit featured properties showing', 'opallisting')
	        ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Show Featured Only", "opallisting"),
	            "param_name" => "onlyfeatured"
	          ),
	          array(
	            "type" => "checkbox",
	            "heading" => __("Pagination", "opallisting"),
	            "param_name" => "pagination"
	        ),
          )
      ));


      class WPBakeryShortCode_Pbr_featured_property  extends WPBakeryShortCode {}
      class WPBakeryShortCode_Pbr_estate_searchbox   extends WPBakeryShortCode {}
      class WPBakeryShortCode_Pbr_estate_search_agents  extends WPBakeryShortCode {}
      

      class WPBakeryShortCode_pbr_estate_grid_property  extends WPBakeryShortCode {}
      class WPBakeryShortCode_pbr_estate_list_property  extends WPBakeryShortCode {}
      class WPBakeryShortCode_pbr_estate_filter_property  extends WPBakeryShortCode {}

      class WPBakeryShortCode_pbr_estate_grid_agent  extends WPBakeryShortCode {}
      class WPBakeryShortCode_pbr_estate_list_agent  extends WPBakeryShortCode {}
      class WPBakeryShortCode_pbr_estate_carousel_property  extends WPBakeryShortCode {}
      class WPBakeryShortCode_pbr_estate_manual_carousel_properties  extends WPBakeryShortCode {}
  }