<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Opallisting_PostType_Agent{

	/**
	 *
	 */
	public static function init(){
		add_action( 'init', array( __CLASS__, 'definition' ) );
		add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes_target' ) );
		add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );
		

		define( 'OPALLISTING_AGENT_PREFIX', 'opallisting_agt_' );
	}

	

	/**
	 *
	 */
	public static function definition(){

		$labels = array(
			'name'                  => __( 'Agents', 'opallisting' ),
			'singular_name'         => __( 'Property', 'opallisting' ),
			'add_new'               => __( 'Add New Agent', 'opallisting' ),
			'add_new_item'          => __( 'Add New Agent', 'opallisting' ),
			'edit_item'             => __( 'Edit Agent', 'opallisting' ),
			'new_item'              => __( 'New Agent', 'opallisting' ),
			'all_items'             => __( 'All Agents', 'opallisting' ),
			'view_item'             => __( 'View Agent', 'opallisting' ),
			'search_items'          => __( 'Search Agent', 'opallisting' ),
			'not_found'             => __( 'No Agents found', 'opallisting' ),
			'not_found_in_trash'    => __( 'No Agents found in Trash', 'opallisting' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Agents', 'opallisting' ),
		);

		$labels = apply_filters( 'opallisting_postype_agent_labels' , $labels );

		register_post_type( 'opallisting_agent',
			array(
				'labels'            => $labels,
				'supports'          => array( 'title', 'editor', 'thumbnail', 'comments', 'author', 'excerpt' ),
				'public'            => true,
				'has_archive'       => true,
				'rewrite'           => array( 'slug' => _x( 'property-agent', 'URL Slug', 'opallisting' ) ),
				'menu_position'     => 51,
				'categories'        => array(),
				'menu_icon'         => 'dashicons-groups',

			)
		);


		///
		$labels = array(
	        'name'              => __( 'Agent Levels', 'opallisting' ),
	        'singular_name'     => __( 'Level', 'opallisting' ),
	        'search_items'      => __( 'Search Level', 'opallisting' ),
	        'all_items'         => __( 'All Levels', 'opallisting' ),
	        'parent_item'       => __( 'Parent Level', 'opallisting' ),
	        'parent_item_colon' => __( 'Parent Level:', 'opallisting' ),
	        'edit_item'         => __( 'Edit Level', 'opallisting' ),
	        'update_item'       => __( 'Update Level', 'opallisting' ),
	        'add_new_item'      => __( 'Add New Level', 'opallisting' ),
	        'new_item_name'     => __( 'New Level Name', 'opallisting' ),
	        'menu_name'         => __( 'Agent Levels', 'opallisting' ),
	      );
		///
		register_taxonomy('opallisting_agent_level',array('opallisting_agent'),
          array(
              'hierarchical'      => true,
              'labels'            => $labels,
              'show_ui'           => true,
              'show_admin_column' => true,
              'query_var'         => true,
              'show_in_nav_menus' =>true,
              'rewrite'           => array( 'slug' => 'agent-level'
          ),
      ));
	}

	public static function metaboxes_target(){
		$prefix = OPALLISTING_AGENT_PREFIX;
		$fields = array(
			array(
				'id'   => "{$prefix}target_min_price",
				'name' => __( 'Target Min Price', 'opallisting' ),
				'type' => 'text',
				'description'  => __( 'Enter min price of property which is for sale/rent...', 'opallisting' ),
			),

			array(
				'id'   => "{$prefix}target_max_price",
				'name' => __( 'Target Max Price', 'opallisting' ),
				'type' => 'text',
				'description'  => __( 'Enter max price of property which is for sale/rent...', 'opallisting' ),
			),

			array(
			    'name'     => __('Location' ,'opallisting'),
			    'desc'     => __('Select one, to add new you create in location of estate panel','opallisting'),
			    'id'       => $prefix."location",
			    'taxonomy' => 'opallisting_location', //Enter Taxonomy Slug
			    'type'     => 'taxonomy_select',
			) ,

			array(
			    'name'     => __('Types' ,'opallisting'),
			    'desc'     => __('Select one, to add new you create in location of estate panel','opallisting'),
			    'id'       => $prefix."type",
			    'taxonomy' => 'opallisting_types', //Enter Taxonomy Slug
			    'type'     => 'taxonomy_select',
			) ,

		); 
	    $metaboxes[ $prefix . 'target' ] = array(
			'id'                        => $prefix . 'target',
			'title'                     => __( 'Agent For Seachable', 'opallisting' ),
			'object_types'              => array( 'opallisting_agent' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    =>  $fields
		);

	    return $metaboxes;
	}
	/**
	 *
	 */
	public static function metaboxes_fields( $prefix = '' ){
		if ( ! $prefix ) {
			$prefix = OPALLISTING_AGENT_PREFIX;
		}

		$fields =  array(
			array(
				'id'   => "{$prefix}featured",
				'name' => __( 'Is Featured', 'opallisting' ),
				'type' => 'select',
				'description'  => __( 'Set this agent as featured', 'opallisting' ),
				 'options'          => array(
			        0 => __( 'No', 'opallisting' ),
			        1  => __( 'Yes', 'opallisting' )
			    ),

			),

			array(
				'id'   => "{$prefix}avatar",
				'name' => __( 'Avatar', 'opallisting' ),
				'type' => 'file',
				'description'  => __( 'Select one or more images to show as gallery', 'opallisting' ),
			),

			array(
				'name' => __( 'job', 'opallisting' ),
				'id'   => "{$prefix}job",
				'type' => 'text'
			),

			array(
				'name' => __( 'email', 'opallisting' ),
				'id'   => "{$prefix}email",
				'type' => 'text'
			),

			array(
				'name' => __( 'Phone', 'opallisting' ),
				'id'   => "{$prefix}phone",
				'type' => 'text'
			),

			array(
				'name' => __( 'Mobile', 'opallisting' ),
				'id'   => "{$prefix}mobile",
				'type' => 'text'
			),

			array(
				'name' => __( 'Fax', 'opallisting' ),
				'id'   => "{$prefix}fax",
				'type' => 'text'
			),
			array(
				'name' => __( 'Website', 'opallisting' ),
				'id'   => "{$prefix}web",
				'type' => 'text_url'
			),

			

			array(
				'name' => __( 'Twitter', 'opallisting' ),
				'id'   => "{$prefix}twitter",
				'type' => 'text_url'
			),

			array(
				'name' => __( 'Facebook', 'opallisting' ),
				'id'   => "{$prefix}facebook",
				'type' => 'text_url'
			),

			array(
				'name' => __( 'Google', 'opallisting' ),
				'id'   => "{$prefix}google",
				'type' => 'text_url'
			),

			array(
				'name' => __( 'LinkedIn', 'opallisting' ),
				'id'   => "{$prefix}linkedin",
				'type' => 'text_url'
			),

			array(
				'name' => __( 'Pinterest', 'opallisting' ),
				'id'   => "{$prefix}pinterest",
				'type' => 'text_url'
			),
			array(
				'name' => __( 'Instagram', 'opallisting' ),
				'id'   => "{$prefix}instagram",
				'type' => 'text_url'
			),


			array(
				'name' => __( 'Address', 'opallisting' ),
				'id'   => "{$prefix}address",
				'type' => 'text'
			),
			
			array(
				'id'            => "{$prefix}map",
				'name'          => __( 'Map Location', 'opallisting' ),
				'type'              => 'opal_map',
				'sanitization_cb'   => 'opal_map_sanitise',
                'split_values'      => true,
			),
		);
	
		return apply_filters( 'opallisting_postype_agent_metaboxes_fields' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes(array $metaboxes){
		$prefix = OPALLISTING_AGENT_PREFIX;

	    $metaboxes[ $prefix . 'info' ] = array(
			'id'                        => $prefix . 'info',
			'title'                     => __( 'Agent Information', 'opallisting' ),
			'object_types'              => array( 'opallisting_agent' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_fields( $prefix )
		);

	    return $metaboxes;
	}
}

Opallisting_PostType_Agent::init();