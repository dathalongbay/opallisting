<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Opallisting_PostType_Place{

	/**
	 * init action and filter data to define property post type
	 */
	public static function init(){
		add_action( 'init', array( __CLASS__, 'definition' ) );
		add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'metaboxes' ) );
		add_filter( 'cmb2_meta_boxes', array( __CLASS__, 'fields_front' ) , 9999 );
		add_action( 'transition_opallisting_property_status', array( __CLASS__, 'process_publish_property' ), 10, 1 );

		define( 'OPALLISTING_PROPERTY_PREFIX', 'opallisting_ppt_' );

		/* property column */
		add_filter( 'manage_opallisting_property_posts_columns', array( __CLASS__, 'columns' ) );
		add_action( 'manage_opallisting_property_posts_custom_column', array( __CLASS__, 'custom_columns' ), 10, 2 );

		add_action( 'save_post', array( __CLASS__, 'save_post' ), 1, 2 );
	}



	public static function save_post( $post_id , $post  ){

		$prefix = OPALLISTING_PROPERTY_PREFIX;
		if ( empty( $post_id ) || empty( $post )   ) {
			return;
		}

		// Dont' save meta boxes for revisions or autosaves
		if ( defined( 'DOING_AUTOSAVE' ) || is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
			return;
		}

		if( $post->post_type == 'opallisting_place' ){

			$opallisting_type = $_POST['opallisting_ppt_type'];


			//update_post_meta($post_id, 'opallisting_ppt_type', $opallisting_type);

			$custom_fields = get_post_meta($opallisting_type, 'opal_listing_type_custom_fields', true);

			$accept_fields = [];

			if(!empty($custom_fields)){
				foreach($custom_fields as $custom_field_item){
					if($custom_field_item['meta_key']){
						$accept_fields[] = $prefix.'specific_field_'.$custom_field_item['meta_key'];
					}
				}
			}

			global $wpdb;

			$sql = "DELETE FROM wp_postmeta WHERE post_id = $post_id AND meta_key LIKE '".OPALLISTING_PROPERTY_PREFIX."specific_field_%'";

			$wpdb->query($sql);

			$spec_prefix = OPALLISTING_PROPERTY_PREFIX."specific_field_";

			foreach($_POST as $key_post => $val_post){

				if(substr($key_post, 0, strlen($spec_prefix)) == $spec_prefix){
					if(!in_array($key_post, $accept_fields)){
						unset($_POST[$key_post]);
					}
				}

			}

		}
	}
	/**
	 *
	 */
	public static function definition(){

		$labels = array(
			'name'                  => __( 'Listings', 'opallisting' ),
			'singular_name'         => __( 'Listing', 'opallisting' ),
			'add_new'               => __( 'Add New Listing', 'opallisting' ),
			'add_new_item'          => __( 'Add New Listing', 'opallisting' ),
			'edit_item'             => __( 'Edit Listing', 'opallisting' ),
			'new_item'              => __( 'New Listing', 'opallisting' ),
			'all_items'             => __( 'All Listings', 'opallisting' ),
			'view_item'             => __( 'View Listing', 'opallisting' ),
			'search_items'          => __( 'Search Listing', 'opallisting' ),
			'not_found'             => __( 'No Listings found', 'opallisting' ),
			'not_found_in_trash'    => __( 'No Listings found in Trash', 'opallisting' ),
			'parent_item_colon'     => '',
			'menu_name'             => __( 'Listings', 'opallisting' ),
		);

		$labels = apply_filters( 'opallisting_postype_property_labels' , $labels );

		register_post_type( 'opallisting_place',
			array(
				'labels'            => $labels,
				'supports'          => array( 'title', 'editor', 'thumbnail', 'comments', 'author' ),
				'public'            => true,
				'has_archive'       => true,
				'rewrite'           => array( 'slug' => __( 'property', 'opallisting' ) ),
				'menu_position'     => 51,
				'categories'        => array(),
				'menu_icon'         => 'dashicons-admin-home',
				'map_meta_cap'        => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'query_var'           => true,
				'hierarchical'        => false, // Hierarchical causes memory issues - WP loads all records!
				'show_in_nav_menus'   => true
			)
		);

        register_taxonomy('property_category', 'opallisting_place', apply_filters( 'opallisting_taxonomy_args_property_category', array(
            'labels' => array(
                'name'              => __('Property Categories','reales'),
                'add_new_item'      => __('Add New Property Category','reales'),
                'new_item_name'     => __('New Property Category','reales')
            ),
            'hierarchical'  => true,
            'show_ui'               => true,
			'query_var'             => true,
            'rewrite'       => array('slug' => _x( 'property-category', 'slug', 'opallisting' ), 'with_front' => false, 'hierarchical' => true )
        )) );
	}

	/**
	 *
	 */
	public static function metaboxes( array $metaboxes ) {
		$prefix = OPALLISTING_PROPERTY_PREFIX;


		$metaboxes[ $prefix . 'management' ] = array(
			'id'                        => $prefix . 'management',
			'title'                     => __( 'Property Management ', 'opallisting' ),
			'object_types'              => array( 'opallisting_place' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_management_fields()
		);

		$metaboxes[ $prefix . 'info' ] = array(
			'id'                        => $prefix . 'info',
			'title'                     => __( 'Property Information', 'opallisting' ),
			'object_types'              => array( 'opallisting_place' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_info_fields()
		);

		$metaboxes[ $prefix . 'public_facilities' ] = array(
			'id'                        => $prefix . 'public_facilities',
			'title'                     => __( 'Public facilities', 'opallisting' ),
			'object_types'              => array( 'opallisting_place' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_public_facilities_fields()
		);

		$metaboxes[ $prefix . 'agent' ] = array(
			'id'                        => $prefix . 'agent',
			'title'                     => __( 'Agents Information', 'opallisting' ),
			'object_types'              => array( 'opallisting_place' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_agent_fields()
		);


		$metaboxes[ $prefix . 'layout_template' ] = array(
			'id'                        => $prefix . 'layout',
			'title'                     => __( 'Layout Template', 'opallisting' ),
			'object_types'              => array( 'opallisting_place' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_layout_fields()
		);

		$metaboxes[ $prefix . 'specific_fields' ] = array(
			'id'                        => $prefix . 'specific_fields',
			'title'                     => __( 'Listing type specific fields', 'opallisting' ),
			'object_types'              => array( 'opallisting_place' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => self::metaboxes_specifics_fields()
		);

		$listing_options = opallisting_get_listing_types();

		if(!empty($listing_options)){
			foreach($listing_options as $listing_option_key => $listing_option){
				$metaboxes[ $prefix . 'specific_fields_' . $listing_option_key ] = array(
					'id'                        => $prefix . 'specific_fields_' . $listing_option_key,
					'title'                     => __( 'Listing type specific fields ' . $listing_option, 'opallisting' ),
					'object_types'              => array( 'opallisting_place' ),
					'context'                   => 'normal',
					'priority'                  => 'high',
					'show_names'                => true,
					'fields'                    => self::metaboxes_specifics_type_fields($listing_option_key)
				);
			}
		}

		return $metaboxes;
	}

	public static function metaboxes_specifics_type_fields($listing_option_key){

		$prefix = OPALLISTING_PROPERTY_PREFIX;

		$fields = array();

		$custom_fields = get_post_meta($listing_option_key, 'opal_listing_type_custom_fields', true);


		foreach($custom_fields as $custom_field){
			if($custom_field['meta_key']){
				switch($custom_field['type']){
					case "select":
						$fields[] = array(
							'name'    => $custom_field['title'],
							'desc'    => '',
							'default' => $custom_field['default_value'],
							'id'      => $prefix.'specific_field_'.$custom_field['meta_key'],
							'type'    => $custom_field['type'],
							'options'          => $custom_field['options'],
						);
						break;
					default:
						$fields[] = array(
							'name'    => $custom_field['title'],
							'desc'    => '',
							'default' => $custom_field['default_value'],
							'id'      => $prefix.'specific_field_'.$custom_field['meta_key'],
							'type'    => $custom_field['type'],
						);
						break;
				}
			}


		}

		return $fields;

	}


	public static function metaboxes_specifics_fields(){

		$listing_options = opallisting_get_listing_types();

		$prefix = OPALLISTING_PROPERTY_PREFIX;
		$fields = array(
			array(
				'name'              => __( 'Listing Type', 'opallisting' ),
				'id'                => $prefix . 'type',
				'type'              => 'select',
				//'show_option_none' => true,
				'default'          => 'custom',
				'options'          => $listing_options,
				'description' => __('Choose a listing item type','opallisting')
			)
		);

		wp_enqueue_script( 'listing-specific-meta-box', OPALLISTING_PLUGIN_URL . 'assets/js/listing-specific-meta-box.js', array( 'jquery'), "1.3" );

		return apply_filters( 'opallisting_postype_property_metaboxes_specifics_fields' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_management_fields(){
		$prefix = OPALLISTING_PROPERTY_PREFIX;
		$fields = array(
			array(
				'name'              => __( 'Featured', 'opallisting' ),
				'id'                => $prefix . 'featured',
				 'type'    => 'radio_inline',
			    'options' => array(
			        0 => __( 'No', 'opallisting' ),
			        1   => __( 'Yes', 'opallisting' ),
			    ),
			),
			array(
				'name'              => __( 'Property SKU', 'opallisting' ),
				'id'                => $prefix . 'sku',
				'type'              => 'text',
				'description' => __('Please Enter Your Property SKU','opallisting')
			),
			array(
				'id'                => $prefix . 'map',
				'name'              => __( 'Location', 'opallisting' ),
				'type'              => 'opal_map',
				'sanitization_cb'   => 'opal_map_sanitise',
                'split_values'      => true,
			),

			array(
				'name'      => __( 'Postal Code / Zip', 'opallisting' ),
				'id'        => $prefix . 'zipcode',
				'type'      => 'text',

			),
			array(
				'name'      => __( 'Google Map View', 'opallisting' ),
				'id'        => $prefix . 'enablemapview',
				'type'      => 'select',
				'options'          => array(
			        1 => __( 'Yes', 'cmb2' ),
			        0   => __( 'No', 'cmb2' )
			    ),
			),

			array(
				'name'              => __( 'Address', 'opallisting' ),
				'id'                => $prefix . 'address',
				'type'              => 'textarea_small',
				'attributes' => array(
			        'required' => 'required',
			    ),
			),

			array(
				'id'   => "{$prefix}gallery",
				'name' => __( 'Images Gallery', 'opallisting' ),
				'type' => 'file_list',
				'description'  => __( 'Select one or more images to show as gallery', 'opallisting' ),
			),

			array(
				'id'   => "{$prefix}video",
				'name' => __( 'Video', 'opallisting' ),
				'type' => 'text_url',
				'description'  => __( 'Input for videos, audios from Youtube, Vimeo and all supported sites by WordPress. It has preview feature.', 'opallisting' ),
			),

			array(
				'id'   => "{$prefix}attachments",
				'name' => __( 'Attachments', 'opallisting' ),
				'type' => 'file_list',
				'options' => array(
			    	'url' => true, // Hide the text input for the url
			    ),
				'description'  => __( 'Select one or more files to allow download', 'opallisting' ),
			),

		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_management' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_price_fields() {
		$prefix = OPALLISTING_PROPERTY_PREFIX;

		$fields = array(
			array(
				'id'                => $prefix . 'price',
				'name'              => __( 'Price ($)', 'opallisting' ),
				'type'              => 'text_money',
				'description'       => __( 'Enter amount without currency.', 'opallisting' ),
				'attributes' => array(
			        'required' => 'required',
			    ),
				'before_row'   => '<div class="row-group-features group-has-three group-price   clearfix"><h3>'.( is_admin() ? "" :__('Price','opallisting') ).'</h3>', // callback
			),
			array(
				'id'                => $prefix . 'pricelabel',
				'name'              => __( 'Price Label', 'opallisting' ),
				'type'              => 'text',
				'attributes' => array(
			        'required' => 'required',
			    ),
				'description'       => __( 'Price Label (e.g. "per month")', 'opallisting' ),
				'after_row'   		=> '</div>', // callback
			)

		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_price' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_info_fields() {
		$prefix = OPALLISTING_PROPERTY_PREFIX;

		$fields = array(

			array(
				'name'              => __( 'Built year', 'opallisting' ),
				'id'                => $prefix . 'builtyear',
				'type'				 => 'text_date',
				'description'   	=> __('Enter built year','opallisting'),
				'date_format' => 'l jS \of F Y',
				'before_row'   => '<div class="row-group-features group-has-three group-property-info clearfix"><h3>'.(is_admin() ? "":__('Property Information','opallisting')).'</h3>', // callback

			),


			array(
                'name'              => __( 'Parking', 'opallisting' ),
                'id'                => $prefix . 'parking',
                'type'              => 'text',
                'description' => __('Enter number of Parking','opallisting')
			),

			array(
				'name'              => __( 'Bedrooms', 'opallisting' ),
				'id'                => $prefix . 'bedrooms',
				'type'              => 'text',
				'description' => __('Enter number of bedrooms','opallisting')
			),
			array(
				'name'              => __( 'Bathrooms', 'opallisting' ),
				'id'                => $prefix . 'bathrooms',
				'type' => 'text',
				'description' => __('Enter number of bathrooms','opallisting')
			),
			array(
				'name'              => __( 'Plot Size', 'opallisting' ),
				'id'                => $prefix . 'plotsize',
				'type' => 'text',
				'description' => __('Enter size of Plot as 20x30, 20x30x40, 20x30x40x50','opallisting')
			),
			array(
				'name'              => __( 'Area Size', 'opallisting' ),
				'id'                => $prefix . 'areasize',
				'type' => 'text',
				'description' => __('Enter size of area in sqft','opallisting')
			),
            array(
				'name' => __( 'Orientation', 'opallisting' ),
				'id'   => "{$prefix}orientation",
				'type' => 'text',
				'description' => __('Enter Orientation of property','opallisting')
			),

			array(
				'name' => __( 'Living Rooms', 'opallisting' ),
				'id'   => "{$prefix}livingrooms",
				'type' => 'text',
				'description' => __('Enter Number of Living Rooms','opallisting')
			),

			array(
				'name' => __( 'Kitchens', 'opallisting' ),
				'id'   => "{$prefix}kitchens",
				'type' => 'text',
				'description' => __('Enter Number of Kitchens','opallisting')
			),

			array(
				'name' => __( 'Rooms', 'opallisting' ),
				'id'   => "{$prefix}amountrooms",
				'type' => 'text',
				'description' => __('Enter Number of Amount Rooms','opallisting'),

				'after_row'    => '</div>',

			)
		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_info' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_public_facilities_fields() {
		$prefix = OPALLISTING_PROPERTY_PREFIX;
		$fields = array(
		   	array(
				'id'                => $prefix . 'public_facilities_group',
				'type'              => 'group',
				'fields'            => array(
					array(
						'id'                => $prefix . 'public_facilities_key',
						'name'              => __( 'Label', 'opallisting' ),
						'type'              => 'text',
					),
					array(
						'id'                => $prefix . 'public_facilities_value',
						'name'              => __( 'Content', 'opallisting' ),
						'type'              => 'text',
					)
				),
				'options'     => array(
			        'group_title'   => __( 'Facility {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
			        'add_button'    => __( 'Add Another Entry', 'cmb2' ),
			        'remove_button' => __( 'Remove Entry', 'cmb2' ),
			        'sortable'      => true, // beta
			       	'closed'     => false, // true to have the groups closed by default
			    ),
			)
		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_public_facilities' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_agent_fields() {
		$prefix = OPALLISTING_PROPERTY_PREFIX;
		$agents_objects = Opallisting_Query::get_agents();
		$agents = array( 0 => __('Use Author Information','opallisting') );
		if ( !empty($agents_objects->posts) && is_array( $agents_objects->posts ) ) {
			foreach( $agents_objects->posts as $object ){
				$agents[$object->ID] = $object->post_title;
			}
		}

		$fields = array(
			array(
				'name' => __( 'Agent', 'opallisting' ),
				'id'   => "{$prefix}agent",
				'type' => 'select',
				'options'	=> $agents
			),
		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_agent' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_layout_fields(){

		$prefix = OPALLISTING_PROPERTY_PREFIX;

		$fields = array(
			array(
				'name' => __( 'Layout Display', 'opallisting' ),
				'id'   => "{$prefix}layout",
				'type' => 'select',
				'options'	=> apply_filters('opallisting_single_layout_templates', array( '' => __('Inherit','opallisting') ) ),
				'description' =>__( 'Select a layout to display full information of this property' )
			),
		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_agent' , $fields );
	}

	/**
	 * Defines custom front end fields
	 *
	 * @access public
	 * @param array $metaboxes
	 * @return array
	 */
	public static function fields_front( array $metaboxes ) {

		$prefix = OPALLISTING_PROPERTY_PREFIX;

		if ( ! is_admin() ) {

			$management = array(


				array(
					'name'              => __( 'Address', 'opallisting' ),
					'id'                => $prefix . 'address',
					'type'              => 'text',
					'attributes' => array(
				        'required' => 'required',
				    ),
					'before_row'   => '<div class="row-group-features group-has-two clearfix"><h3>'.__('Property Location','opallisting').'</h3>', // callback

				),


				array(
					'name'      => __( 'Location', 'opallisting' ),
					'id'        => $prefix . 'location',
					'type'      => 'taxonomy_select',
					'taxonomy'  => 'opallisting_location',
					'attributes' => array(
				        'required' => 'required',
				    ),

				),

				array(
					'name'      => __( 'Postal Code / Zip', 'opallisting' ),
					'id'        => $prefix . 'zipcode',
					'type'      => 'text',

				),
				array(
					'name'      => __( 'Google Map View', 'opallisting' ),
					'id'        => $prefix . 'enablemapview',
					'type'      => 'select',
					'options'          => array(
				        0   => __( 'No', 'cmb2' ),
				        1 => __( 'Yes', 'cmb2' )
				    ),
				    'after_row' 		=> '</div>'
				),
				array(
				'id'                => $prefix . 'map',
				'name'              => __( 'Google Map', 'opallisting' ),
				'type'              => 'opal_map',
				'sanitization_cb'   => 'opal_map_sanitise',
                'split_values'      => true,
				),

			);

			$fields = array_merge(
				self::metaboxes_general_fields_front(),
				self::metaboxes_price_fields(),
				$management,
				self::metaboxes_info_fields(),
				self::metaboxes_taxonomies_fields(),
				 self::metaboxes_public_facilities_fields()

			);

			$metaboxes[ $prefix . 'front' ] = array(
				'id'                        => $prefix . 'front',
				'title'                     => __( 'Name and Description', 'opallisting' ),
				'object_types'              => array( 'opallisting_place' ),
				'context'                   => 'normal',
				'priority'                  => 'high',
				'show_names'                => true,
				'fields'                    => $fields
			);


		}
		return $metaboxes;
	}

	/**
	 *
	 */
	public static function metaboxes_general_fields_front() {
		$prefix = OPALLISTING_PROPERTY_PREFIX;
		if ( ! empty( $_GET['id'] ) ) {
			$post = get_post( $_GET['id'] );
			$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $_GET['id'] ) );
		}

		$fields = array(
			array(
				'id'                => $prefix . 'post_type',
				'type'              => 'hidden',
				'default'           => 'opallisting_place',
			),
			array(
				'name'              => __( 'Title', 'opallisting' ),
				'id'                => $prefix . 'title',
				'type'              => 'text_medium',
				'default'           => ! empty( $post ) ? $post->post_title : '',
				'attributes' => array(
			        'required' => 'required',
			    ),
			),
			array(
				'name'              => __( 'Description', 'opallisting' ),
				'id'                => $prefix . 'text',
				'type'              => 'wysiwyg',
				'default'           => ! empty( $post ) ? $post->post_content : '',
				'attributes' => array(
			        'required' => 'required',
			    ),
			),


			array(
				'name'      => __( 'Statuses', 'opallisting' ),
				'id'        => $prefix . 'status',
				'type'      => 'taxonomy_select',
				'taxonomy'  => 'opallisting_status',
				'class'		=> 'form-control',
				'attributes' => array(
			        'required' => 'required',
			    ),
				'before_row'   => '<div class="group-has-two clearfix">', // callback

			),
			array(
				'name'      => __( 'Types', 'opallisting' ),
				'id'        => $prefix . 'type',
				'type'      => 'taxonomy_select',
				'taxonomy'  => 'opallisting_types',
				'class'		=> 'form-control',
				'attributes' => array(
			        'required' => 'required',
			    ),
				'after_row'   => '</div>', // callback
			),

			array(
				'id'   => "{$prefix}gallery",
				'name' => __( 'Images Gallery', 'opallisting' ),
				'type' => 'opal_upload' ,
				'description'  => __( 'Select one or more images to show as gallery', 'opallisting' ),
				'before_row'   => '<div class="row-group-features clearfix"><h3>'.__('Media','opallisting').'</h3>', // callback
			),

			array(
				'id'   => "{$prefix}video",
				'name' => __( 'Video', 'opallisting' ),
				'type' => 'text',
				'description'  => __( 'Input for videos, audios from Youtube, Vimeo and all supported sites by WordPress. It has preview feature.', 'opallisting' ),
				'before_row'   => '</div>'
			),
		);

		if( is_admin() ){
			$fields[] = array(
					'name'              => __( 'Featured Image', 'opallisting' ),
					'id'                => $prefix . 'featured_image',
					'type'              => 'file',
					'default'           => ! empty( $featured_image ) ? $featured_image[0] : '',
			);
		}
		return apply_filters( 'opallisting_postype_property_metaboxes_fields_general' , $fields );
	}

	/**
	 *
	 */
	public static function metaboxes_taxonomies_fields() {
		$prefix = OPALLISTING_PROPERTY_PREFIX;
		$fields = array(
			array(
				'name'      => __( 'Amenities', 'opallisting' ),
				'id'        => $prefix . 'amenity',
				'type'      => 'taxonomy_multicheck',
				'taxonomy'  => 'opallisting_amenities',
				'before_row'   => '<div class="row-group-features group-td-full group-amenities clearfix"><h3>'.__('Property Amenities','opallisting').'</h3>', // callback
				'after_row'    => '</div>',
			),
		);

		return apply_filters( 'opallisting_postype_property_metaboxes_fields_taxonomies' , $fields );
	}

	/**
	 *
	 */
	public static function process_publish_property($post) {
		if ( $old_status == 'pending'  &&  $new_status == 'publish' ) {
			$user_id = $post->post_author;
			$user = get_user_by( 'id', $user_id );
			if (!is_object($user)) {
				$from_name = opallisting_get_option('from_name');
				$from_email = opallisting_get_option('from_email');
				$subject = opallisting_get_option('publish_submission_email_subject');

				$headers = sprintf( "From: %s <%s>\r\n Content-type: text/html", $from_name, $from_email );

				$property_link = get_permalink( $post );
				$tags = array("{first_name}", "{last_name}", "{property_link}");
				$values = array($user->first_name, $user->last_name, $property_link);

				$body = opallisting_get_option('publish_submission_email_body');
				$body = html_entity_decode($body);
				$message = str_replace($tags, $values, $body);

				return wp_mail( $user->user_email, $subject, $message, $headers );
			}
		}
	}

	/**
	 *
	 */
	public static function columns( $columns ) {
		$comments = $columns['comments'];
		unset( $columns['author'], $columns['date'], $columns['comments'] );
		$columns['featured'] 	= __( 'Featured', 'opallisting' );
		$columns['sku']			= __( 'Sku','opallisting' );
		$columns['address']		= __( 'Address','opallisting' );
		$columns['comments']	= $comments;
		$columns['author']		= __( 'Author', 'opallisting' );
		$columns['date']		= __( 'Date', 'opallisting' );
		return $columns;
	}

	/**
	 *
	 */
	public static function custom_columns( $column, $post_id ) {
		$property = new Opallisting_Property( $post_id );
		$nonce = wp_create_nonce( 'opallisting_place' );
		switch ( $column ) {
			case 'featured':
					if ( $property->featured ) {
						$url = add_query_arg( array(
								'action'		=> 'opallisting_remove_feature_property',
								'property_id'	=> $post_id,
								'nonce'			=> $nonce
							), admin_url( 'admin-ajax.php' ) );
						echo '<a href="' . esc_url( $url ) . '">';
						echo '<i class="dashicons dashicons-star-filled"></i>';
						echo '</a>';
					} else {
						$url = add_query_arg( array(
								'action'		=> 'opallisting_set_feature_property',
								'property_id'	=> $post_id,
								'nonce'			=> $nonce
							), admin_url( 'admin-ajax.php' ) );
						echo '<a href="' . esc_url( $url ) . '">';
						echo '<i class="dashicons dashicons-star-empty"></i>';
						echo '</a>';
					}
				break;

			case 'sku':
					if ( $property->sku ) {
						echo sprintf( '%s', $property->sku );
					}
				break;

			case 'address':
					if ( $property->address ) {
						echo sprintf( '%s', $property->address );
					}
				break;

			default:
				# code...
				break;
		}
	}
}

Opallisting_PostType_Place::init();