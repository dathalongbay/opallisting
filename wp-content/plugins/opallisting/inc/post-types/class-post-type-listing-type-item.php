<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class Opallisting_PostType_Listing_Type_Item{

    /**
     * init action and filter data to define property post type
     */
    public static function init(){
        add_action( 'init', array( __CLASS__, 'definition' ) );

        define( 'OPALLISTING_LISTING_TYPE_PREFIX', 'opallisting_listing_type_' );

        add_action('add_meta_boxes' , function(){
            add_meta_box( 'custom-field-creator', 'Form builder listing', array( __CLASS__, 'output_meta_boxs'), 'opallisting_type' );

        });


        add_action('save_post', array( __CLASS__, 'save_meta_data' ), 1, 2);

        add_action("wp_ajax_creator_custom_type", array( __CLASS__, 'creator_custom_type' ));
        add_action("wp_ajax_nopriv_creator_custom_type", array( __CLASS__, 'creator_custom_type' ));

        require (OPALLISTING_PLUGIN_DIR . 'inc/custom-field-creator/class-opalling-custom-field-text.php');
        require (OPALLISTING_PLUGIN_DIR . 'inc/custom-field-creator/class-opallisting-custom-field-textarea.php');
        require (OPALLISTING_PLUGIN_DIR . 'inc/custom-field-creator/class-opallisting-custom-field-checkbox.php');
        require (OPALLISTING_PLUGIN_DIR . 'inc/custom-field-creator/class-opallisting-custom-field-select.php');
    }

    function creator_custom_type(){

        $type = $_POST['type'];

        $icons = new Opallisting_font_awesome(OPALLISTING_PLUGIN_URL.'assets/font-awesome-4.6.3/css/font-awesome.css');

        $icons_data = $icons->getIcons();

        switch($type){
            case "textarea":
                $field = new Opallisting_Custom_Field_Textarea('no','','','', '', false, $icons_data, '');
                ob_start();
                $field->render_blank();
                $html = ob_get_contents();
                ob_end_clean();
                break;
            case "select":
                $field = new Opallisting_Custom_Field_Select('no','','','', false, $icons_data, '');
                ob_start();

                $field->render_blank();
                $html = ob_get_contents();
                ob_end_clean();
                break;
            case "checkbox";
                $field = new Opallisting_Custom_Field_Checkbox('no','','','', '', false, $icons_data, '');
                ob_start();
                $field->render_blank();
                $html = ob_get_contents();
                ob_end_clean();
                break;
            case "text":
            default:
                $field = new Opalling_Custom_Field_Text('no','','','', '', false, $icons_data, '');
                ob_start();
                $field->render_blank();
                $html = ob_get_contents();
                ob_end_clean();
                break;
        }

        $result = ['type' => 'success', 'html' => $html];

        echo json_encode($result);
        exit;
    }

    function save_meta_data($post_id, $post){


        $custom_fields_flag = false;
        if(isset($_POST['cf_type'])){
            $custom_fields_flag = true;

            $case_select = 0;
            $case_common = 0;

            foreach($_POST['cf_type'] as $key => $value){
                switch ($value){
                    case 'text':
                    case 'textarea':
                    case 'checkbox':
                        $custom_fields[$key] = array(
                            'type' => $value,
                            'meta_key' => sanitize_title($_POST['cf_meta_key'][$key]),
                            'title' => $_POST['cf_title'][$key],
                            'icon' => $_POST['cf_icon'][$key],
                        );

                        $default_value = isset( $_POST['cf_default_value'][$case_common] ) ? $_POST['cf_default_value'][$case_common] : '';
                        $custom_fields[$key]['default_value'] = $default_value;
                        $case_common++;
                        break;
                    case 'select':

                        $def_value_index = (int)$_POST['opal_custom_select_options_default'][$case_select];
                        $custom_fields[$key] = array(
                            'type'=>'select',
                            'meta_key'=>sanitize_title($_POST['cf_meta_key'][$key]),
                            'title'=>$_POST['cf_title'][$key],
                            'icon' => $_POST['cf_icon'][$key],
                            'options'=>array_combine($_POST['opal_custom_select_options_value'][$case_select],
                                $_POST['opal_custom_select_options_label'][$case_select]),
                            'default_value'=>$_POST['opal_custom_select_options_value'][$case_select][$def_value_index],
                            'default_value_index'=>$def_value_index
                        );


                        $case_select++;
                        break;
                }
            }
        }


        if($custom_fields_flag){
            update_post_meta( $post_id,  'opal_listing_type_custom_fields', $custom_fields );
        }else{
            delete_post_meta( $post_id,  'opal_listing_type_custom_fields' );
        }
    }

    function add_meta_box(){

        add_meta_box( 'custom-field-creator', 'Form builder listing', array( __CLASS__, 'output_meta_boxs'), 'opallisting_type' );
    }


    function output_meta_boxs(){

        global $post;

        wp_enqueue_style("bootstrap3style", OPALLISTING_PLUGIN_URL . '/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css');
        wp_enqueue_style("creatorfieldsstyle", OPALLISTING_PLUGIN_URL . '/assets/creator-fields-style.css');


        wp_enqueue_style("fonticonpicker", OPALLISTING_PLUGIN_URL . '/assets/fontIconPicker-2.0.0/css/jquery.fonticonpicker.min.css');
        wp_enqueue_style("fonticonpicker-grey-theme", OPALLISTING_PLUGIN_URL . '/assets/fontIconPicker-2.0.0/themes/grey-theme/jquery.fonticonpicker.grey.min.css');
        wp_enqueue_style("fontawesome", OPALLISTING_PLUGIN_URL . '/assets/font-awesome-4.6.3/css/font-awesome.css');



        wp_enqueue_script("bootstrap3js", OPALLISTING_PLUGIN_URL . 'assets/bootstrap-3.3.7-dist/js/bootstrap.min.js', array( 'jquery' ), "1.3", false);

        wp_enqueue_script("creatorfields", OPALLISTING_PLUGIN_URL . 'assets/js/creator-fields.js', array( 'jquery' ), "1.3", false);


        wp_enqueue_script("fonticonpicker", OPALLISTING_PLUGIN_URL . 'assets/fontIconPicker-2.0.0/jquery.fonticonpicker.min.js', array( 'jquery' ), "1.3", false);

        //
        wp_localize_script( 'creatorfields', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

        $custom_fields = get_post_meta($post->ID, 'opal_listing_type_custom_fields', true);

        require_once (OPALLISTING_PLUGIN_DIR . 'templates/metaboxs/listing-type-creator.php');

    }



    function cmb2_add_metabox() {

        $prefix = OPALLISTING_LISTING_TYPE_PREFIX;

        $cmb = new_cmb2_box( array(
            'id'           => $prefix . 'metabox',
            'title'        => __( 'Custom fields creator', 'cmb2' ),
            'object_types' => array( 'opallisting_type' ),
            'context'      => 'normal',
            'priority'     => 'default',
        ) );

        $cmb->add_field( array(
            'name' => __( 'sdsd', 'cmb2' ),
            'id' => $prefix . 'fdff',
            'type' => 'text',
            'default' => 'sdasd',
        ) );

    }

    public static function save_post( $post_id , $post  ){
        if ( empty( $post_id ) || empty( $post )   ) {
            return;
        }

        // Dont' save meta boxes for revisions or autosaves
        if ( defined( 'DOING_AUTOSAVE' ) || is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
            return;
        }

        if( $post->post_type == 'opallisting_listing_type' ){

        }
    }
    /**
     *
     */
    public static function definition(){

        $labels = array(
            'name'                  => __( 'Listing Types', 'opallisting' ),
            'singular_name'         => __( 'Listing Type', 'opallisting' ),
            'add_new'               => __( 'Add New Listing Type', 'opallisting' ),
            'add_new_item'          => __( 'Add New Listing Type', 'opallisting' ),
            'edit_item'             => __( 'Edit Listing Type', 'opallisting' ),
            'new_item'              => __( 'New Listing Type', 'opallisting' ),
            'all_items'             => __( 'All Listing Types', 'opallisting' ),
            'view_item'             => __( 'View Listing Type', 'opallisting' ),
            'search_items'          => __( 'Search Listing Type', 'opallisting' ),
            'not_found'             => __( 'No Listing Type found', 'opallisting' ),
            'not_found_in_trash'    => __( 'No Listing Type found in Trash', 'opallisting' ),
            'parent_item_colon'     => '',
            'menu_name'             => __( 'Listing Types', 'opallisting' ),
        );

        $labels = apply_filters( 'opallisting_postype_listing_type_item_labels' , $labels );


        register_post_type( 'opallisting_type',
            array(
                'labels'            => $labels,
                'supports'          => array( 'title' ),

                'public'            => false,
                'has_archive'       => false,
                'rewrite'           => array( 'slug' => __( 'listing-type', 'opallisting' ) ),
                'show_in_menu' 		  => 'edit.php?post_type=opallisting_place',
                'categories'        => array(),
                'menu_icon'         => 'dashicons-admin-home',
                'map_meta_cap'        => true,
                'publicly_queryable'  => true,
                'exclude_from_search' => false,
                'query_var'           => true,
                'hierarchical'        => false, // Hierarchical causes memory issues - WP loads all records!
                'show_in_nav_menus'   => true,

                'show_ui' => true,
                'capability_type'     => 'post',
            )
        );



        /*register_taxonomy('property_category', 'opallisting_place', apply_filters( 'opallisting_taxonomy_args_property_category', array(
            'labels' => array(
                'name'              => __('Property Categories','reales'),
                'add_new_item'      => __('Add New Property Category','reales'),
                'new_item_name'     => __('New Property Category','reales')
            ),
            'hierarchical'  => true,
            'show_ui'               => true,
            'query_var'             => true,
            'rewrite'       => array('slug' => _x( 'property-category', 'slug', 'opallisting' ), 'with_front' => false, 'hierarchical' => true )
        )) );*/
    }




    /**
     *
     */
    public static function metaboxes_agent_fields() {
        $prefix = OPALLISTING_PROPERTY_PREFIX;
        $agents_objects = Opallisting_Query::get_agents();
        $agents = array( 0 => __('Use Author Information','opallisting') );
        if ( !empty($agents_objects->posts) && is_array( $agents_objects->posts ) ) {
            foreach( $agents_objects->posts as $object ){
                $agents[$object->ID] = $object->post_title;
            }
        }

        $fields = array(
            array(
                'name' => __( 'Agent', 'opallisting' ),
                'id'   => "{$prefix}agent",
                'type' => 'select',
                'options'	=> $agents
            ),
        );

        return apply_filters( 'opallisting_postype_property_metaboxes_fields_agent' , $fields );
    }

    /**
     *
     */
    public static function metaboxes_layout_fields(){

        $prefix = OPALLISTING_PROPERTY_PREFIX;

        $fields = array(
            array(
                'name' => __( 'Layout Display', 'opallisting' ),
                'id'   => "{$prefix}layout",
                'type' => 'select',
                'options'	=> apply_filters('opallisting_single_layout_templates', array( '' => __('Inherit','opallisting') ) ),
                'description' =>__( 'Select a layout to display full information of this property' )
            ),
        );

        return apply_filters( 'opallisting_postype_property_metaboxes_fields_agent' , $fields );
    }

    /**
     * Defines custom front end fields
     *
     * @access public
     * @param array $metaboxes
     * @return array
     */
    public static function fields_front( array $metaboxes ) {

        $prefix = OPALLISTING_PROPERTY_PREFIX;

        if ( ! is_admin() ) {

            $management = array(


                array(
                    'name'              => __( 'Address', 'opallisting' ),
                    'id'                => $prefix . 'address',
                    'type'              => 'text',
                    'attributes' => array(
                        'required' => 'required',
                    ),
                    'before_row'   => '<div class="row-group-features group-has-two clearfix"><h3>'.__('Property Location','opallisting').'</h3>', // callback

                ),


                array(
                    'name'      => __( 'Location', 'opallisting' ),
                    'id'        => $prefix . 'location',
                    'type'      => 'taxonomy_select',
                    'taxonomy'  => 'opallisting_location',
                    'attributes' => array(
                        'required' => 'required',
                    ),

                ),

                array(
                    'name'      => __( 'Postal Code / Zip', 'opallisting' ),
                    'id'        => $prefix . 'zipcode',
                    'type'      => 'text',

                ),
                array(
                    'name'      => __( 'Google Map View', 'opallisting' ),
                    'id'        => $prefix . 'enablemapview',
                    'type'      => 'select',
                    'options'          => array(
                        0   => __( 'No', 'cmb2' ),
                        1 => __( 'Yes', 'cmb2' )
                    ),
                    'after_row' 		=> '</div>'
                ),
                array(
                    'id'                => $prefix . 'map',
                    'name'              => __( 'Google Map', 'opallisting' ),
                    'type'              => 'opal_map',
                    'sanitization_cb'   => 'opal_map_sanitise',
                    'split_values'      => true,
                ),

            );

            $fields = array_merge(
                self::metaboxes_general_fields_front(),
                self::metaboxes_price_fields(),
                $management,
                self::metaboxes_info_fields(),
                self::metaboxes_taxonomies_fields(),
                self::metaboxes_public_facilities_fields()

            );

            $metaboxes[ $prefix . 'front' ] = array(
                'id'                        => $prefix . 'front',
                'title'                     => __( 'Name and Description', 'opallisting' ),
                'object_types'              => array( 'opallisting_place' ),
                'context'                   => 'normal',
                'priority'                  => 'high',
                'show_names'                => true,
                'fields'                    => $fields
            );


        }
        return $metaboxes;
    }

    /**
     *
     */
    public static function metaboxes_general_fields_front() {
        $prefix = OPALLISTING_PROPERTY_PREFIX;
        if ( ! empty( $_GET['id'] ) ) {
            $post = get_post( $_GET['id'] );
            $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $_GET['id'] ) );
        }

        $fields = array(
            array(
                'id'                => $prefix . 'post_type',
                'type'              => 'hidden',
                'default'           => 'opallisting_place',
            ),
            array(
                'name'              => __( 'Title', 'opallisting' ),
                'id'                => $prefix . 'title',
                'type'              => 'text_medium',
                'default'           => ! empty( $post ) ? $post->post_title : '',
                'attributes' => array(
                    'required' => 'required',
                ),
            ),
            array(
                'name'              => __( 'Description', 'opallisting' ),
                'id'                => $prefix . 'text',
                'type'              => 'wysiwyg',
                'default'           => ! empty( $post ) ? $post->post_content : '',
                'attributes' => array(
                    'required' => 'required',
                ),
            ),


            array(
                'name'      => __( 'Statuses', 'opallisting' ),
                'id'        => $prefix . 'status',
                'type'      => 'taxonomy_select',
                'taxonomy'  => 'opallisting_status',
                'class'		=> 'form-control',
                'attributes' => array(
                    'required' => 'required',
                ),
                'before_row'   => '<div class="group-has-two clearfix">', // callback

            ),
            array(
                'name'      => __( 'Types', 'opallisting' ),
                'id'        => $prefix . 'type',
                'type'      => 'taxonomy_select',
                'taxonomy'  => 'opallisting_types',
                'class'		=> 'form-control',
                'attributes' => array(
                    'required' => 'required',
                ),
                'after_row'   => '</div>', // callback
            ),

            array(
                'id'   => "{$prefix}gallery",
                'name' => __( 'Images Gallery', 'opallisting' ),
                'type' => 'opal_upload' ,
                'description'  => __( 'Select one or more images to show as gallery', 'opallisting' ),
                'before_row'   => '<div class="row-group-features clearfix"><h3>'.__('Media','opallisting').'</h3>', // callback
            ),

            array(
                'id'   => "{$prefix}video",
                'name' => __( 'Video', 'opallisting' ),
                'type' => 'text',
                'description'  => __( 'Input for videos, audios from Youtube, Vimeo and all supported sites by WordPress. It has preview feature.', 'opallisting' ),
                'before_row'   => '</div>'
            ),
        );

        if( is_admin() ){
            $fields[] = array(
                'name'              => __( 'Featured Image', 'opallisting' ),
                'id'                => $prefix . 'featured_image',
                'type'              => 'file',
                'default'           => ! empty( $featured_image ) ? $featured_image[0] : '',
            );
        }
        return apply_filters( 'opallisting_postype_property_metaboxes_fields_general' , $fields );
    }

    /**
     *
     */
    public static function metaboxes_taxonomies_fields() {
        $prefix = OPALLISTING_PROPERTY_PREFIX;
        $fields = array(
            array(
                'name'      => __( 'Amenities', 'opallisting' ),
                'id'        => $prefix . 'amenity',
                'type'      => 'taxonomy_multicheck',
                'taxonomy'  => 'opallisting_amenities',
                'before_row'   => '<div class="row-group-features group-td-full group-amenities clearfix"><h3>'.__('Property Amenities','opallisting').'</h3>', // callback
                'after_row'    => '</div>',
            ),
        );

        return apply_filters( 'opallisting_postype_property_metaboxes_fields_taxonomies' , $fields );
    }

    /**
     *
     */
    public static function process_publish_property($post) {
        if ( $old_status == 'pending'  &&  $new_status == 'publish' ) {
            $user_id = $post->post_author;
            $user = get_user_by( 'id', $user_id );
            if (!is_object($user)) {
                $from_name = opallisting_get_option('from_name');
                $from_email = opallisting_get_option('from_email');
                $subject = opallisting_get_option('publish_submission_email_subject');

                $headers = sprintf( "From: %s <%s>\r\n Content-type: text/html", $from_name, $from_email );

                $property_link = get_permalink( $post );
                $tags = array("{first_name}", "{last_name}", "{property_link}");
                $values = array($user->first_name, $user->last_name, $property_link);

                $body = opallisting_get_option('publish_submission_email_body');
                $body = html_entity_decode($body);
                $message = str_replace($tags, $values, $body);

                return wp_mail( $user->user_email, $subject, $message, $headers );
            }
        }
    }

    /**
     *
     */
    public static function columns( $columns ) {
        $comments = $columns['comments'];
        unset( $columns['author'], $columns['date'], $columns['comments'] );
        $columns['featured'] 	= __( 'Featured', 'opallisting' );
        $columns['sku']			= __( 'Sku','opallisting' );
        $columns['address']		= __( 'Address','opallisting' );
        $columns['comments']	= $comments;
        $columns['author']		= __( 'Author', 'opallisting' );
        $columns['date']		= __( 'Date', 'opallisting' );
        return $columns;
    }

    /**
     *
     */
    public static function custom_columns( $column, $post_id ) {
        $property = new Opallisting_Property( $post_id );
        $nonce = wp_create_nonce( 'opallisting_place' );
        switch ( $column ) {
            case 'featured':
                if ( $property->featured ) {
                    $url = add_query_arg( array(
                        'action'		=> 'opallisting_remove_feature_property',
                        'property_id'	=> $post_id,
                        'nonce'			=> $nonce
                    ), admin_url( 'admin-ajax.php' ) );
                    echo '<a href="' . esc_url( $url ) . '">';
                    echo '<i class="dashicons dashicons-star-filled"></i>';
                    echo '</a>';
                } else {
                    $url = add_query_arg( array(
                        'action'		=> 'opallisting_set_feature_property',
                        'property_id'	=> $post_id,
                        'nonce'			=> $nonce
                    ), admin_url( 'admin-ajax.php' ) );
                    echo '<a href="' . esc_url( $url ) . '">';
                    echo '<i class="dashicons dashicons-star-empty"></i>';
                    echo '</a>';
                }
                break;

            case 'sku':
                if ( $property->sku ) {
                    echo sprintf( '%s', $property->sku );
                }
                break;

            case 'address':
                if ( $property->address ) {
                    echo sprintf( '%s', $property->address );
                }
                break;

            default:
                # code...
                break;
        }
    }
}

Opallisting_PostType_Listing_Type_Item::init();