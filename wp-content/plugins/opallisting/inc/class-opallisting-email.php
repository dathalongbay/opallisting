<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    $package$
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2014 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * @class OpalMembership_Checkout
 *
 * @version 1.0
 */
class Opallisting_Emails {

	
	/**
	 *
	 */
	public static function init() {

	  	add_action(  'opallisting_process_new_submission' , array( __CLASS__ , 'new_submission_email'), 10, 2 );
	  	add_action(  'opallisting_process_edit_submission' , array( __CLASS__ , 'new_submission_email'), 10, 2 );
	  	if( is_admin() ){
	  		add_filter( 'opallisting_settings_tabs', array( __CLASS__, 'setting_email_tab'), 1 );
	  		add_filter( 'opallisting_registered_emails_settings', array( __CLASS__, 'setting_email_fields'), 10, 1   );
	  	}
	}

	/**
	 *
	 */
	public static function setting_email_tab( $tabs ){

		$tabs['emails'] = __( 'Email', 'opallisting' );

		return $tabs;

	}

	/**
	 *
	 */
	public static function setting_email_fields( $fields ){ 

		$list_tags = '<td>
				<p class="tags-description">Use the following tags to automatically add booking information to the emails. Tags labeled with an asterisk (*) can be used in the email subject as well.</p>
				
				<div class="rtb-template-tags-box">
					<strong>{property_name}</strong> Email of the user who made the booking
				</div>

				<div class="rtb-template-tags-box">
					<strong>{property_link}</strong> Email of the user who made the booking
				</div>
	
				<div class="rtb-template-tags-box">
					<strong>{user_email}</strong> Email of the user who made the booking
				</div>

				<div class="rtb-template-tags-box">
					<strong>{submitted_date}</strong> Email of the user who made the booking
				</div>

				<div class="rtb-template-tags-box">
					<strong>{user_name}</strong> * Name of the user who made the booking
				</div>
			
				<div class="rtb-template-tags-box">
					<strong>{date}</strong> * Date and time of the booking
				</div>

				<div class="rtb-template-tags-box">
					<strong>{site_name}</strong> The name of this website
				</div>
				<div class="rtb-template-tags-box">
					<strong>{site_link}</strong> A link to this website
				</div>
				<div class="rtb-template-tags-box">
					<strong>{current_time}</strong> Current date and time
				</div></td>';

		$list_tags = apply_filters( 'opallisting_email_tags', $list_tags );
				

		$fields = array(
			'id'         => 'options_page',
			'title' => __( 'Email Settings', 'opallisting' ),
			'show_on'    => array( 'key' => 'options-page', 'value' => array( 'opallisting_settings' ), ),
			'fields'     => apply_filters( 'opallisting_settings_emails', array(
					array(
						'name' => __( 'Email Settings', 'opallisting' ),
						'desc' => '<hr>',
						'id'   => 'opallisting_title_email_settings_1',
						'type' => 'title'
					),
					array(
						'id'      => 'from_name',
						'name'    => __( 'From Name', 'opallisting' ),
						'desc'    => __( 'The name donation receipts are said to come from. This should probably be your site or shop name.', 'opallisting' ),
						'default' => get_bloginfo( 'name' ),
						'type'    => 'text'
					),
					array(
						'id'      => 'from_email',
						'name'    => __( 'From Email', 'opallisting' ),
						'desc'    => __( 'Email to send donation receipts from. This will act as the "from" and "reply-to" address.', 'opallisting' ),
						'default' => get_bloginfo( 'admin_email' ),
						'type'    => 'text'
					),
			 
					array(
						'name' => __( 'Email Templates (Template Tags)', 'opallisting' ),
						'desc' => $list_tags.'<br><hr>',
						'id'   => 'opallisting_title_email_settings_2',
						'type' => 'title'
					),
					array(
						'name' => __( 'Notification For New Property Submission', 'opallisting' ),
						'desc' => '<hr>',
						'id'   => 'opallisting_title_email_settings_3',
						'type' => 'title'
					),
				

					array(
						'id'      			=> 'newproperty_email_subject',
						'name'    			=> __( 'Email Subject', 'opallisting' ),
						'type'    			=> 'text',
						'desc'				=> __( 'The email subject for admin notifications.', 'opallisting' ),
						'attributes'  		=> array(
	        										'placeholder' 		=> 'Your package is expired',
	        										'rows'       	 	=> 3,
	    										),
						'default'			=> __( 'New property submitted - {property_name}', 'opallisting' )

					),
					array(
						'id'      => 'newproperty_email_body',
						'name'    => __( 'Email Body', 'opallisting' ),
						'type'    => 'wysiwyg',
						'desc'	=> __( 'Enter the email an admin should receive when an initial payment request is made.', 'opallisting' ),
						'default' => trim(preg_replace('/\t+/', '','
									Hi {user_name},
									<br>
									Thanks you so much for submitting {property_name}  at  {site_name}:<br>
									 Give us a few moments to make sure that we are got space for you. You will receive another email from us soon.
									 If this request was made outside of our normal working hours, we may not be able to confirm it until we are open again.
									<br>
 									You may review your property at any time by logging in to your client area.
									<br>
									<em>This message was sent by {site_link} on {current_time}.</em>'))	
					),
					//------------------------------------------
					array(
						'name' => __( 'Approve property for publish', 'opallisting' ),
						'desc' => '<hr>',
						'id'   => 'opallisting_title_email_settings_4',
						'type' => 'title'
					),
					array(
						'id'      		=> 'approve_email_subject',
						'name'    		=> __( 'Email Subject', 'opallisting' ),
						'type'    		=> 'text',
						'desc'			=> __( 'The email subject a user should receive when they make an initial booking request.', 'opallisting' ),
						'attributes'  	=> array(
	        									'placeholder' 		=> 'Your booking at I Love WordPress is pending',get_bloginfo( 'name' ),
	        									'rows'       	 	=> 3,
	    									),
						'default'	=> __('Approve For Publish - {property_name}')
					),
					array(
						'id'      	=> 'approve_email_body',
						'name'    	=> __( 'Email Body', 'opallisting' ),
						'type'    	=> 'wysiwyg',
						'desc'		=> __( 'Enter the email a user should receive when they make an initial payment request.', 'opallisting' ),
						'default' 	=> trim(preg_replace('/\t+/', '', "Hi {user_name},<br>
						<br>
						Thank you so much for submitting to {site_name}.
						<br>
						 We have completed the auditing process for your theme '{property_name}'  and are pleased to inform you that your submission has been accepted.
						 <br>
						<br>
						Thanks again for your contribution
						<br>
						&nbsp;<br>
						<br>
						<em>This message was sent by {site_link} on {current_time}.</em>"))
					),
				)
			)
		);
		return $fields;
	}
	/**
	 * get data of newrequest email
	 *
	 * @var $args  array: booking_id , $body 
	 * @return text: message
	 */
	public static function replace_shortcode( $args, $body ) {

		$tags =  array(
			'user_name' 	=> "",
			'user_mail' 	=> "",
			'submitted_date' => "",
			'property_name' => "",
			'site_name' => '',
			'site_link'	=> '',
			'property_link' => '',
		);
		$tags = array_merge( $tags, $args );

		extract( $tags );

		$tags 	 = array( "{user_mail}",
						  "{user_name}",
						  "{submitted_date}",
						  "{site_name}",
						  "{site_link}",
						  "{current_time}",
						  '{property_name}',
						  '{property_link}');

		$values  = array(   $user_mail, 
							$user_name ,
							$submitted_date ,
							get_bloginfo( 'name' ) ,
							get_home_url(), 
							date("F j, Y, g:i a"),
							$property_name,
							$property_link
		);

		$message = str_replace($tags, $values, $body);

		return $message;
	}

	/**
	 * Send email
	 */
	public static function send( $emailto, $subject, $body ){

		$from_name 	= opallisting_get_option('from_name');
		$from_email = opallisting_get_option('from_email');
		$headers 	= sprintf( "From: %s <%s>\r\n Content-type: text/html", $from_name, $from_email );

		wp_mail( @$emailto, @$subject, @$body, $headers );

	}

	/**
	 *
	 */
	public static function get_email_args_by_property(  $property_id ){
	 	
	 	$property 	   = get_post( $property_id );
	 
		$user    	   = get_userdata( $property->post_author ); 
		$email 		   = get_user_meta( $property->post_author, OPALLISTING_USER_PROFILE_PREFIX . 'email', true );

		$args = array(
			'user_mail' 		 => $email,
			'user_name'			 => $user->display_name,
			'submitted_date'	 => $property->post_date,
			'property_name'	 	 => $property->post_title,
			'property_link'		 => get_permalink( $property_id )
		); 

		return $args ;
	}

	/**
	 *
	 */
	public static function new_submission_email( $user_id,  $post_id ){

		 
		$args = self::get_email_args_by_property( $post_id );

		$subject = opallisting_get_option( 'newproperty_email_subject' );
		$body 	 = opallisting_get_option( 'newproperty_email_body' );

		// repleace all custom tags
		$subject = self::replace_shortcode( $args, $subject );
		$body 	 = self::replace_shortcode( $args, $body );
 
		self::send( $args['user_mail'], $subject, $body );
	}


}

Opallisting_Emails::init();