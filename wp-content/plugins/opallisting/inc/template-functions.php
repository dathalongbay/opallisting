<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 *
 */
function opallisting_template_init(){
	if( isset($_GET['display']) && ($_GET['display'] == 'list' || $_GET['display']=='grid') ){
		setcookie( 'opallisting_displaymode', trim($_GET['display']) , time()+3600*24*100,'/' );
		$_COOKIE['opallisting_displaymode'] = trim($_GET['display']);
	}
}
add_action( 'init', 'opallisting_template_init' );

function opallisting_get_current_url(){

	global $wp;
	$current_url = home_url(add_query_arg(array(),$wp->request));

 	return $current_url;
}
/**
 *
 */
function opallisting_get_loop_thumbnail( $size='property-thumbnail' ){ ?>

	<div class="property-box-image">
        <a href="<?php the_permalink(); ?>" class="agent-box-image-inner ">
        	<?php if ( has_post_thumbnail() ) : ?>
            <?php the_post_thumbnail( ); ?>
        	<?php else: ?>
			<?php echo opallisting_get_image_placeholder( 'medium' ); ?>
			<?php endif; ?>
        </a>
	</div>
<?php 
}
/**
 *
 */
function opallisting_render_sortable_dropdown( $selected='', $class='' ){

	$output = '';
		$modes = array(
			'price_asc' 	=> __( 'Price Ascending', 'opallisting' ),
			'price_desc' 	=> __( 'Price Desending', 'opallisting' ),
			'title_asc' 	=> __( 'Title Ascending', 'opallisting' ),
			'title_desc' 	=> __( 'Title Desending', 'opallisting' )
		);
		$modes  = apply_filters( 'opallisting_sortable_modes', $modes );
		$modes  = array_merge( array('' => __('Sort By','opallisting') ), $modes );
		$output = '<form id="opallisting-sortable-form" action="" method="POST"><select name="opalsortable" class="form-control sortable-dropdown" >';
		foreach( $modes as $key => $mode ){

			$sselected = $key == $selected ? 'selected="selected"' : "";
			$output .= '<option '.$sselected.' value="'.$key.'">'.$mode.'</option>';
		}

		$output .= '</select></form>';

	return $output;
}


function opallisting_management_user_menu(){
	global $opallisting_options;
	$menu = array();

	$menu['profile'] = array(
		'icon' 	=> 'fa fa-user',
		'link'	=> opallisting_get_profile_page_uri(),
		'title' =>  __( 'My Profile', 'opallisting'),
		'id'	=> isset( $opallisting_options['profile_page'] ) ? $opallisting_options['profile_page'] : 0
	);

	$menu['favorite'] = array(
		'icon' 	=> 'fa fa-star',
		'link'	=> opallisting_get_favorite_page_uri(),
		'title' =>  __( 'My Favorite', 'opallisting'),
		'id'	=> isset( $opallisting_options['favorite_page'] ) ? $opallisting_options['favorite_page'] : 0
	);

	$menu['submission'] = array(
		'icon' 	=> 'fa fa-upload',
		'link'	=> opallisting_submssion_page(),
		'title' =>  __( 'Submit Property', 'opallisting'),
		'id'	=> isset( $opallisting_options['submission_page'] ) ? $opallisting_options['submission_page'] : 0
	);

	$menu['myproperties'] = array(
		'icon' 	=> 'fa fa-building',
		'link'	=> opallisting_submssion_list_page(),
		'title' =>  __( 'My Properties', 'opallisting'),
		'id'			=> isset( $opallisting_options['submission_list_page'] ) ? $opallisting_options['submission_list_page'] : 0
	);

	$menu = apply_filters( 'opallisting_management_user_menu', $menu );

	$output = '<ul class="account-links nav nav-pills nav-stacked">';

	global $post;

	foreach( $menu as $key => $item ){
		$output .= '<li class="'.( is_object($post) && $post->ID == $item['id'] ? 'active' : '' ).'"><a href="'.$item['link'].'"><i class="'.$item['icon'].'"></i>'.$item['title'].'</a></li>';
	}
	
	$output .= '<li><a href="'.wp_logout_url( home_url('/') ).'"> <i class="fa fa-unlock"></i>'.esc_html__( 'Log out', 'opallisting' ).'</a></li>';
	
	$output .= '</ul>';

	echo $output;
}

/**
 *
 */
function opallisting_show_display_modes( $default = 'list' ){
	global $wp;
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
	$op_display = opallisting_get_display_mode($default);

	echo '<form action="'.  $current_url  .'" class="display-mode" method="get">';
		echo '<button title="'.esc_html__('Grid','opallisting').'" class="btn '.($op_display == 'grid' ? 'active' : '').'" value="grid" name="display" type="submit"><i class="fa fa-th"></i></button>';
		echo '<button title="'.esc_html__( 'List', 'opallisting' ).'" class="btn '.($op_display == 'list' ? 'active' : '').'" value="list" name="display" type="submit"><i class="fa fa-th-list"></i></button>';
	echo '</form>';
}

function opallisting_show_display_status( ){
	global $wp; 
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
	$gstatus = isset($_GET['status'])  ? $_GET['status']:"";
	?>	
	<div id="property-filter-status" class="clearfix">
 
	<?php  
		$statuses = Opallisting_Taxonomy_Status::getList();
		if( $statuses ): 
		echo '<form action="'.  $current_url  .'" id="display-by-status" method="get">';
			 
			echo '<input type="hidden" name="status" value="">';
		echo '</form>';
	?>
		<ul class="list-inline clearfix list-property-status pull-left">
			<li class="status-item <?php if( $gstatus == "" ): ?>active<?php endif; ?>" data-id="all">	
				<span><?php _e( 'All', 'opallisting' ); ?></span>
			</li>	
			<?php foreach( $statuses as $status ):  ?>

			<li class="status-item <?php if( $status->slug == $gstatus): ?> active <?php endif; ?>" data-id="<?php echo $status->slug; ?>">
				<span><?php echo $status->name; ?> </span> 
			</li>	
			<?php endforeach; ?>
		</ul>	
		<?php endif; ?>
	</div>	
<?php 
}



function opallisting_get_display_mode( $default = '' ){
	$op_display = $default ? $default : opallisting_options('displaymode', 'grid');
	if ( isset($_COOKIE['opallisting_displaymode']) ) {
		$op_display = $_COOKIE['opallisting_displaymode'];
	}
	return $op_display;
}

/**
 *
 */
function opallisting_get_search_link() {
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-templates/page-property-search-results.php'
    ));

    if( $pages ) {
        $search_submit = get_permalink($pages[0]->ID);
    } else {
        $search_submit = '';
    }

    return $search_submit;
}

function opallisting_submssion_page( $id = false ){
	$page = get_permalink( opallisting_get_option( 'submission_page', '/' ) );
	if ( $id ) {
		$edit_page_id = opallisting_get_option( 'submission_edit_page' );
		$page = $edit_page_id ? get_permalink( $edit_page_id ) : $page;
		$page = add_query_arg( 'id', $id, $page );
	}
	return $page;
}


function opallisting_get_user_properties_uri( $args = array() ) {
  
    global $opallisting_options;

    $uri = isset( $opallisting_options['submission_list_page'] ) ? get_permalink( absint( $opallisting_options['submission_list_page'] ) ) : get_bloginfo( 'url' );

    if ( ! empty( $args ) ) {
        // Check for backward compatibility
        if ( is_string( $args ) )
            $args = str_replace( '?', '', $args );
        $args = wp_parse_args( $args );
        $uri = add_query_arg( $args, $uri );
    }

    $scheme = defined( 'FORCE_SSL_ADMIN' ) && FORCE_SSL_ADMIN ? 'https' : 'admin';

    $ajax_url = admin_url( 'admin-ajax.php', $scheme );

    if ( ( ! preg_match( '/^https/', $uri ) && preg_match( '/^https/', $ajax_url ) )  ) {
        $uri = preg_replace( '/^http:/', 'https:', $uri );
    }
    return apply_filters( 'opallisting_get_user_properties_uri', $uri );
}


function opallisting_submssion_list_page( $args = array() ) {
  
    global $opallisting_options;

    $uri = isset( $opallisting_options['submission_list_page'] ) ? get_permalink( absint( $opallisting_options['submission_list_page'] ) ) : get_bloginfo( 'url' );

    if ( ! empty( $args ) ) {
        // Check for backward compatibility
        if ( is_string( $args ) )
            $args = str_replace( '?', '', $args );
        $args = wp_parse_args( $args );
        $uri = add_query_arg( $args, $uri );
    }

    $scheme = defined( 'FORCE_SSL_ADMIN' ) && FORCE_SSL_ADMIN ? 'https' : 'admin';

    $ajax_url = admin_url( 'admin-ajax.php', $scheme );

    if ( ( ! preg_match( '/^https/', $uri ) && preg_match( '/^https/', $ajax_url ) )  ) {
        $uri = preg_replace( '/^http:/', 'https:', $uri );
    }
    return apply_filters( 'opallisting_get_user_properties_uri', $uri );
}

function opallisting_get_profile_page_uri() {

    global $opallisting_options;

    $profile_page = isset( $opallisting_options['profile_page'] ) ? get_permalink( absint( $opallisting_options['profile_page'] ) ) : get_bloginfo( 'url' );

    return apply_filters( 'opallisting_get_register_page_uri', $profile_page );
}


function opallisting_get_favorite_page_uri() {

    global $opallisting_options;

    $favorite_page = isset( $opallisting_options['favorite_page'] ) ? get_permalink( absint( $opallisting_options['favorite_page'] ) ) : get_bloginfo( 'url' );

    return apply_filters( 'opallisting_get_favorite_page_uri', $favorite_page );
}

function opallisting_single_layout_templates( $layout ){
	$layout['v2'] = __( 'Vesion 2', 'opallisting' );
	return $layout;	
}
add_filter( 'opallisting_single_layout_templates', 'opallisting_single_layout_templates' );

function opallisting_single_the_property_layout(){

	global $opallisting_options;

	$layout = get_post_meta( get_the_ID(), OPALLISTING_PROPERTY_PREFIX . 'layout', true );

	if( !$layout ){
		$layout = isset( $opallisting_options['layout'] ) ? $opallisting_options['layout']: '';
	}

	return $layout;
}

function opallisting_property_featured_label(){
	echo Opallisting_Template_Loader::get_template_part( 'parts/featured-label' );
}

add_action( 'opallisting_before_property_loop_item' , 'opallisting_property_featured_label' );
/**
 * Single property logic functions
 */

/**
 * Single property logic functions
 */
function opallisting_property_meta(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/meta' );
}

/**
 * Single property logic functions
 */
function opallisting_property_preview(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/preview' );
}

/**
 *
 */
function opallisting_property_content(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/content' );
}

/**
 *
 */
function opallisting_property_information(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/information' );
}

/**
 *
 */
function opallisting_property_amenities(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/amenities' );
}

function opallisting_property_attachments(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/attachments' );
}

function opallisting_property_tags(){
	return the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' );
}

function opallisting_property_map(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/map' );
}

function opallisting_property_map_v2(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/map-v2' );
}

function opallisting_property_author(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/author' );
}

function opallisting_property_video(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/video' );
}

function opallisting_properties_same_agent(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/sameagent' );
}

function opallisting_property_location(){
	echo Opallisting_Template_Loader::get_template_part( 'single-property/location' );
}

add_action( 'opallisting_single_property_preview', 'opallisting_property_preview', 15 );

add_action( 'opallisting_single_property_summary', 'opallisting_property_information', 15 );

add_action( 'opallisting_after_single_property_summary', 'opallisting_property_amenities', 15 );
add_action( 'opallisting_after_single_property_summary', 'opallisting_property_attachments', 15 );

add_action( 'opallisting_after_single_property_summary', 'opallisting_property_video', 20 );
add_action( 'opallisting_after_single_property_summary', 'opallisting_property_map', 25 );
add_action( 'opallisting_after_single_property_summary', 'opallisting_property_author', 30 );
add_action( 'opallisting_after_single_property_summary', 'opallisting_property_tags', 35 );

add_action( 'opallisting_after_single_property_summary_v2', 'opallisting_property_map_v2', 5 );

/**
 *
 */
add_action( 'opallisting_single_property_sameagent', 'opallisting_properties_same_agent', 5 );

function opallisting_agent_summary() {
	echo Opallisting_Template_Loader::get_template_part( 'single-agent/summary' );
}

function opallisting_agent_properties() {
	echo Opallisting_Template_Loader::get_template_part( 'single-agent/properties' );
}

function opallisting_agent_contactform() {
	global $post;
	$args = array( 'post_id' => $post->ID );
	echo Opallisting_Template_Loader::get_template_part( 'single-agent/form', $args );
}

add_action( 'opallisting_single_agent_summary', 'opallisting_agent_summary', 5 );
add_action( 'opallisting_single_content_agent_after', 'opallisting_agent_properties', 15 );

/**
 *
 */
function opallisting_agent_navbar(){

}
add_action( 'opallisting_single_agent_summary', 'opallisting_agent_navbar', 5 );
