<?php


class Opallisting_Comment {


    public static function init(){

        // show extend field comment
        add_action( 'comment_form_logged_in_after', array( __CLASS__, 'additional_fields' ) ); // logged in
        add_action( 'comment_form_after_fields', array( __CLASS__, 'additional_fields' ) ); // guest

        // Save Rating Field on Comment Post
        add_action( 'comment_post', array( __CLASS__, 'save_comment_meta_data'));

        // display rating each comment
        add_filter( 'comment_text', array( __CLASS__, 'displayRating' ));

        // Displays Average Rating below Content
        add_filter('the_content', array( __CLASS__, 'displayAverageRatingPost'));

        if(is_admin()){

            add_action( 'add_meta_boxes_comment', array(__CLASS__, 'extend_comment_add_meta_box') );

            add_action('wp_set_comment_status', array(__CLASS__, 'update_post_rating')); // Recalculate average rating on comment approval / hold / spam
            add_action('deleted_comment', array(__CLASS__, 'update_post_rating')); // Recalculate average rating on comment delete

        }


        add_action( 'edit_comment', array(__CLASS__, 'extend_comment_edit_metafields'));
    }



    /**
     * Display comment rating
     */
    function additional_fields () {

        global $post;

        if($post->post_type == "opallisting_place"){
            ?>

            <p class="comment-form-rating">
                <label for="rating"><?php echo __('Rating') ?><span class="required">*</span></label>
	        <span class="commentratingbox">

                    <span class="commentrating"><input type="radio" name="rating" id="rating" value="1"/>1</span>
                    <span class="commentrating"><input type="radio" name="rating" id="rating" value="2"/>2</span>
                    <span class="commentrating"><input type="radio" name="rating" id="rating" value="3"/>3</span>
                    <span class="commentrating"><input type="radio" name="rating" id="rating" value="4"/>4</span>
                    <span class="commentrating"><input type="radio" name="rating" id="rating" value="5"/>5</span>
                </span></p>
            <?php
        }
        ?>

        <?php

    }


    /**
     * Save rating
     */
    function save_comment_meta_data( $comment_id ) {

        if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') )
            $rating = wp_filter_nohtml_kses($_POST['rating']);
            add_comment_meta( $comment_id, 'rating', $rating );

        Opallisting_Comment::update_post_rating($comment_id);
    }


    /**
     * Update post rating
     *
     * @param $comment_id
     */
    public static function update_post_rating($comment_id){
        $comment = get_comment($comment_id);

        $postID = $comment->comment_post_ID;


        // Get all approved comments and total the number of ratings and rating values for fields
        $comments = get_comments(array(
            'post_id' 	=> $postID,
            'status' 	=> 'approve',
        ));

        // Calculate
        $totalRating = 0;
        $totalRatings = 0;
        $averageRating = 0;
        if (is_array($comments) AND count($comments) > 0) {
            // Iterate through comments
            foreach ($comments as $comment) {
                $rating = get_comment_meta($comment->comment_ID, 'rating', true);
                if ($rating > 0) {
                    $totalRatings++;
                    $totalRating += $rating;
                }
            }

            // Calculate average rating
            $averageRating = (($totalRatings == 0 OR $totalRating == 0) ? 0 : round(($totalRating / $totalRatings), 0));
        }



        update_post_meta($postID, 'total-ratings', $totalRatings);
        update_post_meta($postID, 'average-rating', $averageRating);
    }


    /**
     * display rating comment
     *
     * @param $comment
     * @return string
     */
    function displayRating($comment) {
        global $post;

        $commentID = get_comment_ID();

        if($post->post_type == "opallisting_place"){
            $rating = get_comment_meta($commentID, 'rating', true);
            if ($rating == '') $rating = 0;
            $html = "<div><strong>$rating Star</strong></div>";
            return $comment.$html;
        }

        return $comment;
    }


    /**
     * display rating post
     */
    function displayAverageRatingPost(){
        global $post;

        $average_rating = get_post_meta($post->ID, 'average-rating', true);
        echo $average_rating;
    }


    /**
     * Add and edit comment in backend
     */
    function extend_comment_add_meta_box() {
        add_meta_box( 'title', __( 'Comment Metadata - Extend Comment' ), array(__CLASS__, 'extend_comment_meta_box'), 'comment', 'normal', 'high' );
    }

    function extend_comment_meta_box ( $comment ) {

        $post = get_post($comment->comment_post_ID);

        if($post->post_type == "opallisting_place"){
            $rating = get_comment_meta( $comment->comment_ID, 'rating', true );
            wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );

            ?>
            <p>
                <label for="rating"><?php _e( 'Rating: ' ); ?></label>
			<span class="commentratingbox">
			<?php for( $i=1; $i <= 5; $i++ ) {
                echo '<span class="commentrating"><input type="radio" name="rating" id="rating" value="'. $i .'"';
                if ( $rating == $i ) echo ' checked="checked"';
                echo ' />'. $i .' </span>';
            }
            ?>
			</span>
            </p>
            <?php
        }
    }

    // Update comment meta data from comment edit screen
    function extend_comment_edit_metafields( $comment_id ) {
        if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) ) return;

        if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') ):
            $rating = wp_filter_nohtml_kses($_POST['rating']);
            update_comment_meta( $comment_id, 'rating', $rating );
        else :
            delete_comment_meta( $comment_id, 'rating');
        endif;

    }




}


Opallisting_Comment::init();