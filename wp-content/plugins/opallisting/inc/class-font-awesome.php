<?php

class Opallisting_font_awesome{

    private $prefix;

    private $data = array();

    public function __construct($path, $fa_css_prefix = 'fa')
    {
        $this->prefix = $fa_css_prefix;

        $css = file_get_contents($path);

        $pattern = '/\.('.$fa_css_prefix.'-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';

        preg_match_all($pattern, $css, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {

            // Set Basic Data
            $item = array();
            $item['class'] = $match[1];
            $item['unicode'] = $match[2];
            $this->data[] = $item;
        }

    }

    public function getIcons(){
        return $this->data;
    }

    public function getPrefix()
    {
        return (string) $this->prefix;
    }
}