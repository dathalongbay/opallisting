<?php
class Opallisting_Custom_Field_Select{

    private $required;
    private $expanded;
    private $title;
    private $meta_key;
    private $default_value;
    private $default_value_index;
    private $placeholder;
    private $icon_data;
    private $icon;

    function __construct1($required = 'no',$title = '', $meta_key = '', $default_value = '', $placeholder = '', $expanded = false, $icon_pack = '', $icon = ''){
        $this->required = $required;
        $this->title = $title;
        $this->meta_key = $meta_key;
        $this->default_value = $default_value;
        $this->placeholder = $placeholder;
        $this->expanded = $expanded;
        $this->icon_pack = $icon_pack;
        $this->icon = $icon;
    }

    function __construct($title = '', $options = array(), $meta_key = '', $default_value = '', $expanded = false, $icon_data = '', $icon = ''){
        $this->title = $title;
        $this->meta_key = $meta_key;
        $this->default_value = $default_value;
        $this->expanded = $expanded;
        $this->options = $options;
        $this->icon_data = $icon_data;
        $this->icon = $icon;
    }

    public function render_blank(){
        ?>
        <div class="panel-group select-container" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="toggle-panel">
                            Select</a>
                        <a href="#" class="remove-custom-field-item" style="display: block; float: right;">x</a>
                    </h4>
                </div>
                <div class="panel-body" style="display: none">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Metakey</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_meta_key[]" class="form-control" placeholder="Enter metakey" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_title[]" class="form-control" placeholder="Enter title" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Options</label>
                        <div class="col-sm-10 options-container">
                            <a href="#" class="btn btn-info add-new-options">Add New Item</a>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-sm-2">Icon</label>
                        <div class="col-sm-10">
                            <select class="fa-icon-picker" name="cf_icon[]">
                                <option value=""></option>
                                <?php
                                foreach ($this->icon_data as $icon_item) { ?>
                                    <option value="fa <?php echo $icon_item['class'] ?>"><?php echo $icon_item['class'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="cf_type[]" value="select" />
            <input type="hidden" name="cf_select_id[]" class="cf_select_id opallisting-select-index" value="" />
        </div>
        <?php
    }

    public function render($i = 0){
        ?>
        <div class="panel-group select-container" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="toggle-panel">
                            Select : <?php echo $this->title ?></a>
                        <a href="#" class="remove-custom-field-item" style="display: block; float: right;">x</a>
                    </h4>

                </div>
                <div class="panel-body" style="display: none">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Metakey</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_meta_key[]" class="form-control" placeholder="Enter metakey" value="<?php echo $this->meta_key ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_title[]" class="form-control" placeholder="Enter title" value="<?php echo $this->title; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Options</label>
                        <div class="col-sm-10 options-container">
                            <a href="#" class="btn btn-info add-new-options">Add New Item</a>

                            <?php
                            $index = 0;
                            foreach($this->options as $option_item_key => $option_item){
                                $checked = false;
                                if($option_item_key == $this->default_value){
                                    $checked = true;
                                }

                                ?>
                            <div class="row option-row" style="margin-top: 10px; margin-bottom: 10px">
                                <div class="col-lg-4 col-md-4">
                                    <div>
                                        <strong>Label</strong>
                                    </div>
                                    <div style="margin-top: 10px">
                                        <input type="text" name="opal_custom_select_options_label[<?php echo $i ?>][]" class="opallisting-options-label form-control" value="<?php echo $option_item ?>" />
                                    </div>

                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div>
                                        <strong>Value</strong>
                                    </div>
                                    <div style="margin-top: 10px">
                                        <input type="text" name="opal_custom_select_options_value[<?php echo $i ?>][]" class="opallisting-options-value form-control" value="<?php echo $option_item_key; ?>" />
                                    </div>

                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <div>
                                        <strong>Default value</strong>
                                    </div>
                                    <div style="margin-top: 10px">
                                        <input type="radio" class="opallisting-options-default" name="opal_custom_select_options_default[<?php echo $i ?>]" <?php echo $checked ? 'checked' : ''; ?> value="<?php echo $index ?>">
                                    </div>

                                </div>
                                <div class="col-lg-1 col-md-1">
                                    <a href="#" class="opallisting-remove-option">x</a>
                                </div>
                            </div>

                                <?php
                                $index++;
                            }
                            ?>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Icon</label>
                        <div class="col-sm-10">
                            <select class="fa-icon-picker" name="cf_icon[]">
                                <option value=""></option>
                                <?php
                                foreach ($this->icon_data as $icon_item) { ?>
                                    <option <?php echo ($this->icon == "fa " . $icon_item["class"]) ? 'selected="selected"' : ""; ?> value="fa <?php echo $icon_item['class'] ?>"><?php echo $icon_item['class'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <input type="hidden" name="cf_type[]" value="select" />
            <input type="hidden" name="cf_select_id[]" class="cf_select_id opallisting-select-index" value="<?php echo $i; ?>" />
        </div>
        <?php
    }
}