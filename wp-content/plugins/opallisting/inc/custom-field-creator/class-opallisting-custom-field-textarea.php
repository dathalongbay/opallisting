<?php
class Opallisting_Custom_Field_Textarea{

    private $required;
    private $expanded;
    private $title;
    private $meta_key;
    private $default_value;
    private $placeholder;
    private $icon_data;
    private $icon;

    function __construct($required = 'no',$title = '', $meta_key = '', $default_value = '', $placeholder = '', $expanded = false, $icon_data = '', $icon = ''){
        $this->required = $required;
        $this->title = $title;
        $this->meta_key = $meta_key;
        $this->default_value = $default_value;
        $this->placeholder = $placeholder;
        $this->expanded = $expanded;
        $this->icon_data = $icon_data;
        $this->icon = $icon;
    }

    public function render_blank(){
        ?>
        <div class="panel-group" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="toggle-panel">
                            TextArea</a>
                        <a href="#" class="remove-custom-field-item" style="display: block; float: right;">x</a>
                    </h4>
                </div>
                <div class="panel-body" style="display: none">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Metakey</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_meta_key[]" class="form-control" placeholder="Enter metakey">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_title[]" class="form-control" placeholder="Enter title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Default value</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_default_value[]" class="form-control" placeholder="Enter default value">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Icon</label>
                        <div class="col-sm-10">
                            <select class="fa-icon-picker" name="cf_icon[]">
                                <option value=""></option>
                                <?php
                                foreach ($this->icon_data as $icon_item) { ?>
                                    <option value="fa <?php echo $icon_item['class'] ?>"><?php echo $icon_item['class'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="cf_type[]" value="textarea" />
        </div>
        <?php
    }

    public function render(){
        ?>
        <div class="panel-group" >
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="toggle-panel">
                            TextArea : <?php echo $this->title ?></a>
                        <a href="#" class="remove-custom-field-item" style="display: block; float: right;">x</a>
                    </h4>

                </div>
                <div class="panel-body" style="display: none">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Metakey</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_meta_key[]" class="form-control" placeholder="Enter metakey" value="<?php echo $this->meta_key ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Title</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_title[]" class="form-control" placeholder="Enter title" value="<?php echo $this->title; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Default value</label>
                        <div class="col-sm-10">
                            <input type="text" name="cf_default_value[]" class="form-control" placeholder="Enter default value" value="<?php echo $this->default_value ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Icon</label>
                        <div class="col-sm-10">
                            <select class="fa-icon-picker" name="cf_icon[]">
                                <option value=""></option>
                                <?php
                                foreach ($this->icon_data as $icon_item) { ?>
                                    <option <?php echo ($this->icon == "fa " . $icon_item["class"]) ? 'selected="selected"' : ""; ?> value="fa <?php echo $icon_item['class'] ?>"><?php echo $icon_item['class'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="cf_type[]" value="textarea" />
        </div>
        <?php
    }
}