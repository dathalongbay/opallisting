<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OpalListing_Search{
		

	public static function get_search_agents_query(  $args=array()  ){
		$min = opallisting_options( 'search_agent_min_price',0 );
		$max = opallisting_options( 'search_agent_max_price',10000000 );
		

		$search_min_price = isset($_GET['min_price']) ? sanitize_text_field($_GET['min_price']) : '';
        $search_max_price = isset($_GET['max_price']) ? sanitize_text_field($_GET['max_price']) : '';

        $search_min_area = isset($_GET['min_area']) ? sanitize_text_field($_GET['min_area']) : '';
        $search_max_area = isset($_GET['max_area']) ? sanitize_text_field($_GET['max_area']) : '';
        $s = isset($_GET['search_text']) ? sanitize_text_field($_GET['search_text']) : null;


		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
		$default = array(
			'post_type'         => 'opallisting_agent',
			'posts_per_page'    => apply_filters('opallisting_agent_per_page' , 12 ),
			'paged'				=> $paged,
		);
		$args = array_merge( $default, $args );

		$tax_query = array();

		 
		if( isset( $_GET['location']) &&  $_GET['location'] !=-1 ){
			$tax_query[] = 
			    array(
			        'taxonomy' => 'opallisting_location',
			        'field'    => 'slug',
			        'terms'    => $_GET['location'],
			    );
		}
		
		if( isset( $_GET['types']) && $_GET['types'] !=-1 ){ 
			$tax_query[] =
			    array(
			        'taxonomy' => 'opallisting_types',
			        'field'    => 'slug',
			        'terms'    => $_GET['types'],
			    )
			;
		}
	 
		if( $tax_query  ){
			$args['tax_query'] = array('relation' => 'AND');
			$args['tax_query'] = array_merge( $args['tax_query'], $tax_query );
		}

		$args['meta_query'] = array('relation' => 'AND');
		 
 		if($search_min_price != $min && is_numeric($search_min_price)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_AGENT_PREFIX.'target_min_price',
                'value'   => $search_min_price,
                'compare' => '>=',
                'type' => 'NUMERIC'
            ));
        } 
        if( is_numeric($search_max_price) && $search_max_price != $max ) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_AGENT_PREFIX.'target_max_price',
                'value'   => $search_max_price,
                'compare' => '<=',
                'type' => 'NUMERIC'
            ));
        }

        
  		//  echo '<pre>'.print_r( $args ,1 );die;

 		return new WP_Query( $args );
	}


	 

	/**
	 *
	 */
	public static function get_search_results_query( $limit=9 ){

		global $paged;

		$search_min_price = isset($_GET['min_price']) ? sanitize_text_field($_GET['min_price']) : '';
        $search_max_price = isset($_GET['max_price']) ? sanitize_text_field($_GET['max_price']) : '';

        $search_min_area = isset($_GET['min_area']) ? sanitize_text_field($_GET['min_area']) : '';
        $search_max_area = isset($_GET['max_area']) ? sanitize_text_field($_GET['max_area']) : '';
        $s = isset($_GET['search_text']) ? sanitize_text_field($_GET['search_text']) : null;

		$posts_per_page = apply_filters('opallisting_property_per_page' , $limit );

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

		$infos = array();

		$args = array(
            'posts_per_page' => $posts_per_page,
            'paged' 		 => $paged,
            'post_type' 	 => 'opallisting_place',
            'post_status' 	 => 'publish',
            's'		=> $s
        );

		$tax_query = array();

		if( isset( $_GET['location']) &&  $_GET['location'] !=-1 ){
			$tax_query[] = 
			    array(
			        'taxonomy' => 'opallisting_location',
			        'field'    => 'slug',
			        'terms'    => $_GET['location'],
			    );
		}

		if( isset( $_GET['types']) && $_GET['types'] !=-1 ){ 
			$tax_query[] =
			    array(
			        'taxonomy' => 'opallisting_types',
			        'field'    => 'slug',
			        'terms'    => $_GET['types'],
			    )
			;
		}
		if( isset( $_GET['status']) &&  $_GET['status'] !=-1 ){ 
			$tax_query[] = 
			    array(
			        'taxonomy' => 'opallisting_status',
			        'field'    => 'slug',
			        'terms'    => $_GET['status'],
			   
			);
		}

		if( $tax_query  ){
			$args['tax_query'] = array('relation' => 'AND');
			$args['tax_query'] = array_merge( $args['tax_query'], $tax_query );
		}

		$args['meta_query'] = array('relation' => 'AND');
		if( isset($_GET['info']) ){

			$metaquery = array();
			foreach( $_GET['info'] as $key => $value ){
				if( trim($value) ){
					$fieldquery = array(
						'key'     => OPALLISTING_PROPERTY_PREFIX.$key,
	                	'value'   => sanitize_text_field(trim($value)),
	                	'compare' => '>=',
	                	'type' => 'NUMERIC'

					);
					$sarg = apply_filters('opallisting_search_field_query_'.$key, $fieldquery );
					$metaquery[] = $sarg;
				}
			}
	 		$args['meta_query'] = array_merge( $args['meta_query'], $metaquery );
		}

		if($search_min_price != '' && $search_min_price != '' && is_numeric($search_min_price) && is_numeric($search_max_price)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_PROPERTY_PREFIX.'price',
                'value'   => array($search_min_price, $search_max_price),
                'compare' => 'BETWEEN',
                'type' => 'NUMERIC'
            ));
        } else if($search_min_price != '' && is_numeric($search_min_price)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_PROPERTY_PREFIX.'price',
                'value'   => $search_min_price,
                'compare' => '>=',
                'type' => 'NUMERIC'
            ));
        } else if($search_max_price != '' && is_numeric($search_max_price)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_PROPERTY_PREFIX.'price',
                'value'   => $search_max_price,
                'compare' => '<=',
                'type' => 'NUMERIC'
            ));
        }

        if($search_min_area != '' && $search_min_area != '' && is_numeric($search_min_area) && is_numeric($search_max_area)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_PROPERTY_PREFIX.'areasize',
                'value'   => array($search_min_area, $search_max_area),
                'compare' => 'BETWEEN',
                'type' => 'NUMERIC'
            ));
        } else if($search_min_area != '' && is_numeric($search_min_area)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_PROPERTY_PREFIX.'areasize',
                'value'   => $search_min_area,
                'compare' => '>=',
                'type' => 'NUMERIC'
            ));
        } else if($search_max_area != '' && is_numeric($search_max_area)) {
            array_push($args['meta_query'], array(
                'key'     => OPALLISTING_PROPERTY_PREFIX.'areasize',
                'value'   => $search_max_area,
                'compare' => '<=',
                'type' => 'NUMERIC'
            ));
        }
    
     
		$query = new WP_Query($args);

        wp_reset_postdata();

        return $query;
	}

	/**
	 *
	 */
	public static function get_setting_search_fields( $option='' ){
		$options = array(
			OPALLISTING_PROPERTY_PREFIX.'bedrooms' => __('Bed Rooms', 'opallisting'),
			OPALLISTING_PROPERTY_PREFIX.'parking' =>  __('Parking', 'opallisting'),
			OPALLISTING_PROPERTY_PREFIX.'bedrooms' => __('Bed Rooms', 'opallisting'),
		);

		$default = apply_filters( 'opallisting_default_fields_setting', $options );
		$metas = Opallisting_PostType_Place::metaboxes_info_fields();

		$esettings = array();

		foreach( $metas as $key => $meta ){

			if( opallisting_options($meta['id'].'_opt'.$option) ){
				$id = str_replace( OPALLISTING_PROPERTY_PREFIX, "", $meta['id'] );
				$esettings[$id] = $meta['name'];
			}
		}

		if( !empty($esettings) ){
			return $esettings;
		}
		return $default;
	}

	/**
	 *
	 */
	public static function init(){
		add_action( 'wp_ajax_opallisting_ajx_get_properties', array( __CLASS__, 'get_search_json' ) );
		add_action( 'wp_ajax_nopriv_opallisting_ajx_get_properties', array( __CLASS__, 'get_search_json' ) );
	//	add_filter( "pre_get_posts",   array( __CLASS__, 'change_archive_query' )   );
	}

	/**
	 *
	 */
	public static function get_search_json(){

		$query = self::get_search_results_query();

		$output = array();

		while( $query->have_posts() ) {

	        $query->the_post();
			$property = opallisting_place( get_the_ID() );
			$output[]  = $property->get_meta_search_objects();
	    }

	    wp_reset_query();

	    echo json_encode( $output ); exit;
	}

	/**
	 *
	 */
	public static function render_horizontal_form( $atts=array() ){
		echo Opallisting_Template_Loader::get_template_part( 'parts/search-form-h', $atts );
	}

	/**
	 *
	 */
	public static function render_vertical_form(  $atts=array() ){

		echo Opallisting_Template_Loader::get_template_part( 'parts/search-form-v' , $atts );
	}

	/**
	 *
	 */
	public static function render_field_price(){

	}

	/**
	 *
	 */
	public static function render_field_area(){

	}
}

OpalListing_Search::init();
?>