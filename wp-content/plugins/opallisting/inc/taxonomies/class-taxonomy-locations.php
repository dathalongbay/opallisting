<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Opallisting_Taxonomy_Location{

	/**
	 *
	 */
	public static function init(){
		add_action( 'init', array( __CLASS__, 'definition' ) );
		add_filter( 'opallisting_taxomony_location_metaboxes', array( __CLASS__, 'metaboxes' ) );
	}

	/**
	 *
	 */
	public static function definition(){
		
		$labels = array(
			'name'              => __( 'Locations', 'opallisting' ),
			'singular_name'     => __( 'Properties By Location', 'opallisting' ),
			'search_items'      => __( 'Search Locations', 'opallisting' ),
			'all_items'         => __( 'All Locations', 'opallisting' ),
			'parent_item'       => __( 'Parent Location', 'opallisting' ),
			'parent_item_colon' => __( 'Parent Location:', 'opallisting' ),
			'edit_item'         => __( 'Edit Location', 'opallisting' ),
			'update_item'       => __( 'Update Location', 'opallisting' ),
			'add_new_item'      => __( 'Add New Location', 'opallisting' ),
			'new_item_name'     => __( 'New Location', 'opallisting' ),
			'menu_name'         => __( 'Locations', 'opallisting' ),
		);

		register_taxonomy( 'opallisting_location', 'opallisting_place', array(
			'labels'            => apply_filters( 'opallisting_taxomony_location_labels', $labels ),
			'hierarchical'      => true,
			'query_var'         => 'property-location',
			'rewrite'           => array( 'slug' => __( 'property-location', 'opallisting' ) ),
			'public'            => true,
			'show_ui'           => true,
		) );
	}


	public static function metaboxes(){

	}


	public static function getList(){
		 return get_terms('opallisting_location', array('hide_empty'=> false));
	}
	
	public static function dropdownAgentsList( $selected=0  ){
		$id = "opallisting_location".rand();
		$args = array( 
				'show_option_none' => __( 'Select Location', 'opallisting' ),
				'id' => $id,
				'class' => 'form-control',
				'name'	=> 'location',
				'show_count' => 0,
				'hierarchical'	=> '',
				'selected'	=> $selected,
				'value_field'	=> 'slug',
				'taxonomy'	=> 'opallisting_agent_location'
		);		

		return wp_dropdown_categories( $args );
	}

	public static function dropdownList( $selected=0 ){
		$id = "opallisting_location".rand();
		$args = array( 
				'show_option_none' => __( 'Select Location', 'opallisting' ),
				'id' => $id,
				'class' => 'form-control',
				'name'	=> 'location',
				'show_count' => 0,
				'hierarchical'	=> '',
				'selected'	=> $selected,
				'value_field'	=> 'slug',
				'taxonomy'	=> 'opallisting_location'
		);		

		return wp_dropdown_categories( $args );
	}
}

Opallisting_Taxonomy_Location::init();