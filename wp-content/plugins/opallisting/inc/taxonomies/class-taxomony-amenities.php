<?php
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Opallisting_Taxonomy_Amenities{

	/**
	 *
	 */
	public static function init(){
		add_action( 'init', array( __CLASS__, 'definition' ) );
		add_filter( 'opallisting_taxomony_amenities_metaboxes', array( __CLASS__, 'metaboxes' ) );
	}

	/**
	 *
	 */
	public static function definition(){
		
		$labels = array(
			'name'              => __( 'Amenities', 'opallisting' ),
			'singular_name'     => __( 'Properties By Amenity', 'opallisting' ),
			'search_items'      => __( 'Search Amenities', 'opallisting' ),
			'all_items'         => __( 'All Amenities', 'opallisting' ),
			'parent_item'       => __( 'Parent Amenity', 'opallisting' ),
			'parent_item_colon' => __( 'Parent Amenity:', 'opallisting' ),
			'edit_item'         => __( 'Edit Amenity', 'opallisting' ),
			'update_item'       => __( 'Update Amenity', 'opallisting' ),
			'add_new_item'      => __( 'Add New Amenity', 'opallisting' ),
			'new_item_name'     => __( 'New Amenity', 'opallisting' ),
			'menu_name'         => __( 'Amenities', 'opallisting' ),
		);

		register_taxonomy( 'opallisting_amenities', 'opallisting_place', array(
			'labels'            => apply_filters( 'opallisting_taxomony_amenities_labels', $labels ),
			'hierarchical'      => true,
			'query_var'         => 'property-amenity',
			'rewrite'           => array( 'slug' => _x( 'opal-property-amenity', 'slug', 'opallisting' ), 'with_front' => false, 'hierarchical' => true ),
			'public'            => true,
			'show_ui'           => true,
		) );
	}

	public static function metaboxes(){

	}


}

Opallisting_Taxonomy_Amenities::init();