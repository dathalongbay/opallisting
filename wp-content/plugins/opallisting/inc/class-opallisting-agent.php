<?php 
/**
 * $Desc$
 *
 * @version    $Id$
 * @package    opallisting
 * @author     Opal  Team <info@wpopal.com >
 * @copyright  Copyright (C) 2016 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/support/forum.html
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * @class OpalListing_Agent
 *
 * @version 1.0
 */
class OpalListing_Agent{

	protected $content; 
	protected $author_name; 
	
	protected $is_featured; 
	static $_agents = array();

	public function __construct( $post_id=null ){

		global $post;

 		$this->post = $post;
	 	$this->post_id = get_the_ID();
		$this->author 		 = get_userdata( $post->post_author );
		$this->author_name = !empty($this->author)? sprintf('%s %s', $this->author->first_name, $this->author->last_name):null;
		$this->is_featured 	= $this->getMeta( 'featured' ); 

	}

	public function get_socials(){
		$socials = array( 'facebook' 	=> '',
						  'twitter' 	=> '',
						  'pinterest' 	=> '',
						  'google' 		=> '',
						  'instagram' 	=> '',
						  'linkedIn' 	=> ''
		);
		
		$output = array();

		foreach( $socials as $social => $k ){
			$output[$social] = $this->getMeta( $key );
		} 

		return $output;
	}

	public static function get_avatar_url( $userID ){
	
		return get_post_meta( $userID , OPALLISTING_AGENT_PREFIX . "avatar", true );

	}

 	public function render_level(){
		$levels = wp_get_post_terms( $this->post_id, 'opallisting_agent_level' );

		if( empty($levels) ){
			return ;
		}

		$output = '<span class="agent-levels">';
		foreach( $levels as $key => $value ){
			$output .= '<span class="agent-label"><span>'.$value->name.'</span></span>';
		}
		$output .= '</span>';

		echo $output;
	}

	public function getMeta( $key ){
		return get_post_meta( get_the_ID(), OPALLISTING_AGENT_PREFIX .  $key, true );
	}

	public static function render_contact_form( $post_id ){
		echo Opallisting_Template_Loader::get_template_part( 'single-agent/form', array('post_id' => $post_id) );
	}

	public function is_featured(){
		return $this->is_featured;
	}

	public static function render_box_info( $post_id ){
			$args = array(
				'post_type' => 'opallisting_agent',
				'p'	=> $post_id
			);
			$loop = new WP_Query($args);

			if( $loop->have_posts() ){
				while( $loop->have_posts() ){  $loop->the_post();
				 	echo Opallisting_Template_Loader::get_template_part( 'single-agent/box' );
				}
			}
	 	wp_reset_postdata();
	}
}
?>