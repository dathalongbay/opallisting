<?php
/**
*
*	King Composer
*	(c) KingComposer.com
*
*/

if(!defined('ABSPATH')) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}

$kc = KingComposer::globe();
$pdk = $kc->check_pdk();
	
?>
<div class="kc-pro-settings wrap about-wrap">
	<?php
	if( !is_dir( ABSPATH.KDS.'wp-content'.KDS.'plugins'.KDS.'kc_pro' ) ){
		if( $pdk != 1 && $pdk != 2 ){
	?>
		<table class="form-table">
			<tbody>
				<tr>
					<td>
						<div id="kc-pro-settings-download-wrp">
							<h1 style="margin: 0px;"><?php echo _e( 'Try the KC Pro! now', 'kingcomposer' ); ?></h1>
							<h3><?php echo _e( 'Start your 30 days trial today to discover the KC Pro! <br />"Live FrontEnd Editor" and alot of useful addon elements.', 'kingcomposer' ); ?></h3>
							<br />
							<hr />
							<br />
							<p>
								<input type="hidden" id="kc-nonce-download" value="<?php echo wp_create_nonce('kc-pro-download'); ?>" />
								<button class="button button-large button-primary" id="kc-pro-settings-process-download">
									<i class="fa-download"></i> <?php _e('Install KC Pro! Automatically', 'kingcomposer'); ?>
								</button> 
								<span class="kc-pro-direct-download">&nbsp; Or &nbsp; <a href="http://bit.ly/kc-pro"><?php _e('Directly download kc-pro.zip', 'kingcomposer'); ?></a>
								</span>
							</p>
							<br />
						</div>
					</td>
					<td>
						<p>
							<br />
							<iframe width="380" height="250" id="kc-pro-settings-video-frame" src="https://www.youtube.com/embed/CvFTZmGOxmw" frameborder="0" allowfullscreen></iframe>
							<p class="align-center">
								<button class="button" id="kc-pro-settings-larger-video" ><i class="fa-expand"></i> <?php _e( 'Larger video size', 'kingcomposer' ); ?></button>
							</p>
						</p>
					</td>
				</tr>
			</tbody>
		</table>
		<?php }else{ ?>
		<table class="form-table">
			<tbody>
				<tr>
					<td>
						<div id="kc-pro-settings-download-wrp">
							<h1 style="margin: 0px;"><?php echo _e( 'Install the KC Pro!', 'kingcomposer' ); ?></h1>
							<br />
							<br />
							<p>
								<input type="hidden" id="kc-nonce-download" value="<?php echo wp_create_nonce('kc-pro-download'); ?>" />
								<button class="button button-large button-primary" id="kc-pro-settings-process-download">
									<i class="fa-download"></i> <?php _e('Install KC Pro! Automatically', 'kingcomposer'); ?>
								</button> 
								<span class="kc-pro-direct-download">&nbsp; Or &nbsp; <a href="http://bit.ly/kc-pro"><?php _e('Directly download kc-pro.zip', 'kingcomposer'); ?></a>
								</span>
							</p>
							<br />
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<?php } ?>
	<?php }else{ ?>
		<p>
			<?php _e( 'you have not activated the plugin KC Pro! Please active it via plugins page or click on the button bellow:', 'kingcomposer' ); ?>
		</p>
		<input type="hidden" name="re-active-kc-pro" value="0" />
		<button id="kc-pro-settings-re-active" class="button button-large button-primary">
			<?php _e( 'Active KC Pro! now', 'kingcomposer' ); ?>
		</button>
	<?php } ?>
</div>
