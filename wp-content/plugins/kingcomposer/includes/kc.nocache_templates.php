<?php
/**
*
*	King Composer
*	(c) KingComposer.com
*
*/
if(!defined('KC_FILE')) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}

global $kc;

?>
<script type="text/html" id="tmpl-kc-container-template">
	<div id="kc-container" <# if( kc.cfg.showTips == 0 ){ #>class="hideTips"<# } #>>
		<div id="kc-controls">
			<button class="button button-large red classic-mode">
				<i class="sl-action-undo"></i> 
				<?php _e('Classic Mode', 'kingcomposer'); ?>
			</button>
			<?php do_action('kc-top-nav'); ?>
			<button class="button button-large alignright post-settings">
				<i class="sl-settings"></i> <?php _e('Content Settings', 'kingcomposer'); ?>
			</button>
			<span class="alignright inss" <# if( kc.cfg.instantSave == 0 ){ #>style="display: none;"<# } #>>
				<?php _e('Press Ctrl + S to quick save', 'kingcomposer'); ?>
			</span>
		</div>
		
		<div id="kc-rows"></div>
		
		<div id="kc-footers">
			<ul>
				<li class="basic-add">
					<span class="m-a-tips"><?php _e('Browse all elements', 'kingcomposer'); ?></span>
				</li>
				<li class="one-column quickadd" data-content='[kc_row][/kc_row]'>
					<span class="grp-column"></span>
					<span class="m-a-tips"><?php _e('Add an 1-column row', 'kingcomposer'); ?></span>
				</li>
				<li class="two-columns quickadd" data-content='[kc_row][kc_column width="6/12"][/kc_column][kc_column width="6/12"][/kc_column][/kc_row]'>
					<span class="grp-column"></span>
					<span class="grp-column"></span>
					<span class="m-a-tips"><?php _e('Add a 2-column row', 'kingcomposer'); ?></span>
				</li>
				<li class="three-columns quickadd" data-content='[kc_row][kc_column width="4/12"][/kc_column][kc_column width="4/12"][/kc_column][kc_column width="4/12"][/kc_column][/kc_row]'>
					<span class="grp-column"></span>
					<span class="grp-column"></span>
					<span class="grp-column"></span>
					<span class="m-a-tips"><?php _e('Add a 3-column row', 'kingcomposer'); ?></span>
				</li>
				<li class="four-columns quickadd" data-content='[kc_row][kc_column width="3/12"][/kc_column][kc_column width="3/12"][/kc_column][kc_column width="3/12"][/kc_column][kc_column width="3/12"][/kc_column][/kc_row]'>
					<span class="grp-column"></span>
					<span class="grp-column"></span>
					<span class="grp-column"></span>
					<span class="grp-column"></span>
					<span class="m-a-tips"><?php _e('Add a 4-column row', 'kingcomposer'); ?></span>
				</li>
				<li class="column-text quickadd" data-content="custom">
					<i class="et-document"></i>
					<span class="m-a-tips"><?php _e('Push customized content and shortcodes', 'kingcomposer'); ?></span>
				</li>
				<li class="title quickadd" data-content='paste'>
					<i class="et-clipboard"></i>
					<span class="m-a-tips"><?php _e('Paste copied element', 'kingcomposer'); ?></span>
				</li>
				<li class="kc-add-sections">
					<i class="et-lightbulb"></i> 
					<?php _e('Sections Manager', 'kingcomposer'); ?>
					<span class="m-a-tips"><?php _e('Installation of sections which were added', 'kingcomposer'); ?></span>
				</li>
			</ul>
		</div>
	</div>	
</script>
<script type="text/html" id="tmpl-kc-components-template">
	<div id="kc-components">
		<ul class="kc-components-categories">
			<li data-category="all" class="all active"><?php _e('All', 'kingcomposer'); ?></li>
			<?php
				
				$maps = $kc->get_maps();
				$categories = array();
				
				foreach( $maps as $key => $map )
				{
					$category = isset( $map['category'] ) ? $map['category'] : '';
					
					if( !in_array( $category, $categories ) && $category != '' )
					{
						array_push( $categories, $category );
						echo '<li data-category="'.sanitize_title($category).'" class="'.sanitize_title($category).'">';
						echo esc_html($category);
						echo '</li>';
					}
				}
			?>
			<li data-category="kc-wp-widgets" class="kc-wp-widgets mcl-wp-widgets">
				<i class="fa-wordpress"></i> <?php _e('Widgets', 'kingcomposer'); ?>
			</li>
			<li data-category="kc-clipboard" class="kc-clipboard mcl-clipboard">
				<i class="et-layers"></i> <?php _e('Clipboard', 'kingcomposer'); ?>
			</li>
		</ul>
		<ul class="kc-components-list-main kc-components-list">
			<?php
				foreach( $maps as $key => $map )
				{
					if( !isset( $map['system_only'] ) )
					{
						$category = isset( $map['category'] ) ? $map['category'] : '';
						$name = isset( $map['name'] ) ? $map['name'] : '';
						$icon = isset( $map['icon'] ) ? $map['icon'] : '';
					?>
						<li <?php
								if( isset( $map['description'] ) && !empty( $map['description'] ) ){
									echo 'title="'.esc_attr( $map['description'] ).'"';
								}
							?> data-category="<?php 
									echo sanitize_title( !empty($category) ? $category : 'uncategoried' ); 
								?>" data-name="<?php 
									echo esc_attr( $key ); 
								?>" class="kc-element-item mcpn-<?php 
									echo sanitize_title( !empty($category) ? $category : 'uncategoried' ); 
								?>">
							<div>
								<span class="cpicon <?php echo esc_attr( $icon ); ?>"></span>
								<span class="cpdes">
									<strong><?php echo esc_html( $name ); ?></strong>
								</span>
								<i class="sl-star preset-open" title="<?php _e('Show presets of this element', 'kingcomposer'); ?>"></i>
							</div>
						</li>
					<?php
					}	
				}	
			?>
		</ul>
	</div>
</script>

<script type="text/html" id="tmpl-kc-wp-widgets-template">
<div id="kc-wp-list-widgets"><?php 
	
	if( !function_exists( 'submit_button' ) ){
		function submit_button( $text = null, $type = 'primary', $name = 'submit', $wrap = true, $other_attributes = null ) {
			echo kc_get_submit_button( $text, $type, $name, $wrap, $other_attributes );
		}
	}
	
	ob_start();
		@wp_list_widgets();
		$content = str_replace( array( '<script', '</script>' ), array( '&lt;script', '&lt;/script&gt;' ), ob_get_contents() );
	ob_end_clean();
	
	echo $content;
	
?></div>
</script>

<script type="text/html" id="tmpl-kc-presets-template"><#

var items = kc.backbone.stack.get( 'kc_presets', data.name ),
	cates = [], i;

if( typeof items == 'object' ){
	for( i in items ){
		if( items[i][0] !== '' && cates.indexOf( items[i][0] ) === -1 )
			cates.push( items[i][0] );
	}
}

#><li id="kc-presets-list">
	<h2 class="kc-pretit">
		{{data.name.replace('kc_','').replace(/\_/g,' ')}} <?php _e('Presets', 'kingcomposer'); ?>
		<a href="#add_preset"><i class="fa-plus"></i> <?php _e('Create New', 'kingcomposer'); ?></a>
	</h2>
	<a href="#close" class="preset-close" title="<?php _e('Close Presets', 'kingcomposer'); ?>"></a>
	<div class="kc-preset-create" style="display: none">
		<input type="text" class="kc-preset-name-input" placeholder="<?php _e('Enter preset name', 'kingcomposer'); ?>" />
		<input type="text" class="kc-preset-cats-input" placeholder="<?php _e('Preset category', 'kingcomposer'); ?>" />
		<button class="kc-preset-create-button">Done</button>
		<# if( cates.length > 0 ){ #>
			<ul class="kc-pre-cats">
				<#
					for( i in cates ){
						#><li>{{cates[i]}}</li><#
					}
				#>
			</ul>
		<# } #>
		<h2 class="success-mesg"><?php _e('The preset has been created successfully', 'kingcomposer'); ?> </h2>
	</div>
	<# if( cates.length > 0 ){ #>
	<ul class="kc-preset-categories">
		<li><a href="#all" class="active">All</a></li>
		<#
			for( i in cates ){
				#><li>/</li><li><a href="#">{{cates[i]}}</a></li><#
			}
		#>
	</ul>
	<# } #>
	<div class="kc-preset-wrp">
		<div class="kc-preset-outer" data-name="{{data.name}}">
			<# if( typeof items == 'object' && !_.isEmpty( items ) ){ #>
				<# for( i in items ){ #>
				<div class="kc-preset-item kc-preset-cat-{{kc.tools.esc_slug(items[i][0])}}" title="{{i}}">
					<p>{{i}}</p>
					<small>{{items[i][1]}}</small>
					<i class="sl-close" data-pid="{{i}}" data-pname="{{data.name}}" title="<?php _e('Delete preset', 'kingcomposer'); ?>"></i>
				</div>
				<# } #>
			<# }else{ #>
				<h2 class="kc-prempty">
					<?php _e('You have not created any preset for this element', 'kingcomposer'); ?> 
					<br />
					<a href="http://docs.kingcomposer.com/presets" target=_blank>
						<?php _e('Check the preset document', 'kingcomposer'); ?> 
					</a>
				</h2>
			<# } #>
		</div>
	</div>
</li>
<#
	data.callback = function( wrp, $ ){
		
		wrp.find('.kc-preset-categories li a').on('click', function(e){
			
			wrp.find('.kc-preset-categories li a.active').removeClass('active');
			$(this).addClass('active');
			
			if( $(this).attr('href') != '#all' ){
				wrp.find('.kc-preset-wrp .kc-preset-item').hide();
				wrp.find('.kc-preset-wrp .kc-preset-item.kc-preset-cat-'+kc.tools.esc_slug(this.innerHTML)).show();
			}else{
				wrp.find('.kc-preset-wrp .kc-preset-item').show();
			}
			
			e.preventDefault();
			return false;
			
		});
		
		wrp.find('.kc-preset-item i.sl-close').on('click', function(e){
			
			if( !confirm(kc.__.sure) ){
				e.preventDefault();
				return false;
			}
				
			
			var pid = $(this).data('pid'),
				pname = $(this).data('pname'),
				items = kc.backbone.stack.get( 'kc_presets', pname );
			
			delete items[pid];
			
			kc.backbone.stack.update('kc_presets', pname, items );
			
			$(this).closest('.kc-preset-item').remove();
			
			e.preventDefault();
			return false;
			
		});
		
	}
#>
</script>
