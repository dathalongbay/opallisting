<?php
/**
*
*	King Composer
*	(c) KingComposer.com
*
*/

if(!defined('ABSPATH')) {
	header('HTTP/1.0 403 Forbidden');
	exit;
}

if(!function_exists('is_plugin_active'))
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		
$kc = KingComposer::globe();
$settings = $kc->settings();
$plugin_info = get_plugin_data( KC_FILE );

?>

<div id="kc-settings" class="wrap about-wrap">
	<h1><?php _e('Welcome to', 'kingcomposer'); ?> KingComposer Page Builder!</h1>
	<div class="about-text">
		<?php _e('Thank you so much for choosing our product. We at king-theme are confident that you’ll be satisfied with the powerful features of KingComposer!', 'kingcomposer'); ?>
	</div>
	<div class="wp-badge kc-badge">
		<?php _e('Version', 'kingcomposer'); ?> <?php echo esc_attr( $plugin_info['Version'] ); ?>
	</div>
	<h2 class="nav-tab-wrapper">
		<a href="#kc_general_setting" class="nav-tab nav-tab-active" id="kc_general_setting-tab">
			<?php _e('General Settings', 'kingcomposer'); ?>
		</a>
		<?php if( is_plugin_active('kc_pro/kc_pro.php')){ ?>
		<a href="#kc_product_license" class="nav-tab" id="kc_product_license-tab">
			<?php _e('Product License', 'kingcomposer'); ?>
		</a>
		<?php } ?>
		<!--a href="#kc_pro" class="nav-tab" id="kc_pro-tab">
			<?php _e('KC Pro!', 'kingcomposer'); ?>
		</a-->
	</h2>
	<form method="post" action="options.php" enctype="multipart/form-data" id="kc-settings-form">
		<?php settings_fields( 'kingcomposer_group' ); ?>
		<div id="kc_general_setting" class="group p">
			<?php
				
				$update_plugin = get_site_transient( 'update_plugins' );

			    if ( isset( $update_plugin->response[ KC_BASE ] ) )
				{
			?>
			<div class="kc-notice">
				<p>
					<i class="dashicons dashicons-warning"></i> 
					<?php
						printf( __('There is a new version of KingComposer available, please go to %s to update', 'kingcomposer'),	
							'<a href="'.admin_url('/plugins.php').'">Plugins</a>'
						); ?>.
				</p>
			</div>
			<?php			
				}
			?>
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<?php _e('Supported Content Types', 'kingcomposer'); ?>:
						</th>
						<td>
							<?php
								
								$post_types = get_post_types( array( 'public' => true ) );
								$ignored_types = array('attachment');
								$settings_types = $kc->get_content_types();
								$required_types = $kc->get_required_content_types();
			
								foreach( $post_types as $type ){
									if( !in_array( $type, $ignored_types ) ){
										echo '<p class="radio"><input ';
										if( in_array( $type, $settings_types ) )
											echo 'checked ';
										if( in_array( $type, $required_types ) )
											echo 'disabled ';	
										echo'type="checkbox" name="kc_options[content_types][]" value="'.esc_attr($type).'"> ';
										echo esc_html( $type );
										if( in_array( $type, $required_types ) )
											echo ' <i> (required)</i>';
										echo '</p>';
									}
								}
								
							?>
							
							<br />
							<span class="description">
								<p>
									- <?php _e('Besides page and post above, you can set any content type to be available with KingComposer such as gallery, contact form and so on', 'kingcomposer'); ?>. 
								</p>
								<p>
									- <?php _e('If your', 'kingcomposer'); ?> <strong>"Custom Post-Type"</strong> 
									<?php _e('does not show here? Please make sure that has been registered with', 'kingcomposer'); ?> 
									<strong>"public = true"</strong>
								</p>
								<p>
									- <?php _e('Put this code on action "init" to force support', 'kingcomposer'); ?>:  
									<strong>global $kc; $kc->add_content_type( 'your-post-type-name' );</strong>
								</p>
							</span>
						</td>
					</tr>
					
					<tr>
						<th scope="row">
							<?php _e('Css Code', 'kingcomposer'); ?>:
						</th>
						<td>
							<textarea name="kc_options[css_code]" cols="100" rows="15"><?php 
								echo esc_html( $settings['css_code'] ); 
							?></textarea>
							<span class="description">
								<p>
									<?php _e('Add your custom CSS code to modify or apply additional styling to the Front-End', 'kingcomposer'); ?>. 
								</p>
							</span>
						</td>
					</tr>
				</tbody>
			</table>
			<br />
			<p class="submit">
				<input type="submit" class="button button-large button-primary" value="<?php _e('Save Change', 'kingcomposer'); ?>" />
    		</p>		
		</div>
		<div id="kc_product_license" class="group p" style="display:none">
			<div class="kc-license-notice"></div>
			<h3>
				<?php _e('Verify your license to use KC Pro! If you are using BackEnd Editor only, then please ignore this section.', 'kingcomposer'); ?>
			</h3>
			<hr />
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row">
							<?php _e('Your License Key', 'kingcomposer'); ?>:
						</th>
						<td>
							<input id="kc-pro-license-inp" type="password" class="regular-text kc-license-key" name="kc_options[license]" value="<?php 
								
								if( !isset( $kc->pdk['key'] ) || empty( $kc->pdk['key'] ) ){
									if( !empty( $kc->key ) )
										echo esc_attr( $kc->key ); 
								}else echo esc_attr( $kc->pdk['key'] ); 
								
							?>" autocomplete="off" />
							<a href="#" class="see-key"><i class="fa-eye"></i> <?php _e('see', 'kingcomposer'); ?> </a>
							<input type="hidden" name="sercurity" value="<?php echo wp_create_nonce('kc-verify-nonce'); ?>" />
							<span class="description">
								<p>
									<?php _e('You can find your license by login to', 'kingcomposer'); ?> 
									<a href="https://kingcomposer.com/my-account/" target=_blank>
										<?php _e('My Account', 'kingcomposer'); ?>
									</a>. 
									<?php _e('If you don\'t have an account yet', 'kingcomposer'); ?> 
									<a href="https://kingcomposer.com/pricing/" target=_blank>
										<?php _e('Sign Up Now!', 'kingcomposer'); ?>
									</a> 
								</p>
							</span>	
						</td>
					</tr>
					<tr>
						<th scope="row">
							<?php _e('The theme you are using', 'kingcomposer'); ?>:
						</th>
						<td>
							<p>
								<input type="text" readonly="true" class="regular-text" value="<?php echo sanitize_title( basename( get_template_directory() ) ); ?>" />
							</p>		
						</td>
					</tr>
				</tbody>
			</table>
			<br />
			<p class="submit">
				<button type="submit" id="kc-settings-verify-btn" class="button button-large button-primary">
					<i class="fa-key"></i> <?php _e('Verify your license now', 'kingcomposer'); ?>
				</button>
    		</p>
			<div class="kc-loading">
				<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
				<br />
				<p><?php _e('We are verifying your license... <br />Please do not navigate away while verifying.', 'kingcomposer'); ?></p>
			</div>	
		</div>
		<div id="kc_pro" class="group p" style="display:none">
			<?php do_action('kc-pro-settings-tab'); ?>	
		</div>
	</form>
</div>
<script type="text/javascript" src="<?php echo esc_url(KC_URL); ?>/assets/js/kc.settings.js"></script>