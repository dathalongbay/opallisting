<?php

$output = $width = $col_in_class = $col_in_class_container = $css = '';
$attributes = array();

extract( $atts );

$classes = array( 'kc_column_inner', @kc_column_width_class( $width ) );

if( !empty( $col_in_class ) )
	$classes[] = $col_in_class;

if( !empty( $css ) )
	$classes[] = $css;
	
if( strpos( $width, '%' ) !== false && ( empty($css) || strpos( $css, '|') !== false ) )
	$attributes[] = 'style="width:'.esc_attr($width).'"';

if( strpos( $width, '%' ) !== false ){
	if( empty($css) || strpos( $css, '|') !== false )
		$attributes[] = 'style="width:'.esc_attr($width).'"';
}

$col_in_class_container = !empty($col_in_class_container)?$col_in_class_container.' kc_wrapper kc-col-inner-container':'kc_wrapper kc-col-inner-container';

$attributes[] = 'class="' . esc_attr( trim( implode(' ', $classes) ) ) . '"';

$output .= '<div ' . implode( ' ', $attributes ) . '>'
		. '<div class="'.trim( esc_attr( $col_in_class_container ) ).'">'
		. do_shortcode( str_replace('kc_column_inner#', 'kc_column_inner', $content ) )
		. '</div>'
		. '</div>';

echo $output;
