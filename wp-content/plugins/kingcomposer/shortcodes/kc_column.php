<?php

$width = $css = $output = $col_class = $col_container_class = '';

extract( $atts );

$attributes = array();
$classes = array( 'kc_column', @kc_column_width_class( $width ) );

if( !empty( $col_class ) )
	$classes[] = $col_class;

if( !empty( $css ) )
	$classes[] = $css;
	
if( strpos( $width, '%' ) !== false && ( empty($css) || strpos( $css, '|') !== false ) )
	$attributes[] = 'style="width:'.esc_attr($width).'"';

$attributes[] = 'class="' . esc_attr( trim( implode(' ', $classes) ) ) . '"';

$col_container_class = !empty( $col_container_class ) ? $col_container_class.' kc-col-container' : 'kc-col-container';

$output = '<div ' . implode( ' ', $attributes ) . '>'
		. '<div class="'.esc_attr( $col_container_class ).'">'
		. do_shortcode( str_replace('kc_column#', 'kc_column', $content ) )
		. '</div>'
		. '</div>';

echo $output;
