<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'opallisting.dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2fP)(lo8S~_KI2Eg~F{W 6GhfXorERQzeW9r6&jaeP:)*:S C_.JOs5BDP%#YZr|');
define('SECURE_AUTH_KEY',  'YcjK8^ThJ~tNjcR8b|YaL?@Vy]M&~~W8aexcsyI#*pW|vL7[$InG$a}T zupaYox');
define('LOGGED_IN_KEY',    'k`=.gWTk(FIj7CP!*w<B5p%w*At8)rs=S$kq=(rS<x<P_v~%EaY2xcUEV:aFCc$D');
define('NONCE_KEY',        '3(|@g:k@ShD9b,#!846I>iV5Dd@s/W^CBqPi~n:wpfmKYZbf*p}3^Eku?Y6^?Hyp');
define('AUTH_SALT',        'nifg7/ nA$dGq2vVs4oD!H{2U)qhan(ehmk(XaRPy-hH]L7C7X_?<7aWf$W4)>kr');
define('SECURE_AUTH_SALT', '>D=S7bx?0q[37bcxS${G^!yD,ZZkgn4b ;wL+@#P3roI0yuL&8WevENC5g*[BF(t');
define('LOGGED_IN_SALT',   'Cb5#-goSiB;E(K@p`IL.,u*,.{ee~&G!M(V&C-o^~S5`:FEJXu*bbjDl-J/bE` B');
define('NONCE_SALT',       'RGhgVr/U6?YCf+DL$RQD;{T.^nplM>$4Da$.f>nF-A4VFy}S/XHRv>RMf)$7Ga>Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
